const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");


/* List all products */
module.exports.productList = async (req, res) => {
  try {

    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }
    
    // Shows all "isActive: true" products only
    // Shows only the specified objects in the result, whereas 0 = exclude ; 1 = include
    const productList = await Product.find({isActive: true}, {productName: 1, productDescription: 1, price: 1});

    // Checks database if there are products, otherwise returns a specific result
    if(productList.length === 0) {
      return res.send("Products List not available")
    }else{
      return res.send(productList)
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send("Internal Server Error")
  }
}


/* Cart Creation */
module.exports.addCart = async (req, res) => {
  try {
    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }
    const { productName, quantity } = req.body;

    // Input validation
    if (!productName || !quantity || isNaN(quantity) || quantity <= 0) {
      return res.status(400).json({ message: "Invalid request data" });
    }

    // Find the product by its name (assuming productName is unique)
    const product = await Product.findOne({ productName });

    if (!product) {
      return res.status(404).json({ message: "Product does not exist" });
    }

    const { _id, productDescription, price } = product;
    const totalPrice = price * quantity;

    // Determine user ID or use null for guest users
    const userId = req.user ? req.user.id : null;

    // Determine userName or use "Guest" for guest users
    const userName = req.user ? `${req.user.firstName} ${req.user.lastName}` : "Guest";

    // Find the user's cart or create a new one if not exists
    let userCart = await Cart.findOne({ userId });

    if (!userCart) {
      // Create a new cart for the user
      userCart = new Cart({
        userId: req.user ? req.user.id : null,
        name: userName,
        products: [],
        totalAmount: 0,
        createdAt: new Date(),
      });
    }

    // Check if the product is already in the user's cart
    const existingProduct = userCart.products.find(
      (cartProduct) => cartProduct.productId.toString() === _id.toString()
    );

    if (existingProduct) {
      // Update the quantity, price, and totalPrice of the existing product
      existingProduct.quantity += quantity;
      existingProduct.totalPrice += totalPrice;
    } else {
      // Add the product as a new item in the cart
      userCart.products.push({
        productId: _id,
        productName,
        productDescription,
        quantity,
        price,
        totalPrice,
      });
    }

    // Calculate the new totalAmount for the user's cart
    userCart.totalAmount += totalPrice;

    // Save the updated or new cart to the database
    await userCart.save();

    // Return the updated user's cart in the response
    return res.status(201).json({
      message: "Added to cart successfully",
      cart: userCart,
      addedProduct: {
        productName,
        productDescription,
        quantity,
        price,
        totalPrice,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};



/* Update Existing Cart */
module.exports.updateCart = async (req, res) => {
  try {
    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }

    const { productId, quantity } = req.body;

    // Find the user's cart
    let userCart = await Cart.findOne({ userId: req.user.id });

    if (!userCart) {
      return res.status(404).json({ message: "No Existing Cart, please create one" });
    }

    // Check if the product is in the cart
    const existingProduct = userCart.products.find(
      (cartProduct) => cartProduct.productId.toString() === productId
    );

    if (!existingProduct) {
      return res.status(404).json({ message: "Product not added yet to a cart" });
    }

    // Update the quantity, price, and totalPrice of the existing product
    existingProduct.quantity = quantity;
    existingProduct.totalPrice = existingProduct.price * quantity;

    // Recalculate the totalAmount for the cart
    userCart.totalAmount = userCart.products.reduce(
      (total, product) => total + product.totalPrice,
      0
    );

    // Save the updated cart
    await userCart.save();

    // Return the updated cart
    res.status(200).json({ 
      message: "Cart updated successfully",
      updatedCart: userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};



/* Remove Specific Quantity from Cart */
module.exports.reduceCartQuantity = async (req, res) => {
  try {
    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }
    
    const { productName, quantity } = req.body;

    // Find the user's cart
    let userCart = await Cart.findOne({ userId: req.user.id });

    if (!userCart) {
      return res.status(404).json({ message: "Cart not found" });
    }

    // Find the product in the cart
    const existingProduct = userCart.products.find(
      (cartProduct) => cartProduct.productName === productName
    );

    if (!existingProduct) {
      return res.status(404).json({ message: "Product not found in cart" });
    }

    if (!quantity) {
      // If quantity is not provided, remove the entire product from the cart
      userCart.totalAmount -= existingProduct.totalPrice;
      userCart.products = userCart.products.filter(
        (cartProduct) => cartProduct.productName !== productName
      );
    } else {
      // If quantity is provided, reduce the specified quantity from the product in the cart
      if (quantity <= 0 || quantity > existingProduct.quantity) {
        return res.status(400).json({ message: "Invalid quantity provided" });
      }

      existingProduct.quantity -= quantity;
      existingProduct.totalPrice -= existingProduct.price * quantity;
      userCart.totalAmount -= existingProduct.price * quantity;
    }

    // Save the updated cart to the database
    await userCart.save();

    // Show only specific details or properties on the updated cart
    const updatedDetails = userCart.products.map((cartProduct) => ({
      productName: cartProduct.productName,
      productDescription: cartProduct.productDescription,
      quantity: cartProduct.quantity,
      price: cartProduct.price,
      totalPrice: cartProduct.totalPrice,
    }));

    // Return detailed information of the changes in the cart
    res.status(200).json({ 
      message: "Item removed from cart successfully",
      updatedCart: updatedDetails,
      totalAmount: userCart.totalAmount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};



/* Check Cart Items - Retrieve */
module.exports.userCartSummary = async (req, res) => {
  try {
   
    // Check if the user is an Admin, otherwise returns a message
    if (req.user.isAdmin) {
      return res.status(403).json({ error: "Access denied for admin users" });
    }
    
    // Find the user's cart based on their user ID
    const userId = req.user.id;
    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ error: "You don't have an existing cart" });
    }

    // Fetch the user details
    const user = await User.findOne({ _id: userId });

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    
    const selectedProductInfo = userCart.products.map((product) => ({
      productName: product.productName,
      productDescription: product.productDescription,
      quantity: product.quantity,
      price: product.price,
      totalPrice: product.totalPrice
    }));



    // Return cart summary
    res.status(200).json({
      user: {
        name: `${user.firstName} ${user.lastName}`,
        username: user.userName,
        email: user.email,
        billingAddress: user.billingAddress
      },
      cart: {
        totalAmount: userCart.totalAmount,
        products: selectedProductInfo
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};



/* Cart Checkout */
module.exports.userCartCheckout = async (req, res) => {
  try {
    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }
    
    const { billingAddress, 
            shippingAddress,
            productsToCheckout,
            paymentMethod,
            deliveryOption } = req.body;
    const userId = req.user.id;
    const userCart = await Cart.findOne({ userId, purchasedOn: null });

    // Fetch the user details
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    // Check if paymentMethod and deliveryOption is provided
    if(!paymentMethod && !deliveryOption) {
      return res.status(400).json({error: "Please select a Payment and Delivery option."})
    }else if(!paymentMethod){
      return res.status(400).json({error: "Please select a Payment option."})
    }else if(!deliveryOption){
      return res.status(400).json({error: "Please select a Delivery option."})
    }

    // Converting to case insensitive
    const casePaymentMethod = paymentMethod.toLowerCase();
    const caseDeliveryOption = deliveryOption.toLowerCase();

    // Check if paymentMethod and deliveryMethod is a valid option
    if(casePaymentMethod !== "gcash" && casePaymentMethod !== "cod" && casePaymentMethod !== "card"){
      return res.status(400).json({error: "Payment option selected is not valid."})
    }else if(caseDeliveryOption !== "express" && caseDeliveryOption !== "standard"){
      return res.status(400).json({error: "Delivery option selected is not valid."})
    }

    // Initialize billingAddress as null
    let finalBillingAddress = null;
    let finalShippingAddress = null;

    // Check if billingAddress is provided in the request body
    if (billingAddress) {
      finalBillingAddress = billingAddress;
      finalShippingAddress = shippingAddress || billingAddress;
    } else {

      // If not provided, use the users existing billingAddress from the database
      if (user.billingAddress) {
        finalBillingAddress = user.billingAddress;
        finalShippingAddress = user.shippingAddress || user.billingAddress
      } else {

        // Return an error if billingAddress is neither provided nor available in the database
        return res.status(400).json({ message: `Billing Address is required!` });
      }
    }

    if (!userCart) {
      return res.status(404).json({ message: "Cart not found" });
    }

    // Update the user's billing address and shipping address in the database
    await User.findByIdAndUpdate(userId, { billingAddress, shippingAddress });

    // Calculate the total amount for the products to checkout
    let totalAmount = 0;

    // For full name concatenation
    let name = `${req.user.firstName} ${req.user.lastName}`;

    // Create a new Checkout record
    const newCheckout = new Order({
      userId,
      name,
      paymentMethod,
      shippingAddress: shippingAddress || billingAddress,
      deliveryOption,
      products: [],
      totalAmount: 0,
      purchasedOn: new Date(),
      isActive: true
    });
    console.log(newCheckout)

    // Iterate through the specified products to checkout
    for (const productToCheckout of productsToCheckout) {
      const { productName, quantity } = productToCheckout;
    
      // Find the product in the user's cart
      const cartProduct = userCart.products.find(
        (cartProduct) => cartProduct.productName === productName
      );
    
      if (!cartProduct) {
        return res.status(404).json({ message: `Product '${productName}' not found in cart` });
      }
    
      if (quantity <= cartProduct.quantity) {
        // Fetch the product details including productDescription from the database
        const product = await Product.findOne({ productName });
    
        if (!product) {
          return res.status(404).json({ message: `Product '${productName}' not found in database` });
        }
    
        const { price, productDescription } = product;
    
        // Update the current inventory and automatically archive the product if quantity is 0
        const { quantity: currentQuantity, _id } = product;
    
        if (currentQuantity < quantity) {
          return res.status(400).json({
            message: `Not enough stock for '${productName}' in the inventory`,
          });
        }
    
        // Deduct the purchased quantity from the product's quantity in the database
        await Product.findByIdAndUpdate(_id, { quantity: currentQuantity - quantity });
    
        if (currentQuantity - quantity === 0) {
          await Product.findByIdAndUpdate(_id, { isActive: false });
        }
    
        // Calculate the total amount for the products to checkout
        const productTotalPrice = cartProduct.price * quantity;
        totalAmount += productTotalPrice;
    
        // Update the cart with the reduced quantity
        cartProduct.quantity -= quantity;
        cartProduct.totalPrice -= productTotalPrice;
    
        // Remove the product from the cart if the quantity becomes 0
        if (cartProduct.quantity === 0) {
          userCart.products = userCart.products.filter(
            (product) => product.productName !== productName
          );
        }
    
        // Add the product to the new Checkout record
        newCheckout.products.push({
          productName,
          productDescription,
          quantity,
          price,
          totalPrice: productTotalPrice,
        });

        // Update the products quantity in the database after checkout
        await Product.findByIdAndUpdate(_id, { quantity: currentQuantity - quantity });

      } else {
        return res.status(400).json({
          message: `Quantity requested for '${productName}' exceeds available quantity in cart`,
        });
      }
    }

    // Save the updated cart to the database
    await userCart.save();

    // Save the new Checkout record
    newCheckout.totalAmount = totalAmount;
    await newCheckout.save();

    // Return detailed information of the order after checkout
    res.status(200).json({
      message: "Successful Checkout",
      user: {
        name: `${user.firstName} ${user.lastName}`,
        email: user.email,
        billingAddress: user.billingAddress,
        shippingAddress: finalShippingAddress,
      },
      paymentMethod,
      deliveryOption,
      summary: {
        products: newCheckout.products,
        totalAmount: totalAmount,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};



/* Retrieve Details (ADMIN) */
module.exports.retrieveInformation = async (req, res) => {
  try {
    const { retrieve } = req.body;

    if (!retrieve) {
      return res.status(400).json({ error: "Please provide request for retrieval" });
    }

    if (!req.user || !req.user.isAdmin) {
      return res.status(403).json({ error: "Access denied. Admins Only." });
    }

    let dataToRetrieve;

    if (retrieve === "users") {
      dataToRetrieve = await User.find().select("firstName lastName userName email isAdmin createdAt");
    } else if (retrieve === "carts") {
      dataToRetrieve = await Cart.find().select("name email products")
    } else if ( retrieve === "orders") {
      dataToRetrieve = await Order.find()
    }else {
      return res.status(400).json({ error: "Invalid request" });
    }

    return res.status(200).json(dataToRetrieve);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};



/* Retrieve Orders (USERS) */
module.exports.retrieveOrders = async (req, res) => {
  try {
    // Check if the user is an Admin
    if (req.user && req.user.isAdmin) {
      return res.status(403).json({ message: "Action not allowed for Admins" });
    }

    // Get the user's ID from the authenticated request
    const userId = req.user.id;

    // Query the orders collection for orders associated with the user's ID
    const userOrders = await Order.find({ userId });

    if (!userOrders || userOrders.length === 0) {
      return res.status(404).json({ message: "No orders found" });
    }

    // Return the user's orders
    return res.status(200).json(userOrders);
  } catch (error) {
    console.error("Error retrieving orders:", error);
    return res.status(500).json({ message: "An error occurred while retrieving orders." });
  }
};
