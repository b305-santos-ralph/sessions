/* Retrieve Details of All Users and Their Carts (ADMIN) */
module.exports.getAllUsersAndCarts = async (req, res) => {
    try {
      // Check if the user is an Admin
      if (!req.user || !req.user.isAdmin) {
        return res.status(403).json({ message: "Access denied for non-admin users" });
      }
  
      // Find all users and their associated carts
      const users = await User.find();
      const userCartDetails = [];
  
      for (const user of users) {
        const userId = user.id;
  
        // Find the user's cart or handle if it doesn't exist
        const userCart = await Cart.findOne({ userId });
  
        userCartDetails.push({
          userId: userId,
          name: `${user.firstName} ${user.lastName}`,
          email: user.email,
          billingAddress: user.billingAddress,
          shippingAddress: user.shippingAddress || user.billingAddress,
          cart: userCart || null, // Handle if the cart is null
        });
      }
  
      // Return the details of all users and their carts
      res.status(200).json(userCartDetails);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Internal Server Error" });
    }
  };
  