const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* Product Registration / Creation */
module.exports.regProd = async (req, res) => {
    try {
        const existProd = await Product.findOne({productName: req.body.productName});
        if(existProd){
            return res.send("Product already added.")
        }
        let newProd = new Product({
            productName: req.body.productName,
            productCategory: req.body.productCategory,
            productDescription: req.body.productDescription,
            price: req.body.price,
            quantity: req.body.quantity
        })

        await newProd.save()
        return res.send("Product successfully added!")
    }catch(err){
        console.error(err)
        return res.send(false)
    }
}

/* List all products */
module.exports.listProd = (req, res) => {
    return Product.find({}).then(prodList => {
        if(prodList === 0){
            return res.send("No product registered yet!")
        }else{
            return res.send(prodList)
        }
    })
}

/* Retrieve all active products */
module.exports.statusProd = (req, res) => {
    return Product.find({ isActive: true })

    // .select - to specify what to display on the result
        .select("productName quantity")
        .then((prodList) => {
            if (prodList.length === 0) {
                return res.send("No product registered yet!");
            } else {
                return res.send(prodList);
            }
        })
        .catch((error) => {
            console.error(error);
            return res.status(500).json({ message: "Internal Server Error" });
        });
};

/* List Single Product */
module.exports.getProd = (req, res) => {
    return Product.findOne({productName: req.body.productName})
        .select("productName productDescription productCategory price")
        .then(sgProd => {
        if(!sgProd){
            return res.send("Cannot find product, please specify!");
        }else {
            return res.send(sgProd);
        }
    })
}

/* Update Product (ADMIN ONLY) */
module.exports.updateProduct = async (req, res) => {
    try {
        if (Object.keys(req.body).length === 0) {
            return res.status(400).json({ message: "Please specify a product for updating." });
        }

        const updatedProduct = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true });

        if (!updatedProduct) {
            return res.status(404).json({ message: "Product does not exist." });
        }

        let response = {
            message: "Product Successfully Updated!",
            updates: updatedProduct
        };

        return res.status(200).json(response);
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Update operation failed." });
    }
};


/* Archive Product */
module.exports.archiveProduct = async (req, res) => {
    const { _id } = req.body;

    try {
        if (!_id) {
            return res.status(400).json({ message: "Please specify a product ID for archiving." });
        }

        const prod = await Product.findOneAndUpdate(
            { _id },
            { isActive: false },
            { new: true }
        );

        if (!prod) {
            return res.status(404).json({ message: "Product not found for archiving." });
        }

        let response = {
            message: "Product Successfully Archived!",
            updates: prod
        };

        return res.status(200).json(response);
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Archive operation failed." });
    }
};



/* Activate Product */
module.exports.activateProduct = async (req, res) => {
    const { _id } = req.body;

    try {
        if (!_id) {
            return res.status(400).json({ message: "Please specify a product ID for archiving." });
        }

        const prod = await Product.findOneAndUpdate(
            { _id },
            { isActive: true },
            { new: true }
        );

        if (!prod) {
            return res.status(404).json({ message: "Product not found for activation." });
        }

        let response = {
            message: "Product Successfully Activated!",
            updates: prod
        };

        return res.status(200).json(response);
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Archive operation failed." });
    }
};


/* Retrieve Archived Products */
module.exports.archivedProduct = async (req, res) => {
    try {
        const prodList = await Product.find({ isActive: false }).sort({ productName: 1 });

        if (prodList.length === 0) {
            return res.send("No products found in the archive!");
        } else {
            return res.send(prodList);
        }
    } catch (error) {
        console.error("Error fetching archived products:", error);
        return res.status(500).send("Internal Server Error");
    }
};

