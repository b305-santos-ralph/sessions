const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* User Registration */
module.exports.regUser = async (req, res) => {
    try {
        const { email, userName } = req.body;

        // Checks if either property exists in the database
        const existUser = await User.findOne({
            $or: [{ email }, { userName }]
        });

        if (existUser) {
            if (existUser.email === email) {
                return res.status(409).json({ message: `${email} is already in use.` });
            } else {
                return res.status(409).json({ message: `${userName} is already in use.` });
            }
        }

        // User creation in the database with createdAt
        let newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            userName: req.body.userName,
            password: bcrypt.hashSync(req.body.password, 10),
            billingAddress: req.body.billingAddress
        });

        // Update the `createdAt` field automatically
        newUser.createdAt = new Date();

        await newUser.save();
        return res.status(201).json({ message: "User registered successfully!" });

    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Registration failed." });
    }
};


/* List of Users */
module.exports.allUsers = (req, res) => {
    return User.find({}).then(userList => {
        if(userList.length === 0){
            return res.send("No Registered Users Yet!")
        }else{
            return res.send(userList)
        }
    })
}


/* TEST FOR lastLoggedIn */
/* const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI"

module.exports.allUsers = async (req, res) => {
    try {
        // Check if a valid Bearer token is present in the request headers
        const token = req.headers.authorization;
        if (!token || !token.startsWith('Bearer ')) {
            return res.status(401).json({ message: 'Unauthorized' });
        }

        const authToken = token.slice(7); // Remove 'Bearer ' prefix from the token
        let decoded;

        try {
            // Verify and decode the JWT token
            decoded = jwt.verify(authToken, secret); // Replace 'your-secret-key' with your actual JWT secret key
        } catch (err) {
            return res.status(401).json({ message: 'Unauthorized' });
        }

        // Extract user ID from the decoded token
        const userId = decoded.userId;

        // Find the user by ID
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Update the lastLoggedIn timestamp and save the user document
        user.lastLoggedIn = new Date();
        await user.save();

        // Return the user information including the updated lastLoggedIn timestamp
        return res.status(200).json({
            fullName: `${user.firstName} ${user.lastName}`,
            createdAt: user.createdAt,
            lastLoggedIn: user.lastLoggedIn
        });
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
};
 */


/* List Single User */
module.exports.sgUser = async (req, res) => {
    try {
        const userEmail = await User.findOne({ email: req.body.email });

        if (userEmail === null) {
            return res.send("No Registered Users!");
        } else {
            return res.send(userEmail);
        }
    } catch (error) {
        console.error(error);
        return res.status(500).send("Internal Server Error");
    }
};



/* Deactivate User Account */
module.exports.deactivateAccount = async (req, res) => {
    try {
        const userId = req.user.id;
        const { password, confirmPassword } = req.body;

        // Find the user by ID
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({ message: "User not found!" });
        }

        if(!user.isActive) {
            return res.status(400).json({message: "User account is already deactivated!" })
        }

        // Compare the provided password with the stored hashed password
        const isPasswordValid = await user.comparePassword(password);

        if (!isPasswordValid) {
            return res.status(400).json({ message: "Incorrect password!" });
        }

        // Check if the confirm password matches the password
        if (password !== confirmPassword) {
            return res.status(400).json({ message: "Passwords do not match!" });
        }

        // Deactivate the user account
        user.isActive = false;
        await user.save();

        return res.status(200).json({ message: "User account deactivated successfully!" });
    } catch (error) {
        console.error("Error deactivating account:", error);
        return res.status(500).json({ message: "An error occurred while deactivating the account." });
    }
};


/* User Login */
module.exports.logUser = async (req, res) => {
    try {
        const { userName, password } = req.body;

        if (!userName || !password) {
            return res.send("Username and Password are both required to login.");
        }

        const loggedInUser = await User.findOne({ userName });

        if (!loggedInUser) {
            return res.send("User not found!");
        } else {
            const passwordCorrect = bcrypt.compareSync(
                req.body.password, loggedInUser.password
            );

            if (passwordCorrect) {
                // Set isActive back to true upon successful login
                loggedInUser.isActive = true;
                await loggedInUser.save();

                return res.send({ access: auth.createAccessToken(loggedInUser) });
            } else {
                return res.send("Password Incorrect!");
            }
        }
    } catch (error) {
        console.error(error);
        return res.status(500).send("Internal Server Error");
    }
};


/* Update User to Admin */
module.exports.userToAdmin = async (req, res) => {
    const { email, userName } = req.body;

    try {
        // Find the user based on email and username
        const userToUpdate = await User.findOne({ email, userName });

        if (!userToUpdate) {
            return res.status(404).json({ message: "User not found" });
        }

        // Update the users role to admin
        userToUpdate.isAdmin = true;
        await userToUpdate.save();

        return res.status(200).json({ message: "Role changed to Admin!" });
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Update Failed!" });
    }
};



/* Change Admin to User */
module.exports.userChangeRole = async (req, res) => {
    const { email, userName } = req.body;

    try {
        // Find the user based on email and username
        const userToUpdate = await User.findOne({ email, userName });

        if (!userToUpdate) {
            return res.status(404).json({ message: "User not found" });
        }

        // Check if the user is already not an admin
        if (!userToUpdate.isAdmin) {
            return res.status(400).json({ message: "User is not an admin" });
        }

        // Update the user's role to non-admin
        userToUpdate.isAdmin = false;
        await userToUpdate.save();

        return res.status(200).json({ message: "Role changed to User!" });
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Update Failed!" });
    }
};


/* Update User Details */
module.exports.updUser = async (req, res) => {
    if (Object.keys(req.body).length === 0) {
        return res.status(400).json({ message: "Please specify user details for updating." });
    }

    const loggedInUserId = req.user.id; // Assuming you have the user's ID from authentication

    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });

        if (!updatedUser) {
            return res.status(404).json({ message: "User does not exist" });
        }

        // Check if the logged-in user is the same as the user being updated
        if (loggedInUserId !== updatedUser.id) {
            return res.status(403).json({ message: "You are not authorized to make any changes for this user" });
        }

        let response = {
            message: "User Details Successfully Updated!",
            updates: updatedUser
        };

        return res.status(200).json(response);
    } catch (err) {
        console.error(err);
        return res.status(500).json({ message: "Internal Server Error" });
    }
};

/* Change Password for Users */
module.exports.changePassword = async (req, res) => {
    try {
        const { currentPassword, newPassword } = req.body;
        const userId = req.user.id;

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: "User not found!" });
        }

        // Compare the current password provided by the user with the stored hashed password
        const isCurrentPasswordValid = await bcrypt.compare(currentPassword, user.password);

        if (!isCurrentPasswordValid) {
            return res.status(400).json({ message: "Current password is incorrect." });
        }

        // Compare the new password with the current password to ensure they are different
        if (currentPassword === newPassword) {
            return res.status(400).json({ message: "New password must be different from the current password." });
        }

        // Hash the new password
        const hashedNewPassword = await bcrypt.hash(newPassword, 10);

        // Update and save the user with the new hashed password
        user.password = hashedNewPassword;
        await user.save();

        res.status(200).json({ message: "Password changed successfully!" });
    } catch (error) {
        res.status(500).json({ message: "Internal Server Error" });
    }
};



/* Reset Password for Users */
module.exports.resetPassword = async (req, res) => {
    try {
        const { email, userName, newPassword } = req.body;

        // Find the user based on email and userName
        const user = await User.findOne({ email, userName });

        if (!user) {
            return res.status(404).json({ message: "User not found!" });
        }

        // Hash the new password
        const hashedPassword = await bcrypt.hash(newPassword, 10);

        // Update and save the user with the new hashed password
        user.password = hashedPassword;
        await user.save();

        res.status(200).json({ message: "Password reset successful!" });
    } catch (error) {
        res.status(500).json({ message: "Internal Server Error" });
    }
}


/* Retrieve Archived Users / Deactivated Users */
module.exports.archivedUsers = async (req, res) => {
    try {
        const userList = await User.find({isActive: false});

        if (userList.length === 0) {
            return res.send("No users found in the archive!");
        } else {
            return res.send(userList);
        }
    } catch (error) {
        console.error("Error fetching users:", error);
        return res.status(500).send("Internal Server Error");
    }
};