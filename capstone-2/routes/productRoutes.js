const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth")
const {verify, verifyAdmin} = auth

/* Routing Component */
const router = express.Router()

/* Product Registration */
router.post("/", verify, verifyAdmin, productController.regProd)

/* Retrieve All Products */
router.get("/list", verify, verifyAdmin, productController.listProd)

/* Retrieve All Active Products */
router.get("/list/status", verify, verifyAdmin, productController.statusProd)

/* Retrieve Single Product */
router.get("/", productController.getProd)

/* Retrieve Archived Product */
router.get("/archive", verify, verifyAdmin, productController.archivedProduct)

/* Update Product (Admin Only) */
router.put("/update/:id", verify, verifyAdmin, productController.updateProduct)

/* Archive Product */
router.put("/archive", verify, verifyAdmin, productController.archiveProduct)

/* Activate Product */
router.put("/activate", verify, verifyAdmin, productController.activateProduct)

module.exports = router