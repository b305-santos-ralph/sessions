const express = require("express");
const auth = require("../auth");
const orderController = require("../controllers/orderController");
const cartController = require("../controllers/cartController");
const {verify, verifyAdmin} = auth;

const router = express.Router();

// router.get("/checkout", verify, orderController.userCheckoutDetails)

module.exports = router