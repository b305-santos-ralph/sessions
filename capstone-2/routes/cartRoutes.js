const express = require("express");
const cartController = require("../controllers/cartController");
const auth = require("../auth")
const {verify, verifyAdmin} = auth

const router = express.Router()

/* List all available products */
router.get("/", cartController.productList)

/* Cart Creation - Add to Cart */
router.post("/addcart", verify, cartController.addCart)

/* Cart Update */
router.put("/update", verify, cartController.updateCart)

/* Cart Update - Delete/Reduce Quantity */
router.delete("/update/remove", verify, cartController.reduceCartQuantity)

/* Cart Checkout */
router.post("/checkout", verify, cartController.userCartCheckout)

/* Cart Summary */
router.get("/summary", verify, cartController.userCartSummary)

/* Retrieve Details */
router.post("/getinfo", verify, verifyAdmin, cartController.retrieveInformation)

/* Retrieve Checked-out order by the user */
router.post("/order", verify, cartController.retrieveOrders)



/* test */


module.exports = router