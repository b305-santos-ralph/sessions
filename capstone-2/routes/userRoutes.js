const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

/* Routing Component */
const router = express.Router()

/* User Registration */
router.post("/registration", userController.regUser)

/* Retrieve All Users (ADMIN) */
router.get("/", verify, verifyAdmin, userController.allUsers)

/* Retrieve Single User (ADMIN) */
router.post("/", verify, verifyAdmin, userController.sgUser)

/* User Login */
router.post("/login", userController.logUser)

/* Change User Role (ADMIN) */
router.put("/update/", verify, verifyAdmin, userController.userToAdmin)

/* Admin role to User (ADMIN) */
router.put("/update/admin", verify, verifyAdmin, userController.userChangeRole)

/* Update User Information */
router.put("/user-update/:id", verify, userController.updUser)

/* Acount Deactivation */
router.delete("/deactivate", verify, userController.deactivateAccount)

/* Change Password */
router.put("/change-password", verify, userController.changePassword)

/* Reset Password */
router.put("/reset-password", userController.resetPassword)

/* Retrieve Deactivated Users */
router.post("/inactive", userController.archivedUsers)


/* Export Route System */
module.exports = router