/* Dependencies and Modules */
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

/* Sample Guest User Data */
const guestUserData = {
  guest: true,
};

/* Token Creation */
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    userName: user.userName,
    billingAddress: user.billingAddress,
    shippingAddress: user.shippingAddress,
    isAdmin: user.isAdmin,
  };
  const tokenOptions = {
    expiresIn: "24h", // Set token expiration to 1 hour
  };
  return jwt.sign(data, secret, tokenOptions);
};

module.exports.verify = (req, res, next) => {
  
  // Extract token from "Bearer <token>"
  const token = req.headers.authorization?.split(" ")[1]; 

  if (!token) {
    // If no token provided, set req.user to guest user data
    req.user = guestUserData;
    return next();
  }

  jwt.verify(token, secret, (error, decodedToken) => {
    if (error) {
      return res.status(401).json({
        auth: "Failed",
        message: "Invalid token",
      });
    }

    req.user = decodedToken;
    next();
  });
};

// Verify if user account is admin or not
module.exports.verifyAdmin = (req, res, next) => {
  if (!req.user || !req.user.isAdmin) {
    return res.status(403).json({
      auth: "Failed",
      message: "Action not allowed!",
    });
  }
  next();
};