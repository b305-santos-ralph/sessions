const mongoose = require("mongoose");


const cartSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', // Reference to the User model for association
    required: false
  },
  name: {
    type: String,
  },
  products: [
    {
      productId: {
        type: String,
        required: true,
      },
      productName: {
        type: String,
        required: true,
      },
      productDescription: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        default: 1,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      totalPrice: {
        type: Number,
        required: true,
      }
    },
  ],
  totalAmount: {
    type: Number,
  },
  createdAt: {
    type: Date,
  },
  purchasedOn: {
    type: Date
  },
});

// Calculate the totalAmount before saving
cartSchema.pre('save', function (next) {
  const productsTotal = this.products.reduce((total, product) => {
    return total + product.quantity * product.price;
  }, 0);
  this.totalAmount = productsTotal;
  next();
});
  
  module.exports = mongoose.model("Cart", cartSchema);