const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', 
    required: true,
  },
  name: {
    type: String,
  },
  paymentMethod: {
    type: String,
    required: true
  },
  shippingAddress: {
    type: String,
    required: true
  },
  deliveryOption: {
    type: String, 
    required: true
  },
  products: [
    {
      productName: {
        type: String,
        required: true,
      },
      productDescription: {
        type: String,
        required: true
      },
      quantity: {
        type: Number,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      totalPrice: {
        type: Number,
        required: true,
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: true,
  },
  purchasedOn: {
    type: Date
  },
  isActive: {
    type: Boolean
  }
});

// A middleware function to convert specified fields to uppercase before saving
orderSchema.pre('save', function(next) {
  if (this.name) {
      this.name = this.name.toUpperCase();
  }
  if (this.paymentMethod) {
      this.paymentMethod = this.paymentMethod.toUpperCase();
  }
  if (this.shippingAddress) {
      this.shippingAddress = this.shippingAddress.toUpperCase();
  }
  if (this.deliveryOption) {
      this.deliveryOption = this.deliveryOption.toUpperCase();
  }
  next();
});


module.exports = mongoose.model("Order", orderSchema)





