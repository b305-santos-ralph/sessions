const mongoose = require("mongoose");
const bcrypt = require("bcrypt")

/* SCHEMA */
const userSchema = new mongoose.Schema({
    createdAt: {
        type: Date
    },
    lastLoggedIn: {
        type: Date
    },   
    firstName: {
        type: String,
        required: [true, "First Name REQUIRED!"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name REQUIRED!"]
    },
    email: {
        type: String,
        required: [true, "Email REQUIRED!"]
    },
    mobile: {
        type: Number
    },
    userName: {
        type: String,
        required: [true, "Username REQUIRED!"]
    },
    password: {
        type: String,
        required: [true, "Password REQUIRED!"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    billingAddress: {
        type: String
    },
    shippingAddress: {
        type: String
    }

});


// Method to compare a plaintext password with the stored hashed password
userSchema.methods.comparePassword = async function (candidatePassword) {
    return bcrypt.compare(candidatePassword, this.password);
};


// Middleware to update createdAt and lastLoggedIn
userSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    if (this.isModified('lastLoggedIn')) {
        this.lastLoggedIn = new Date();
    }
    next();
});



/* File Connection to Mongoose */
module.exports = mongoose.model("User", userSchema)