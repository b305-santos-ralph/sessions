// [SECTION] - if, else if, else statement

let numA = -1

/* "if" statement 
TRUE - will execute the code;
FALSE - will not execute the code*/
if(numA < 5){
    console.log("Hello");
}

    // STRING
    let city = "New York";

    if(city === "New York"){
        console.log("Welcome to New York City!");
    }

/* "else if" statement
 */
let numH = 1;

if(numH < 0){
    console.log("Hello!");
}else if(numH >0){
    console.log("World!");
}

city = "Tokyo";

if(city === "New York"){
    console.log("Welcome to New York City");
}else if(city === "Tokyo"){
    console.log("Welcome to Tokyo Japan!")
}

/* "else" statement */

let num1 = 5;
if(num1 === 0){
    console.log("Hello");
}else if(num1 === 3){
    console.log("World");
}else{
    console.log("Sorry, all conditions are not met.")
}


// /* if, else if, else with functions */
function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 30){
        return "Not a typhoon yet";
    }else if (windSpeed <= 61){
        return "Tropical Depression detected.";
    }else if (windSpeed >= 62 && windSpeed <= 88){
        return "Tropical Storm detected.";
    }else if (windSpeed >= 89 && windSpeed <=117){
        return "Severe Tropical Storm detected.";
    }else{
        return "Typhoon Detected";
    }
}

let message = determineTyphoonIntensity(25);
message = determineTyphoonIntensity(85);
message = determineTyphoonIntensity(250);
console.log(message);


// Truthy
if(true){
    console.log("truthy");
}

if(1){
    console.log("truthy");
}
if([]){
    console.log("truthy")
}

// Falsy
if(false){
    console.log("falsy");
}

if(0){
    console.log("falsy");
}

if(undefined){
    console.log("falsy");
}


// Single Statement Execution
/* Ternary Statement */
let ternary = (1 < 18) ? "Yes" : "No";
console.log(ternary);

// Multiple Statement
let name;

function isOfLegalAge(){
    name = "John";
    return "You are of the legal age limit.";
}

function isUnderAge(){
    name = "Jane"
    return "You are under the age limit."
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name)


//  Switch - case statement
let day = prompt ("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
    case "monday" : 
        console.log("The color of the day is red.");
        break;
    case "tuesday" : 
        console.log("The color of the day is orange.");
        break;
    case "wednesday" : 
        console.log("The color of the day is blue.");
        break;
    case "thursday" : 
        console.log("The color of the day is violet.");
        break;
    default:
        console.log("Please enter a valid day!");
        break;
    
}
    