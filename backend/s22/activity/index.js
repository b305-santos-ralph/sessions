// PARAMETERS
let num1 = 5;
let num2 = 15;
let num3 = num1 + num2;
let num4 = num2 - num1;
let num5 = num1 * num4;
let numbers = num3 * num4
let radius = 15

/* PARTS 1 and 2 */
// Addition
function addNum(num1, num2){
    let sum = num1 + num2;
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(sum);
    return sum
}
addNum(num1, num2);



// Subtraction
function subNum(num3, num1){
    const difference = num3 - num1;
    console.log("Displayed difference of " + num3 + " and " + num1);
    console.log(difference);
    return difference 
}
subNum(num3, num1);


// Product / Multiplication
function multiplyNum(num5, num4){
    const product = num5 * num4;
    console.log("The product of " + num5 + " and " + num4);
    console.log(product);
    return product;
}
multiplyNum(num5, num4);


// Quotient / Division
function divideNum(num5, num4){
    const quotient = num5 / num4;
    console.log("The quotient of " + num5 + " and " + num4);
    console.log(quotient);
    return quotient;
}
divideNum(num5, num4);


/* PART 3 - Total area of a Circle from a provided radius*/
function getCircleArea(radius){
    const circleArea = (Math.PI * (radius ** 2)).toFixed(2);
    console.log("The result of getting the area of a circle with " + radius + " radius:")
    console.log(circleArea)
    return circleArea
}
getCircleArea(radius);



/* PART 4 - Total average of four numbers */
function getAverage(num1, num2, num3, num4){
    const p4Sum = num1 + num2 + num3 + num4;
    const averageVar = p4Sum/getAverage.length;
    console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":" )
    console.log(averageVar)
    return averageVar
}
getAverage(num1, num2, num3, num4);


/* PART 5 - Checking the percentage of the score against the passing percentage*/
function checkIfPassed(yourScore, totalScore){
    const percentage = (yourScore / totalScore) * 100;
    let isPassed = percentage >= 75;
    console.log("Is " + yourScore + "/" + totalScore + " a passing score?")
    console.log(isPassed);
    return isPassed;
}
checkIfPassed(77, 100)
let isPassingScore = checkIfPassed;
console.log(isPassingScore);