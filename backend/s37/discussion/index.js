// Node.js
/* Client-server architecture */
    // CLIENT APPLICATIONS -> Browsers
    // DB SERVERS -> systems developed using node
    // DB -> MongoDB

/* Benefits */
/* 
1. Performance -> optimized for web applications.
2. Familiarity -> same old JS.
3. NPM - Node Package Manager
*/



/* Use the "require" directive to load Node.js */
// "http module" contains the components needed to create a server.
let http = require("http")

/* createServer() 
-> listens to a request from the client. */

/* A port is a virtual point where network connection start and end. */
/* .listen()
-> where the server will listen to any request sent in our server */
http.createServer(function(request,response){
        // writeHead() -> sets the status code for response
        // status code 200 means OK
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Hello World!");
}).listen(4000);

console.log("Server is running at localhost:4000");