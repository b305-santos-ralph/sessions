const express = require("express");
const app = express();
const port = 4000;

// Middlewares
app.use(express.json())
/* // Allows your app to read data from forms */

app.use(express.urlencoded({extended:true}));
/* // Accepts all data types */


// [SECTION] Routes
app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.get("/hello", (req, res) => {
    res.send("Hello from /hello end point.");
})

/* route expects to receive a POST method at the URI "/hello" */
app.post("/hello", (req, res) => {
    res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
})

/* POST route to register user */

// MINI CHALLENGE
/* 
if no registered user = Please register first
if user exists = User already exists */

let users = [];
app.post("/signup", (req, res) => {
    console.log(req.body);

        if(req.body.username !== "" && req.body.password !== ""){
            const existingUser = users.find(user => user.username === req.body.username);
            
            if(existingUser){
                res.send(`User ${req.body.username} already exists.`);
            }else{
                users.push(req.body);
                res.send(`User ${req.body.username} successfully registered!`);
            } 
        }else{
            res.send("Please input BOTH username and password");
        }
});


/* Change password - PUT method */
app.put("/change-password",(req, res) => {
    let message;

    if(users.length === 0){
        message = "Not registered. Please register first!";
    }else{
        for(let i = 0; i < users.length; i++){
            if(req.body.username === users[i].username){
                users[i].password = req.body.password;
    
                message = `User ${req.body.username}'s password has been updated.`;
                break;
    
        }else{
            message = "User does not exist.";
        }
    }
}
    res.send(message);
});






if(require.main === module){
    app.listen(port, () =>
    console.log(`Server is running at port ${port}`));
};