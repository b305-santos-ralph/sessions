console.log(
    "Bakit Malungkot ang Beshy ko?"
);

// [SECTION] Syntax, Statements and Comments
    // JS Statements usually end with semicolon(;)

    // There are two types of comments:
        // 1. The single-line comment denoted by "//"
        // 2. The multiple-line comment denoted by "/**/" 

// [SECTION] Variables
    // It is used to contain data
    // Any information that is used by an application is stored in what we call a "memory"

    // Declaring Variables - tells our devices that a variable name is created.

    // A naming convention
    let myVariableNumber; 
    console.log("myVariableNumber");
    console.log(myVariableNumber);
    // let myVariableNumber
    // let my-variable-Number
    // let my_variable_number

    // Syntax
        // let / const / var variableName

    // Declaring and Initializing Variables
    let productName = "desktop computer";
    console.log(productName);

    let productPrice1 = "18999";
    let productPrice = 18999;
    console.log(productPrice);
    console.log(productPrice1);

    // const, impossible to reassign = Constant
    const interest = 3.539;


    // Reassigning variable values
    productName = "Laptop"
    console.log(productName);

    // Will return an error if tried to reassign
/*         interest = 4.239;
        console.log(interest); */

    // Reassigning Variables vs Initializing Variables
        /* Declare the variable */
        myVariableNumber = 2023;
        console.log(myVariableNumber);

    // Multiple Variable Declarations
    let productCode = 'DC017'
    const productBrand = 'Dell'
    console.log(productCode, productBrand);


// [SECTION] Data Types
    // strings
    let country = "Philippines";
    let province = 'Metro Manila';

    // concatenating strings
    let fullAddress = province + ", " + country;
    console.log(fullAddress);
    /* console.log("Metro Manila, Philippines") */

    let greeting = "I live in the " + country;
    console.log(greeting);

    let mailAddress = 'Metro Manila \n\nPhilippines';
    console.log(mailAddress);

    let message = "John's employees went home early";
    console.log(message)

    message = 'John\'s employees went home early again!';
    console.log(message);

    // numbers
    let headcount = 26;
    console.log(headcount);

    // decimal numbers/fractions
    let grade = 98.7;
    console.log(grade);

    // exponential notation
    let planetDistance = 2e10;
    console.log(planetDistance);

    // combining text and strings
    console.log("John's grade last quarter is " + grade);

    // Boolean
    /* returns true or false */
    let isMarried = false;
    let inGoodConduct = true;
    console.log("isMarried: " + isMarried);
    console.log("inGoodConduct: " + inGoodConduct);

    // Arrays
    /*  */
    let grades = [ 98.7, 92.1, 90.2, 94.6 ];
    console.log(grades);

    /* different data types */
    let details = [ "John", "Smith", 32, true ]
    console.log(details);

    // Objects
    /* composed of "key/value pair" */
    let person = {
        fullName: "Juan Dela Cruz",
        age: 35,
        isMarried: false,
        contact: ["09123456789", "09987654321"],
        address: {
            houseNumber: "345",
            city: "Manila"
        }
    };
    console.log(person);
    console.log(person.fullName);
    console.log(person.age);
    console.log(person.isMarried);
    console.log(person.contact[0]);
    console.log(person.contact[1]);


    let arrays = [
        [ "hey", "hey1", "hey2"],
        [ "hey", "hey1", "hey2"],
        [ "hey", "hey1", "hey2"],
    ];

    console.log(arrays);

    let myGrades = {
        firstGrading: 98.7,
        secondGrading: 92.1,
        thirdGrading: 90.2,
        fourthGrading: 94.6,
    };

    console.log(myGrades);

    // typeof Operator
    /* will determine the type of data */
    console.log(typeof myGrades);
    console.log(typeof arrays);
    console.log(typeof greeting);

    // Null
    let spouse = null;
    let myNumber = 0;
    let myString = '';

    console.log(spouse);
    console.log(myNumber);
    console.log(myString);

    // Undefined
    let fullName;
    console.log(fullName);