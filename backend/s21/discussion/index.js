console.log("Good evening!")

//[ SECTION ] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
    //(function statement) defines a function with the specified parameters.

    /*
    Syntax:
        function functionName() {
            code block (statement)
        }
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

function printName(){
    console.log("My Name is Ralph")
}

//[ SECTION ] Function Invocation
    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
    //It is common to use the term "call a function" instead of "invoke a function".

    printName();

    declaredFunction(); // - results in an error, much like variables, we cannot invoke a function we have yet to define. 
    
     //[ SECTION ] Function declarations vs expressions
         //Function Declarations
    
             //A function can be created through function declaration by using the function keyword and adding a function name.
    
             //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
             
             //declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.
    
             //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.
    
     function declaredFunction() {
         console.log("Hello World from declaredFunction()");
     };

    //  declaredFunction();
    // Function Expression
        //  A function can also be stored in a variable.This is called a function expression.
        // A function expression is an anonymous function assigned to the variableFunction
        // Anonymous function - a function without a name.
    
    // variableFunction(); - error (index.js:54 Uncaught ReferenceError: Cannot access 'variableFunction' before initialization)

    let variableFunction = function(){
        console.log("Hello again!");
    }

    variableFunction();
    