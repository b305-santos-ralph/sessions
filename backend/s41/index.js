const express = require ("express");
const mongoose = require ("mongoose");

const app = express ();
const port = 3001;

// MongoDB Connection using mongoose
mongoose.connect("mongodb+srv://admin:admin123@b305.1b2vpeo.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
    /* allows us to avoid any current or future errors while connecting to mongoDB */
});

// set notification for connection status
/* if error occurred, output in console */
let db = mongoose.connection;
    db.on("error", console.error.bind(console, "Connection Error!"));

/* if connection is successful, output in console */
    db.once("open", () => console.log("We're connected to the cloud database."))

// [SECTION] Mongoose Schema
/* SCHEMAS - determine the structure of the documents to be written in the database. */
const taskSchema = new mongoose.Schema({
    /* Define fields with corresponding data type */
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);
/* Middlewares
- allows our app to read json data type.
- allows to read and accept data in form. */
    app.use(express.json());
    app.use(express.urlencoded({
        extended: true
    }));

// TASK ROUTES
/* Business Logic */
/*  1. Add a functionality to check if there are duplicate tasks.
        - if the taks already exists in the database, we return an error
        - if the task doesn't exist in the database, we add in into the database.
    2. The task data will be coming from the request's body
    3. Create a new task object with a "name" field/property
    4. The "status property does not need to be provided because our schema defaults it to "pending" upon creation of an object  */
app.post("/tasks", (req, res) => {
    Task.findOne({name: req.body.name}).then((result, error) => {
        if(result !== null && result.name === req.body.name){
            return res.send("duplicate task found")
        }else{
            let newTask = new Task({
                name: req.body.name
            })

            newTask.save().then((savedTask, saveErr) => {
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    /* if there is no error */
                    return res.status(201).send("New Task Created!");
                }
            })
        }
    })
})

// Port Listening
if(require.main === module){
    app.listen(port, () => console.log(`Server is running at port ${port}`))
};

module.exports = app;
