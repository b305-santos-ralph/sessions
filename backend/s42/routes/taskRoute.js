const express = require("express");

/* ROUTER - allows access to HTTP Methods and middlewares */
const router = express.Router();
const taskController = require("../controllers/taskController.js")

/* Get all tasks */
router.get("/", (req, res) => {
    taskController.getAllTask().then(resultFromController => res.send(resultFromController));
})

/* Create Tasks */
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));

})

/* Delete a task using wildcard on params */
router.delete("/:id", (req, res) => {
    taskController.delTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

/* Update a task */
router.put("/:id", (req, res) => {
    taskController.updTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
