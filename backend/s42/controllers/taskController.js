const Task = require("../models/Task.js");

module.exports.getAllTask = () => {
    return Task.find({}).then(result => {
        return result;
    })
}

module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name
    })
    return newTask.save().then((task, error) => {
        if(error){
            console.log(error);
        }else{
            return task;
        }
    })
}

module.exports.delTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((remTask, error) => {
        if(error){
            console.log(error);
        }else{
            return remTask;
        }
    })
}

module.exports.updTask = (taskId, reqBody) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
            return res.send("Cannot find data with the provided ID");
        }
        result.name = reqBody.name;

        return result.save().then((upddTask, error) => {
            if(error){
                console.log(error);
            return res.send("There is an error while saving the update.")
            }else{
                return upddTask;
            }
        })
    })
}



