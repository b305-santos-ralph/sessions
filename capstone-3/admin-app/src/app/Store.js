import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/user/authSlice";
import customerReducer from "../features/customer/customerSlice";
import productReducer from "../features/product/productSlice";
import brandReducer from "../features/brand/brandSlice";
import productCategoryReducer from "../features/category/categorySlice";
import blogReducer from "../features/blog/blogSlice";
import colorReducer from "../features/color/colorSlice";
import inquiryReducer from '../features/inquiry/inquirySlice'
import uploadReducer from '../features/upload/uploadSlice'
import blogCategoryReducer from "../features/blog-category/blogCategorySlice"

export const store = configureStore({
  reducer: {
    auth: authReducer,
    customer: customerReducer,
    product: productReducer,
    productCategory: productCategoryReducer,
    brand: brandReducer,
    color: colorReducer,
    blog: blogReducer,
    blogCategory: blogCategoryReducer,
    inquiry: inquiryReducer,
    upload: uploadReducer
  },
});
