import React, { useState } from "react";
import { ToastContainer } from "react-toastify";
import { LuLayoutDashboard } from "react-icons/lu";
import { IoPersonOutline } from "react-icons/io5";
import { TbCategory2, TbColorFilter } from "react-icons/tb";
import { HiOutlineClipboardList } from "react-icons/hi";
import {
  BsArrowLeftCircle,
  BsArrowRightCircle,
  BsBell,
  BsBlockquoteRight,
  BsCart,
  BsClipboard2Data,
  BsDashCircle,
  BsNewspaper,
  BsPlusCircle,
  BsTags,
  BsViewList,
} from "react-icons/bs";
import { Layout, Menu, Button, theme } from "antd";
import { Link, Outlet, useNavigate } from "react-router-dom";
import logo from "../images/svg/white-no_bg.svg";
import logo1 from "../images/png/black-nobg.png";

const { Header, Sider, Content } = Layout;

const MainLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          <h2 className="text-center mb-0">
            <img src={logo} alt="" className="img-fluid p-2" />
          </h2>
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[]}
          onClick={({ key }) => {
            if (key === "signout ") {
            } else {
              navigate(key);
            }
          }}
          items={[
            {
              key: "",
              icon: <LuLayoutDashboard className="fs-4" />,
              label: "DASHBOARD",
            },
            {
              key: "customers",
              icon: <IoPersonOutline className="fs-5" />,
              label: "Customers",
            },
            {
              key: "catalog",
              icon: <HiOutlineClipboardList className="fs-5" />,
              label: "Catalog",
              children: [
                {
                  key: "products",
                  icon: <BsCart className="fs-5" />,
                  label: "Products",
                  children: [
                    {
                      key: "add-product",
                      label: "add",
                    },
                    {
                      key: "product-list",
                      label: "show all",
                    },
                  ],
                },
                {
                  key: "brand",
                  icon: <BsTags className="fs-5" />,
                  label: "Brands",
                  children: [
                    {
                      key: "add-brand",
                      label: "add",
                    },
                    {
                      key: "brand-list",
                      label: "show all",
                    },
                  ],
                },
                {
                  key: "category",
                  icon: <TbCategory2 className="fs-5" />,
                  label: "Categories",
                  children: [
                    {
                      key: "add-category",
                      label: "add",
                    },
                    {
                      key: "category-list",
                      label: "show all",
                    },
                  ],
                },
                {
                  key: "color",
                  icon: <TbColorFilter className="fs-5" />,
                  label: "Colors",
                  children: [
                    {
                      key: "add-color",
                      label: "add",
                    },
                    {
                      key: "color-list",
                      label: "show all",
                    },
                  ],
                },
              ],
            },
            {
              key: "orders",
              icon: <BsClipboard2Data className="fs-5" />,
              label: "Orders",
            },
            {
              key: "blogs",
              icon: <BsBlockquoteRight className="fs-5" />,
              label: "Blogs",
              children: [
                {
                  key: "add",
                  icon: <BsPlusCircle />,
                  label: "Add",
                  children: [
                    {
                      key: "add-blog",
                      label: "blog",
                    },
                    {
                      key: "add-blog-category",
                      label: "category",
                    },
                  ],
                },
                {
                  key: "remove",
                  icon: <BsDashCircle />,
                  label: "Remove",
                  children: [
                    {
                      key: "remove-blog",
                      label: "blog",
                    },
                    {
                      key: "remove-blog-category",
                      label: "category",
                    },
                  ],
                },
                {
                  key: "blog-list",
                  icon: <BsViewList />,
                  label: "List",
                },
              ],
            },
            {
              key: "inquiries",
              icon: <BsNewspaper className="fs-5" />,
              label: "Inquiries",
            },
          ]}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="d-flex justify-content-between pe-5"
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          <Button
            type="text"
            icon={collapsed ? <BsArrowRightCircle /> : <BsArrowLeftCircle />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
          <div className="d-flex gap-3 align-items-center">
            <div className="position-relative">
              <BsBell className="fs-4" />
              <span className="position-absolute badge bg-danger rounded-circle">
                3
              </span>
            </div>

            <div className="d-flex gap-2 align-items-center">
              <div>
                <img
                  width={48}
                  height={48}
                  src={logo1}
                  alt=""
                  className="fs-4"
                />
              </div>
              <div
                role="button"
                id="dropdownMenuLink"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <h5 className="mb-0">Cutlery Corner</h5>
                <p className="mb-0">email here</p>
              </div>
              <div className="dropdown-menu mt-2" aria-labelledby="dropdownMenuLink">
                <li className="">
                  <Link to="/" className="dropdown-item" style={{ height: "auto", lineHeight: "14px", fontSize: "12px"}}>Profile</Link>
                </li>
                <li className="">
                  <Link to="/" className="dropdown-item" style={{ height: "auto", lineHeight: "14px", fontSize: "12px"}}>Sign Out</Link>
                </li>
              
              </div>
            </div>
          </div>
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <ToastContainer
          position="bottom-left"
          autoClose={250}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          theme="light"
          />
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
