import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

// Function to fetch a list of colors from the server
const getColors = async () => {
  const response = await axios.get(`${base_url}/color/`);
  return response.data;
};

// Function to create a new color on the server
const createColors = async (color) => {
  const response = await axios.post(
    `${base_url}/color/add`,
    color,
    config
  );
  return response.data;
};

// Function to update an existing color on the server
const updateColor = async (color) => {
  const response = await axios.put(
    `${base_url}/color/${color.id}`,
    { title: color.colorData.title },
    config
  );

  return response.data;
};

// Function to fetch a single color by its ID from the server
const getColor = async (id) => {
  const response = await axios.get(`${base_url}/color/${id}`, config);
  return response.data;
};

// Function to delete a color by its ID on the server
const deleteColor = async (id) => {
  const response = await axios.delete(`${base_url}/color/${id}`, config);
  return response.data;
};

// Object that encapsulates color-related service functions
const colorService = {
  getColors,    // Fetch a list of colors
  createColors, // Create a new color
  getColor,     // Fetch a single color by ID
  updateColor,  // Update an existing color
  deleteColor   // Delete a color by ID
};

export default colorService;
