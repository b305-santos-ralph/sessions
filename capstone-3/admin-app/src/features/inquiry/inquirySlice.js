import { createSlice, createAsyncThunk, createAction } from "@reduxjs/toolkit";
import inquiryService from "./inquiryService";



export const getInquiries = createAsyncThunk(
  "inquiry/get-inquiries",
  async (thunkAPI) => {
    try {
      return await inquiryService.getInquiries();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const deleteInquiry = createAsyncThunk(
  "inquiry/delete-inquiry",
  async (id, thunkAPI) => {
    try {
      return await inquiryService.deleteInquiry(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
export const getInquiry = createAsyncThunk(
  "inquiry/get-inquiry",
  async (id, thunkAPI) => {
    try {
      return await inquiryService.getInquiry(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const updateInquiry = createAsyncThunk(
  "inquiry/update-inquiry",
  async (inq, thunkAPI) => {
    try {
      return await inquiryService.updateInquiry(inq);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
export const resetState = createAction("Reset_all");

const initialState = {
  inquiries: [],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: "",
};
export const inquirySlice = createSlice({
  name: "inquiries",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getInquiries.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getInquiries.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.inquiries = action.payload;
      })
      .addCase(getInquiries.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(deleteInquiry.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteInquiry.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.deletedInquiry = action.payload;
      })
      .addCase(deleteInquiry.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(getInquiry.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getInquiry.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.inqName = action.payload.name;
        state.inqMobile = action.payload.mobile;
        state.inqEmail = action.payload.email;
        state.inqComment = action.payload.comment;
        state.inqStatus = action.payload.status;
      })
      .addCase(getInquiry.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(updateInquiry.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateInquiry.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.updatedInquiry = action.payload;
      })
      .addCase(updateInquiry.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetState, () => initialState);
  },
});
export default inquirySlice.reducer;
