// Import necessary modules and components
import React, { useEffect, useState } from "react";
import CustomInput from "../components/CustomInput";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  createBrands,
  getABrand,
  updateABrand,
  resetState,
} from "../features/brand/brandSlice";

// Define a validation schema using Yup for form validation
let schema = Yup.object().shape({
  title: Yup.string().required("Brand Name is a required field"),
});

// Create a functional React component for adding a brand
const AddBrand = () => {
  const dispatch = useDispatch(); // Redux dispatch function
  const navigate = useNavigate(); // React Router navigation function
  const location = useLocation();
  const getBrandId = location.pathname.split("/")[3];
  const newBrand = useSelector((state) => state.brand); // Access data from Redux store
  const {
    isSuccess,
    isError,
    isLoading,
    createdBrand,
    brandName,
    updatedBrand,
  } = newBrand; // Destructure properties

  useEffect(() => {
    if (getBrandId !== undefined) {
      dispatch(getABrand(getBrandId));
    } else {
      dispatch(resetState());
    }
  }, [getBrandId]);

  // Use useEffect to display toasts based on success and error states
  useEffect(() => {
    if (isSuccess && createdBrand) {
      toast.success("Added Successfully!");
    }
    if (isSuccess && updatedBrand) {
      toast.success("Updated Successfully");
      navigate("/admin/brand-list");
    }
    if (isError) {
      toast.error("Something Went Wrong!");
    }
  }, [isSuccess, isError, isLoading, createdBrand]);

  // Use the useFormik hook to handle form state and validation
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: brandName || "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      if (getBrandId !== undefined) {
        const data = { id: getBrandId, brandData: values };
        dispatch(updateABrand(data));
        dispatch(resetState());
      } else {
        // Dispatch an action to create a new brand
        dispatch(createBrands(values));

        // Reset the form after submission
        formik.resetForm();

        // Navigate to a different route after a delay (e.g., 3 seconds)
        setTimeout(() => {
          dispatch(resetState());
        }, 300);
      }
    },
  });

  return (
    <div>
      <h3 className="mb-4">{getBrandId !== undefined ? "Edit" : "Add"} Brand</h3>
      <div>
        {/* Create a form for adding a brand */}
        <form action="" onSubmit={formik.handleSubmit}>
          <CustomInput
            type="text"
            label="Brand Name"
            name="title"
            onChange={formik.handleChange("title")}
            onBlur={formik.handleBlur("title")}
            value={formik.values.title}
          />
          <div className="error">
            {formik.touched.title && formik.errors.title}
          </div>
          <button
            className="btn btn-secondary border-0 rounded-3 my-4"
            type="submit"
          >
            {getBrandId !== undefined ? "Edit" : "Add"}
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddBrand;
