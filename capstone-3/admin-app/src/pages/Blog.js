import React, { useEffect, useState } from "react";
import { Table, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deleteABlog, getBlogs } from "../features/blog/blogSlice";
import { Link } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import { BiShow } from "react-icons/bi";
import CustomModal from "../components/CustomModal";

// Component for displaying a list of blogs
const Blog = () => {
  // State variables
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [fullDescription, setFullDescription] = useState("");
  const [open, setOpen] = useState(false);
  const [blogId, setBlogId] = useState("");

  // Function to show a modal with full blog description
  const showModal = (e) => {
    setOpen(true);
    setBlogId(e);
  };

  // Function to hide the modal
  const hideModal = () => {
    setOpen(false);
  };

  // Function to display full blog description in a modal
  const handleShowMoreClick = (description) => {
    setFullDescription(description);
    setIsModalVisible(true);
  };

  // Columns configuration for the blog table
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a, b) => a.name.length - b.name.length,
    },
    {
      title: "Description",
      dataIndex: "description",
      render: (text) => {
        const maxDescriptionLength = 0; // Specify the maximum description length to truncate
        const isTruncated = text.length > maxDescriptionLength;
        const displayText = isTruncated
          ? text.slice(0, maxDescriptionLength) + ""
          : text;

        return (
          <div className="icon-container">
            {displayText}
            {isTruncated && (
              <a
                onClick={() => handleShowMoreClick(text)}
                className="show-icon"
              >
                <BiShow />
              </a>
            )}
          </div>
        );
      },
    },
    {
      title: "Category",
      dataIndex: "category",
      sorter: (a, b) => a.category.length - b.category.length,
    },
    {
      title: "Action",
      dataIndex: "action",
    },
  ];

  const dispatch = useDispatch();

  // Fetch blogs from the Redux store when the component mounts
  useEffect(() => {
    dispatch(getBlogs());
  }, []);

  // Retrieve the list of blogs from the Redux store
  const blogState = useSelector((state) => state.blog.blogs);

  // Prepare blog data for display in the table
  const blogData = [];
  for (let i = 0; i < blogState.length; i++) {
    blogData.push({
      key: i + 1,
      name: blogState[i].title,
      description: blogState[i].description,
      category: blogState[i].category,

      // Actions for each blog item, including edit, archive, and delete
      action: (
        <>
          <Link
            to={`/admin/update-blog/${blogState[i]._id}`}
            className="fs-3 ms-3"
          >
            <FiEdit />
          </Link>
          <Link className="fs-3 ms-3 text-warning">
            <FiArchive />
          </Link>
            <button
              className="ms-3 fs-3 px-0 text-danger bg-transparent border-0"
              onClick={() => showModal(blogState[i]._id)}
            >
              <FiDelete />
            </button>
        </>
      ),
    });
  }

  // Function to delete a blog
  const deleteBlog = (e) => {
    dispatch(deleteABlog(e));
    setOpen(false);
    setTimeout(() => {
      dispatch(getBlogs());
    }, 300);
  };

  return (
    <div>
      <h3 className="mb-4">Blogs</h3>
      <div className="tables">
        <Table columns={columns} dataSource={blogData} />
      </div>
      {/* Modal for displaying the full blog description */}
      <Modal
        title="Full Description"
        open={isModalVisible}
        onOk={() => setIsModalVisible(false)}
        onCancel={() => setIsModalVisible(false)}
        okText="Close"
      >
        {fullDescription}
      </Modal>
      {/* Custom modal for confirming blog deletion */}
      <CustomModal
        hideModal={hideModal}
        open={open}
        performAction={() => {
          deleteBlog(blogId);
        }}
        title="Are you sure you want to delete this blog?"
      />
    </div>
  );
};

export default Blog;
