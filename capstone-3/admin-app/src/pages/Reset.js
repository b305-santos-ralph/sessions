import React from "react";
import CustomInput from "../components/CustomInput";
import { Link } from "react-router-dom";

const Reset = () => {
  return (
    <>
      <div
        className="py-5"
        style={{ background: "#131921", minHeight: "100vh" }}
      >
        <br />
        <br />
        <br />
        <br />

        <div className="my-4 w-25 round-3 mx-auto p-4">
          <h3 className="text-center text-white">Reset Password</h3>
          <p className="text-center text-secondary mb-4">
            Please enter new password
          </p>

          <form action="">
            <CustomInput
              type="password"
              label="New Password"
              id="new-password"
            />
            <CustomInput
              type="password"
              label="Confirm Password"
              id="confirm-password"
            />
            <Link
              to="/"
              className="border-0 py-2 px-3 text-white text-center text-decoration-none fs-5 fw-bold w-100 rounded-3 mt-3"
              style={{ background: "#777777" }}
              type="submit"
            >
              Reset Password
            </Link>
          </form>
        </div>
      </div>
    </>
  );
};

export default Reset;
