import React, { useEffect, useState } from "react";
import { Modal, Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deleteAProduct, getProducts } from "../features/product/productSlice";
import { Link } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import { BiShow } from "react-icons/bi";
import CustomModal from "../components/CustomModal";

const Product = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [fullDescription, setFullDescription] = useState("");
  const [open, setOpen] = useState(false);
  const [productId, setProductId] = useState("");

  const showModal = (e) => {
    setOpen(true);
    setProductId(e);
  };

  const hideModal = () => {
    setOpen(false);
  };

  const handleShowMoreClick = (description) => {
    setFullDescription(description);
    setIsModalVisible(true);
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a, b) => a.name.length - b.name.length,
    },
    {
      title: "Description",
      dataIndex: "description",
      render: (text) => {
        const maxDescriptionLength = 0;
        const isTruncated = text.length > maxDescriptionLength;
        const displayText = isTruncated
          ? text.slice(0, maxDescriptionLength) + ""
          : text;

        return (
          <div className="icon-container">
            {displayText}
            {isTruncated && (
              <a
                onClick={() => handleShowMoreClick(text)}
                className="show-icon"
              >
                <BiShow className="" />
              </a>
            )}
          </div>
        );
      },
    },
    {
      title: "Category",
      dataIndex: "category",
      sorter: (a, b) => a.category.length - b.category.length,
    },
    {
      title: "Brand",
      dataIndex: "brand",
      sorter: (a, b) => a.brand.length - b.brand.length,
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "Price",
      dataIndex: "price",
      sorter: (a, b) => a.price - b.price,
    },
    {
      title: "Action",
      dataIndex: "action",
    },
  ];

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, []);

  const productState = useSelector((state) => state.product.products);
  const productData = [];
  for (let i = 0; i < productState.length; i++) {
    if (productState[i].role !== "admin") {
      productData.push({
        key: i + 1,
        name: productState[i].title,
        description: productState[i].description,
        category: productState[i].category,
        brand: productState[i].brand,
        quantity: productState[i].quantity,
        price: productState[i].price,
        action: (
          <>
            <Link
              to={`/admin/update-product/${productState[i]._id}`}
              className="fs-5 ms-3"
            >
              <FiEdit />
            </Link>
            <Link className="fs-5 ms-3 text-warning">
              <FiArchive />
            </Link>
            <button className="fs-5 ms-3 text-danger bg-transparent border-0"
            onClick={() => showModal(productState[i]._id)}
            >
              <FiDelete />
            </button>
          </>
        ),
      });
    }
  }

  const deleteProduct = (e) => {
    dispatch(deleteAProduct(e))
    setOpen(false)
    setTimeout(() => {
      dispatch(getProducts())
    }, 300)
  }

  return (
    <div>
      <h3 className="mb-4">Products</h3>
      <div className="tables">
        <Table columns={columns} dataSource={productData} />
      </div>
      <Modal
        title="Full Description"
        open={isModalVisible}
        onOk={() => setIsModalVisible(false)}
        onCancel={() => setIsModalVisible(false)}
        okText="Close"
      >
        {fullDescription}
      </Modal>
      <CustomModal 
      hideModal={hideModal}
      open={open}
      performAction={() => {
        deleteProduct(productId);
      }}
      title="Are you sure you want to delete this productlog?"
      />
    </div>
  );
};

export default Product;
