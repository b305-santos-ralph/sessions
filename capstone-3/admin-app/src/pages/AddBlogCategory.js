import React, { useEffect, useState } from "react";
import CustomInput from "../components/CustomInput";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  createBlogCategory,
  getBlogCategory,
  updateBlogCategory,
  resetState,
} from "../features/blog-category/blogCategorySlice";

// Define Yup validation schema for form fields
let schema = Yup.object().shape({
  title: Yup.string().required("Please provide blog category"),
});

// Define the main component for adding or editing a blog category
const AddBlogCategory = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const getBlogCategoryId = location.pathname.split("/")[3];
  const newBlogCategory = useSelector((state) => state.blogCategory);
  const {
    isSuccess,
    isError,
    isLoading,
    createdBlogCategory,
    blogCategoryName,
    updatedBlogCategory,
  } = newBlogCategory;

  // Fetch the existing blog category if editing or reset the state for a new category
  useEffect(() => {
    if (getBlogCategoryId !== undefined) {
      dispatch(getBlogCategory(getBlogCategoryId));
    } else {
      dispatch(resetState());
    }
  }, [getBlogCategoryId]);

  // Display success or error messages to the user
  useEffect(() => {
    if (isSuccess && createdBlogCategory) {
      toast.success("Added Successfully!");
    }
    if (isSuccess && updatedBlogCategory) {
      toast.success("Updated Successfully!");
    }
    if (isError) {
      toast.error("Something Went Wrong!");
    }
  }, [isSuccess, isError, isLoading]);

  // Create a formik form for handling form state and submission
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: blogCategoryName || "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      const data = { id: getBlogCategoryId, blogCategoryData: values };
      if (getBlogCategoryId !== undefined) {
        dispatch(updateBlogCategory(data));
        dispatch(resetState());
      } else {
        dispatch(createBlogCategory(values));
        formik.resetForm();
        setTimeout(() => {
          dispatch(resetState());
        }, 300);
      }
    },
  });

  // Render the component
  return (
    <div>
      <h3 className="mb-4 title">
        {getBlogCategoryId !== undefined ? "Edit" : "Add"} Blog Category
      </h3>
      <div>
        <form action="" onSubmit={formik.handleSubmit}>
          <CustomInput
            type="text"
            label="Category Name"
            name="title"
            onChange={formik.handleChange("title")}
            onBlur={formik.handleBlur("title")}
            value={formik.values.title}
            id="blogCategory"
          />
          <div className="error">
            {formik.touched.title && formik.errors.title}
          </div>
          <button
            className="btn btn-secondary border-0 rounded-3 my-4"
            type="submit"
          >
            {getBlogCategoryId !== undefined ? "Edit" : "Add"} Category
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddBlogCategory;
