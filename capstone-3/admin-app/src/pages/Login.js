import React, { useEffect } from "react";
import CustomInput from "../components/CustomInput";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { adminLogin } from "../features/user/authSlice";

let schema = Yup.object().shape({
  email: Yup.string().email().required(),
  password: Yup.string().required(),
});

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      dispatch(adminLogin(values));
    },
  });

  const authState = useSelector((state) => state);
  const { user, isLoading, isError, isSuccess, message } = authState.auth;

  useEffect(() => {
    if (isSuccess) {
      navigate("admin");
    } else {
      navigate("");
    }
  }, [user, isLoading, isError, isSuccess]);

  return (
    <>
      <div
        className="py-5"
        style={{ background: "#131921", minHeight: "100vh" }}
      >
        <br />
        <br />
        <br />
        <br />

        <div className="my-4 w-25 round-3 mx-auto p-4">
          <h3 className="text-center text-white">Login</h3>
          <p className="text-center text-secondary mb-4">
            Please provide admin credentials
          </p>
          <div className="error text-center">
            {message.message == "Rejected" ? "You are not an Admin" : ""}
          </div>

          <form action="" onSubmit={formik.handleSubmit}>
            <CustomInput
              type="email"
              name="name"
              label="Email Address"
              id="email"
              value={formik.values.email}
              onChange={formik.handleChange("email")}
            />
            <div className="error">
              {formik.touched.email && formik.errors.email ? (
                <div>{formik.errors.email}</div>
              ) : null}
            </div>
            <CustomInput
              type="password"
              name="password"
              label="Password"
              id="password"
              value={formik.values.password}
              onChange={formik.handleChange("password")}
            />
            <div className="error">
              {formik.touched.password && formik.errors.password ? (
                <div>{formik.errors.password}</div>
              ) : null}
            </div>
            <div className="mt-4 text-end ">
              <Link to="/forgot-password" className="text-decoration-none">
                Forgot Password?
              </Link>
            </div>
            <button
              className="border-0 py-2 px-3 text-white text-center text-decoration-none fw-bold w-100 rounded-3 mt-3 fs-5"
              style={{ background: "#777777" }}
              type="submit"
            >
              Login
            </button>

            <div className="mt-5">
              <p className="p-0 m-0" style={{ fontSize: "14px", color: "#ffffff" }}>TEST ACCOUNT</p>
              <p className="p-0 m-0 fst-italic" style={{ fontSize: "12px", color: "#ffffff" }}>test@mail.com</p>
              <p className="p-0 m-0 fst-italic" style={{ fontSize: "12px", color: "#ffffff" }}>testuser123</p>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
