// import React from "react";
// import { BsArrowDownShort } from "react-icons/bs";
// import { Column } from "@ant-design/plots";
// import { Table } from 'antd';

// const Dashboard = () => {

//   // CHART - Income Statics
//   const data = [
//     {
//       type: "January",
//       sales: 1,
//     },
//     {
//       type: "February",
//       sales: 2,
//     },
//     {
//       type: "March",
//       sales: 3,
//     },
//     {
//       type: "April",
//       sales: 4,
//     },
//     {
//       type: "May",
//       sales: 5,
//     },
//     {
//       type: "June",
//       sales: 6,
//     },
//     {
//       type: "July",
//       sales: 7,
//     },
//     {
//       type: "August",
//       sales: 8,
//     },
//     {
//       type: "September",
//       sales: 9,
//     },
//     {
//       type: "October",
//       sales: 10,
//     },
//     {
//       type: "November",
//       sales: 11,
//     },
//     {
//       type: "December",
//       sales: 12,
//     },
//   ];
//   const config = {
//     data,
//     xField: "type",
//     yField: "sales",
//     label: {
//       position: "middle",

//       style: {
//         fill: "#FFFFFF",
//         opacity: 0.6,
//       },
//     },
//     xAxis: {
//       label: {
//         autoHide: true,
//         autoRotate: false,
//       },
//     },
//     meta: {
//       type: {
//         alias: "Month",
//       },
//       sales: {
//         alias: "Income",
//       },
//     },
//   };

//   // ORDERS HISTORY 
//   const columns = [
//     {
//       title: 'Name',
//       dataIndex: 'name',
//       filters: [
//         {
//           text: 'Joe',
//           value: 'Joe',
//         },
//         {
//           text: 'Category 1',
//           value: 'Category 1',
//         },
//         {
//           text: 'Category 2',
//           value: 'Category 2',
//         },
//       ],
//       filterMode: 'tree',
//       filterSearch: true,
//       onFilter: (value, record) => record.name.startsWith(value),
//       width: '30%',
//     },
//     {
//       title: 'Age',
//       dataIndex: 'age',
//       sorter: (a, b) => a.age - b.age,
//     },
//     {
//       title: 'Address',
//       dataIndex: 'address',
//       filters: [
//         {
//           text: 'London',
//           value: 'London',
//         },
//         {
//           text: 'New York',
//           value: 'New York',
//         },
//       ],
//       onFilter: (value, record) => record.address.startsWith(value),
//       filterSearch: true,
//       width: '40%',
//     },
//   ];
//   const data1 = [
//     {
//       key: '1',
//       name: 'John Brown',
//       age: 32,
//       address: 'New York No. 1 Lake Park',
//     },
//     {
//       key: '2',
//       name: 'Jim Green',
//       age: 42,
//       address: 'London No. 1 Lake Park',
//     },
//     {
//       key: '3',
//       name: 'Joe Black',
//       age: 32,
//       address: 'Sydney No. 1 Lake Park',
//     },
//     {
//       key: '4',
//       name: 'Jim Red',
//       age: 32,
//       address: 'London No. 2 Lake Park',
//     },
//   ];
//   const onChange = (pagination, filters, sorter, extra) => {
//     console.log('params', pagination, filters, sorter, extra);
//   };

//   return (
//     <div>
//       <h3 className="mb-3">Dashboard</h3>
//       <div className="d-flex justify-content-between align-items-center gap-3">
//         <div className="dash-cards d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3">
//           <div>
//             <p className="">Total</p>
//             <h3 className="mb-0">1000</h3>
//           </div>
//           <div className="d-flex flex-column align-items-end">
//             <h6>
//               <BsArrowDownShort />
//               32%
//             </h6>
//             <p className="mb-0">Compared To October 2023</p>
//           </div>
//         </div>

//         <div className="dash-cards d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3">
//           <div>
//             <p className="">Total</p>
//             <h3 className="mb-0">1000</h3>
//           </div>
//           <div className="d-flex flex-column align-items-end">
//             <h6>
//               <BsArrowDownShort />
//               32%
//             </h6>
//             <p className="mb-0">Compared To October 2023</p>
//           </div>
//         </div>

//         <div className="dash-cards d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3">
//           <div>
//             <p className="">Total</p>
//             <h3 className="mb-0">1000</h3>
//           </div>
//           <div className="d-flex flex-column align-items-end">
//             <h6>
//               <BsArrowDownShort />
//               32%
//             </h6>
//             <p className="mb-0">Compared To October 2023</p>
//           </div>
//         </div>
//       </div>

//       <div className="mt-5">
//         <h3 className="mb-3">Income Statistics</h3>
//         <div className="charts">
//           <Column {...config} />;
//         </div>
//       </div>

//       <div className="mt-5">
//         <h3 className="mb-3">Recent Orders</h3>
//         <div className="tables">
//         <Table columns={columns} dataSource={data1} onChange={onChange} />
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Dashboard;
