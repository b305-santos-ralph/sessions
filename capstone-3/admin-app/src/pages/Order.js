import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getOrders } from "../features/user/authSlice";
import { Link, useNavigate } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import { BiArrowBack } from "react-icons/bi";

const columns = [
  {
    title: "Order ID",
    dataIndex: "order",
  },
  {
    title: "Name",
    dataIndex: "name",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Product",
    dataIndex: "product",
  },
  {
    title: "Amount",
    dataIndex: "amount",
  },
  {
    title: "Date",
    dataIndex: "date",
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Order = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrders());
  }, []);

  const orderState = useSelector((state) => state.auth.orders);

  const ordersData = [];
  for (let i = 0; i < orderState.length; i++) {
      ordersData.push({
        key: i + 1,
        order: orderState[i].orderby._id.slice(-5).toUpperCase(),
        name:
          orderState[i].orderby.firstName.charAt(0).toUpperCase() +
          orderState[i].orderby.firstName.slice(1) +
          " " +
          orderState[i].orderby.lastName.charAt(0).toUpperCase() +
          orderState[i].orderby.lastName.slice(1),
        product: (
          <Link to={`/admin/orders/${orderState[i].orderby._id}`} >
            View Orders
          </Link>
        ),
        amount: orderState[i].payment.amount,
        date: new Date(orderState[i].createdAt).toLocaleDateString(),
        action: (
          <>
            <Link to="/" className="fs-5 ms-2">
              <FiEdit />
            </Link>
            <Link className="fs-5 ms-2 text-warning">
              <FiArchive />
            </Link>
            <Link className="fs-5 ms-2 text-danger">
              <FiDelete />
            </Link>
          </>
        ),
      });
  }

  return (
    <div>
      <h3 className="mb-4">Orders</h3>
      <div className="tables">
        <Table columns={columns} dataSource={ordersData} />
      </div>
    </div>
  );
};

export default Order;
