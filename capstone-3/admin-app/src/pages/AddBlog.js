// Import necessary dependencies and components
import React, { useState, useEffect } from "react";
import CustomInput from "../components/CustomInput";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import Dropzone from "react-dropzone";
import { Stepper } from "react-form-stepper";
import { InboxOutlined } from "@ant-design/icons";
import { deleteImage, uploadImage } from "../features/upload/uploadSlice";
import { Select } from "antd";
import { useNavigate, useLocation } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import {
  createBlogs,
  getABlog,
  updateABlog,
  resetState,
} from "../features/blog/blogSlice";
import { getBlogCategories } from "../features/blog-category/blogCategorySlice";

// Define Yup validation schema for form fields
let schema = Yup.object().shape({
  title: Yup.string().required("Title is a required field"),
  description: Yup.string().required("Description is a required field"),
  category: Yup.string().required("Please select a category"),
});

// Define the main component
const AddBlog = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const getBlogId = location.pathname.split("/")[3];
  const imageState = useSelector((state) => state.upload.images);
  const blogCatState = useSelector((state) => state.blogCategory.categories);
  const blogState = useSelector((state) => state.blog);
  const {
    isSuccess,
    isError,
    isLoading,
    createdBlog,
    blogName,
    blogDescription,
    blogCategory,
    blogImages,
    updatedBlog,
  } = blogState;

  // Fetch a specific blog if editing or reset the state for a new blog
  useEffect(() => {
    if (getBlogId !== undefined) {
      dispatch(getABlog(getBlogId));
      img.push(blogImages);
    } else {
      dispatch(resetState());
    }
  }, [getBlogId]);

  // Fetch blog categories and reset the state
  useEffect(() => {
    dispatch(resetState());
    dispatch(getBlogCategories());
  }, []);

  // Display success or error messages to the user
  useEffect(() => {
    if (isSuccess && createdBlog) {
      toast.success("Added Successfully!");
    }
    if (isSuccess && updatedBlog) {
      toast.success("Updated Successfully!");
      navigate("/admin/blog-list");
    }
    if (isError) {
      toast.error("Something Went Wrong!");
    }
  }, [isSuccess, isError, isLoading]);

  // Initialize an array for storing images
  const img = [];
  if (imageState) {
    imageState.forEach((i) => {
      img.push({
        public_id: i.public_id,
        url: i.url,
      });
    });
  }

  // Update formik's values when images change
  useEffect(() => {
    formik.values.images = img;
  }, [blogImages]);

  // Create a formik form for handling form state and submission
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: blogName || "",
      description: blogDescription || "",
      category: blogCategory || "",
      images: "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      if (getBlogId !== undefined) {
        const data = { id: getBlogId, blogData: values };
        dispatch(updateABlog(data));
        dispatch(resetState());
      } else {
        dispatch(createBlogs(values));
        formik.resetForm();
        setTimeout(() => {
          dispatch(resetState());
        }, 300);
      }
    },
  });

  // Render the component
  return (
    <div>
      <h3 className="mb-4 title">
        {getBlogId !== undefined ? "Edit" : "Add"} Blog
      </h3>

      {/* Display a stepper for tracking form progress */}
      <Stepper
        steps={[
          { label: "Add Blog Details" },
          { label: "Upload Images" },
          { label: "Finish" },
        ]}
        activeStep={1}
      />

      <div className="">
        <form action="" onSubmit={formik.handleSubmit}>
          <div className="mt-4">
            {/* Input field for the blog title */}
            <CustomInput
              type="text"
              label="Enter Blog Name / Title"
              name="title"
              onChange={formik.handleChange("title")}
              onBlur={formik.handleBlur("title")}
              value={formik.values.title}
            />
          </div>
          <div className="error">
            {formik.touched.title && formik.errors.title}
          </div>

          {/* Select field for blog category */}
          <select
            name="category"
            onChange={formik.handleChange("category")}
            onBlur={formik.handleBlur("category")}
            value={formik.values.category}
            className="form-control py-3  mt-3"
            id=""
          >
            <option value="">Select Blog Category</option>
            {blogCatState.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>

          {/* Rich text editor for blog description */}
          <ReactQuill
            className="mb-4 mt-4"
            theme="snow"
            name="description"
            value={formik.values.description}
            onChange={formik.handleChange("description")}
          />
          <div className="error">
            {formik.touched.description && formik.errors.description}
          </div>

          {/* Dropzone for uploading images */}
          <div className="dropzone bg-white border-1 p-5 text-center mt-3">
            <Dropzone
              onDrop={(acceptedFiles) => dispatch(uploadImage(acceptedFiles))}
            >
              {({ getRootProps, getInputProps }) => (
                <section>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <p>
                      Drag 'n' drop some files here, or click to select files
                    </p>
                  </div>
                </section>
              )}
            </Dropzone>
          </div>

          {/* Display uploaded images */}
          <div className="showImages d-flex flex-wrap mt-3 gap-3">
            {imageState?.map((i, j) => {
              return (
                <div className=" position-relative" key={j}>
                  <button
                    type="button"
                    onClick={() => dispatch(deleteImage(i.public_id))}
                    className="btn-close position-absolute"
                    style={{ top: "10px", right: "10px" }}
                  ></button>
                  <img src={i.url} alt="" width={200} height={200} />
                </div>
              );
            })}
          </div>

          {/* Submit button */}
          <button
            className="btn btn-secondary border-0 rounded-3 my-4"
            type="submit"
          >
            {getBlogId !== undefined ? "Edit" : "Add"}
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddBlog;
