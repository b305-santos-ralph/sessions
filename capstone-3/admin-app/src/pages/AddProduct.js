import React, { useState, useEffect } from "react";
import CustomInput from "../components/CustomInput";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
// import { InboxOutlined } from "@ant-design/icons";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import { getBrands } from "../features/brand/brandSlice";
import { getCategories } from "../features/category/categorySlice";
import { getColors } from "../features/color/colorSlice";
import Dropzone from "react-dropzone";
import { deleteImage, uploadImage } from "../features/upload/uploadSlice";
import {
  createProducts,
  getAProduct,
  updateAProduct,
  resetState,
} from "../features/product/productSlice";
import { Select } from "antd";
import { useNavigate, useLocation } from "react-router-dom";

// Define a schema for form validation
let schema = Yup.object().shape({
  title: Yup.string().required("Product Name is a required field"),
  description: Yup.string().required("Product Description is a required field"),
  price: Yup.number().required("Price is a required field"),
  quantity: Yup.number().required("Quantity is a required field"),
  tags: Yup.string().required("Tag is a required field"),
  category: Yup.string().required("Please select a category"),
  brand: Yup.string().required("Please select a brand"),
  color: Yup.array()
    .min(1, "Pick at least one color")
    .required("Color is Required"),
});

const AddProduct = () => {
  // Initialize state and access Redux store
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const getProductId = location.pathname.split("/")[3];
  const [color, setColor] = useState([]);
  const [category, setCategory] = useState([]);
  const [brand, setBrand] = useState([]);
  const [images, setImage] = useState([]);
  const brandState = useSelector((state) => state.brand.brands);
  const colorState = useSelector((state) => state.color.colors);
  const categoryState = useSelector(
    (state) => state.productCategory.categories
  );
  const imageState = useSelector((state) => state.upload.images);
  const productState = useSelector((state) => state.product);
  const {
    isSuccess,
    isError,
    isLoading,
    createdProduct,
    productName,
    productDescription,
    productCategory,
    productBrand,
    productColor,
    productQuantity,
    productPrice,
    productTags,
    productImages,
    updatedProduct,
  } = productState;

  // Fetch product data if editing an existing product
  useEffect(() => {
    if (getProductId !== undefined) {
      // Store product images in 'img' array
      img.push(productImages);
      dispatch(getAProduct(getProductId));
    } else {
      dispatch(resetState());
    }
  }, [getProductId]);

  // Fetch initial data from Redux store
  useEffect(() => {
    dispatch(resetState());
    dispatch(getCategories());
    dispatch(getBrands());
    dispatch(getColors());
  }, []);

  // Handle success and error messages with toast notifications
  useEffect(() => {
    if (isSuccess && createdProduct) {
      toast.success("Added Successfully!");
    }
    if (isSuccess && updatedProduct) {
      toast.success("Updated Successfully!");
      navigate("/admin/product-list");
    }
    if (isError) {
      toast.error("Something Went Wrong!");
    }
  }, [isSuccess, isError, isLoading]);

  // Convert color data for Select component
  const colorOption = [];
  colorState.forEach((i) => {
    colorOption.push({
      label: i.title,
      value: i._id,
    });
  });

  // Initialize 'img' array with image data
  const img = [];
  imageState.forEach((i) => {
    img.push({
      public_id: i.public_id,
      url: i.url,
    });
  });

  // Update form values with image and color data
  useEffect(() => {
    formik.values.images = img;
    formik.values.color = color ? color : " ";
  }, [productImages,img, color]);

  // Define Formik form with validation schema and submission logic
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: productName || "",
      description: productDescription || "",
      price: productPrice || "",
      category: productCategory || "",
      brand: productBrand || "",
      tags: productTags || "",
      color: productColor || "",
      quantity: productQuantity || "",
      images: "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      if (getProductId !== undefined) {
        const data = { id: getProductId, productData: values };
        dispatch(updateAProduct(data));
        dispatch(resetState());
      } else {
        dispatch(createProducts(values));
        formik.resetForm();
        setColor(null);
        setTimeout(() => {
          dispatch(resetState());
        }, 3000);
      }
    },
  });

  // Handle color selection
  const handleColors = (event) => {
    setColor(event);
  };

  return (
    <div>
      <h3 className="mb-4">
        {getProductId !== undefined ? "Edit" : "Add"} Product
      </h3>
      <div>
        <form
          onSubmit={formik.handleSubmit}
          className="d-flex gap-2 flex-column"
        >
          {/* Product Name / Title Input */}
          <div>
            <CustomInput
              type="text"
              label="Product Name / Title"
              value={formik.values.title}
              name="title"
              onChange={formik.handleChange("title")}
              onBlur={formik.handleBlur("title")}
            />
            <div className="error">
              {formik.touched.title && formik.errors.title}
            </div>
          </div>
          {/* Description Input */}
          <div className="">
            <ReactQuill
              placeholder="Description"
              theme="snow"
              value={formik.values.description}
              name="description"
              onChange={formik.handleChange("description")}
              onBlur={formik.handleBlur("description")}
            />
            <div className="error">
              {formik.touched.description && formik.errors.description}
            </div>
          </div>
          {/* Brand Selection Dropdown */}
          <select
            className="form-control py-3 mt-5 dropdown-bar"
            name="brand"
            onChange={formik.handleChange("brand")}
            onBlur={formik.handleBlur("brand")}
            value={formik.values.brand}
          >
            <option>Select Brand</option>
            {brandState.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <div className="error">
            {formik.touched.brand && formik.errors.brand}
          </div>
          {/* Category Selection Dropdown */}
          <select
            className="form-control py-3 mt-5 dropdown-bar"
            name="category"
            onChange={formik.handleChange("category")}
            onBlur={formik.handleBlur("category")}
            value={formik.values.category}
          >
            <option>Select Category</option>
            {categoryState.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <div className="error">
            {formik.touched.category && formik.errors.category}
          </div>
          {/* Color Selection with Select component */}
          <Select
            className="dropdown-bar"
            mode="multiple"
            allowClear
            defaultValue={color}
            placeholder="Select Colors"
            onChange={(i) => handleColors(i)}
            options={colorOption}
          />
          <div className="error">
            {formik.touched.color && formik.errors.color}
          </div>
          {/* Tags Selection Dropdown */}
          <select
            name="tags"
            onChange={formik.handleChange("tags")}
            onBlur={formik.handleBlur("tags")}
            value={formik.values.tags}
            className="form-control"
            id=""
          >
            <option value="" disabled>
              Select Tag
            </option>
            <option value="featured">Featured</option>
            <option value="popular">Popular</option>
            <option value="special">Special</option>
          </select>
          <div className="error">
            {formik.touched.tags && formik.errors.tags}
          </div>
          {/* Price and Quantity Inputs */}
          <div className="d-flex justify-content-between gap-3">
            <div className="flex-grow-1">
              <CustomInput
                type="number"
                label="Price"
                value={formik.values.price}
                name="price"
                onChange={formik.handleChange("price")}
                onBlur={formik.handleBlur("price")}
              />
              <div className="error">
                {formik.touched.price && formik.errors.price}
              </div>
            </div>
            <div className="flex-grow-1">
              <CustomInput
                type="number"
                label="Quantity"
                value={formik.values.quantity}
                name="quantity"
                onChange={formik.handleChange("quantity")}
                onBlur={formik.handleBlur("quantity")}
              />
              <div className="error">
                {formik.touched.quantity && formik.errors.quantity}
              </div>
            </div>
          </div>
          {/* File Upload with Dropzone */}
          <div className="bg-white border-1 p-5 text-center">
            <Dropzone
              onDrop={(acceptedFiles) => dispatch(uploadImage(acceptedFiles))}
            >
              {({ getRootProps, getInputProps }) => (
                <section>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <p>
                      Drag 'n' drop some files here, or click to select files
                    </p>
                  </div>
                </section>
              )}
            </Dropzone>
          </div>
          {/* Display Uploaded Images */}
          <div className="showImage d-flex flex-wrap gap-3">
            {imageState?.map((i, j) => {
              return (
                <div key={j} className="position-relative">
                  <button
                    type="button"
                    onClick={() => dispatch(deleteImage(i.public_id))}
                    className="btn-close position-absolute"
                    style={{ top: "4px", right: "4px" }}
                  ></button>
                  <img
                    src={i.url}
                    alt=""
                    className="img-fluid"
                    width={200}
                    height={200}
                  />
                </div>
              );
            })}
          </div>
          {/* Submit Button */}
          <button
            className="btn btn-secondary border-0 rounded-3 my-4"
            type="submit"
          >
            {getProductId !== undefined ? "Edit" : "Add"}
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddProduct;
