import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getBrands,
  deleteABrand,
  resetState,
} from "../features/brand/brandSlice";
import { Link } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import CustomModal from "../components/CustomModal";

const columns = [
  {
    title: "Serial",
    dataIndex: "serial",
    key: "serial",
  },
  {
    title: "Name",
    dataIndex: "name",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Brand = () => {
  const [open, setOpen] = useState(false);
  const [brandId, setBrandId] = useState("");
  const showModal = (e) => {
    setOpen(true);
    setBrandId(e);
  };
  const hideModal = () => {
    setOpen(false);
  };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(resetState());
    dispatch(getBrands());
  }, []);

  const brandState = useSelector((state) => state.brand.brands);
  const brandData = [];
  for (let i = 0; i < brandState.length; i++) {
    brandData.push({
      key: i + 1,
      name: brandState[i].title,
      serial: brandState[i]._id.slice(-5).toUpperCase(),
      action: (
        <>
          <Link
            to={`/admin/update-brand/${brandState[i]._id}`}
            className="fs-3 ms-3"
          >
            <FiEdit />
          </Link>
          <Link className="fs-3 ms-3 text-warning">
            <FiArchive />
          </Link>
          <button className="ms-3 fs-3 px-0 text-danger bg-transparent border-0"
          onClick={() => showModal(brandState[i]._id)}
          >
          <FiDelete />
          </button>   
        </>
      ),
    });
  }
  const deleteBrand = (e) => {
    dispatch(deleteABrand(e))
    setOpen(false)
    setTimeout(() => {
      dispatch(getBrands())
    }, 300)
  }

  return (
    <div>
      <h3 className="mb-4">Brands</h3>
      <div className="tables">
        <Table columns={columns} dataSource={brandData} />
      </div>
      <CustomModal 
      hideModal={hideModal}
      open={open}
      performAction={() => {
        deleteBrand(brandId)
      }}
      title="Are you sure you want to delete this brand?"
      />
    </div>
  );
};

export default Brand;
