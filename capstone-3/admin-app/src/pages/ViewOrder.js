import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { FiDelete, FiEdit } from 'react-icons/fi'
import { Link, useLocation, useNavigate } from "react-router-dom";
import { getOrderByUser } from "../features/user/authSlice";

const columns = [
  {
    title: "SNo",
    dataIndex: "key",
  },
  {
    title: "Product Name",
    dataIndex: "name",
  },
  {
    title: "Brand",
    dataIndex: "brand",
  },
  {
    title: "Count",
    dataIndex: "count",
  },
  {
    title: "Color",
    dataIndex: "color",
  },
  {
    title: "Amount",
    dataIndex: "amount",
  },
  {
    title: "Date",
    dataIndex: "date",
  },

  {
    title: "Action",
    dataIndex: "action",
  },
];

const ViewOrder = () => {
  const location = useLocation();
  const userId = location.pathname.split("/")[3];
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrderByUser(userId));
  }, []);

  const orderState = useSelector((state) => state.auth.orderByUser.products);
  console.log(orderState);
  const orderData = [];
  for (let i = 0; i < orderState.length; i++) {
    orderData.push({
      key: i + 1,
      name: orderState[i].product.title,
      brand: orderState[i].product.brand,
      count: orderState[i].count,
      amount: orderState[i].product.price,
      color: orderState[i].product.color,
      date: new Date(orderState[i].product.createdAt).toLocaleDateString(),
      action: (
        <>
          <Link to="/" className=" fs-3 text-danger">
            <FiEdit />
          </Link>
          <Link className="ms-3 fs-3 text-danger" to="/">
            <FiDelete />
          </Link>
        </>
      ),
    });
  }
  return (
    <div>
      <h3 className="mb-4 title">View Order</h3>
      <h5></h5>
      <div>
        <Table columns={columns} dataSource={orderData} />
      </div>
    </div>
  );
};

export default ViewOrder;
