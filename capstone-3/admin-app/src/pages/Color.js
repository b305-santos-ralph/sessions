import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deleteAColor, getColors } from "../features/color/colorSlice";
import { Link } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import CustomModal from "../components/CustomModal";

// Define table columns for displaying colors
const columns = [
  {
    title: "Serial",
    dataIndex: "serial",
    key: "serial",
  },
  {
    title: "Name",
    dataIndex: "name",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Color = () => {
  // State variables for controlling the delete confirmation modal
  const [open, setOpen] = useState(false);
  const [colorId, setColorId] = useState("");

  // Function to show the delete confirmation modal
  const showModal = (event) => {
    setOpen(true);
    setColorId(event);
  };

  // Function to hide the delete confirmation modal
  const hideModal = () => {
    setOpen(false);
  };

  const dispatch = useDispatch();

  // Fetch colors from the Redux store when the component mounts
  useEffect(() => {
    dispatch(getColors());
  }, []);

  // Get color data from the Redux store
  const colorState = useSelector((state) => state.color.colors);
  const colorData = [];
  for (let i = 0; i < colorState.length; i++) {
    colorData.push({
      key: i + 1,
      name: colorState[i].title,
      serial: colorState[i]._id.slice(-5).toUpperCase(),
      action: (
        <>
          <Link to={`/admin/update-color/${colorState[i]._id}`} className="fs-3 ms-3">
            <FiEdit />
          </Link>
          <button
            className="ms-3 fs-3 px-0 text-danger bg-transparent border-0"
            onClick={() => showModal(colorState[i]._id)}
          >
            <FiDelete />
          </button>
        </>
      ),
    });
  }

  // Function to delete a color
  const deleteColor = (event) => {
    dispatch(deleteAColor(event));
    setOpen(false);
    setTimeout(() => {
      dispatch(getColors());
    }, 100);
  };

  return (
    <div>
      <h3 className="mb-4">Colors</h3>
      <div className="tables">
        <Table columns={columns} dataSource={colorData} />
      </div>

      {/* Custom modal for confirming color deletion */}
      <CustomModal
        hideModal={hideModal}
        open={open}
        performAction={() => {
          deleteColor(colorId);
        }}
        title="Are you sure you want to delete this color?"
      />
    </div>
  );
};

export default Color;
