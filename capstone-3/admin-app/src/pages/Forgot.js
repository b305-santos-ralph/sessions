import React from "react";
import CustomInput from "../components/CustomInput";

const Forgot = () => {
  return (
    <div className="py-5" style={{ background: "#131921", minHeight: "100vh" }}>
      <br />
      <br />
      <br />
      <br />

      <div className="my-4 w-25 round-3 mx-auto p-4">
        <h3 className="text-center text-white">Forgot Password ?</h3>
        <p className="text-center text-secondary mb-4">Please enter your registered email</p>

        <form action="">
          <CustomInput type="email" label="Email Address" id="email" />
    
          <button
            className="border-0 py-2 px-3 text-white fw-bold w-100 rounded-3 mt-3 fs-5"
            style={{ background: "#777777" }}
            type="submit"
          >
            Submit
          </button>
        </form>
      </div>
      </div>
  );
};

export default Forgot;
