import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategories,
  deleteAProductCategory,
  resetState,
} from "../features/category/categorySlice";
import { Link } from "react-router-dom";
import { FiArchive, FiDelete, FiEdit } from "react-icons/fi";
import CustomModal from "../components/CustomModal";

const columns = [
  {
    title: "Serial",
    dataIndex: "serial",
    key: "serial",
  },
  {
    title: "Name",
    dataIndex: "name",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Category = () => {
  const [open, setOpen] = useState(false);
  const [pCatId, setPCatId] = useState("");
  const showModal = (e) => {
    setOpen(true);
    setPCatId(e);
  };
  const hideModal = () => {
    setOpen(false);
  };

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategories());
    dispatch(resetState());
  }, []);

  const categoryState = useSelector(
    (state) => state.productCategory.categories
  );
  const categoryData = [];
  for (let i = 0; i < categoryState.length; i++) {
    categoryData.push({
      key: i + 1,
      name: categoryState[i].title,
      serial: categoryState[i]._id.slice(-5).toUpperCase(),
      action: (
        <>
          <Link
            to={`/admin/update-category/${categoryState[i]._id}`}
            className="fs-3 ms-3"
          >
            <FiEdit className=""/>
          </Link>
          <Link className="fs-3 ms-3 text-warning">
            <FiArchive />
          </Link>
          <button
            className="ms-3 fs-3 text-danger bg-transparent border-0 px-0"
            onClick={() => showModal(categoryState[i]._id)}
          >
            <FiDelete />
          </button>
        </>
      ),
    });
  }

  const deleteCategory = (e) => {
    dispatch(deleteAProductCategory(e));
    setOpen(false);
    setTimeout(() => {
      dispatch(getCategories());
    }, 100);
  };

  return (
    <div>
      <h3 className="mb-4">Product Categories</h3>
      <div className="tables">
        <Table columns={columns} dataSource={categoryData} />
      </div>
      <CustomModal 
      hideModal={hideModal}
      open={open}
      performAction={() => {
        deleteCategory(pCatId)
      }}
      title="Are you sure you want to delete this Category?"
      />
    </div>
  );
};

export default Category;
