import React, { useEffect, useState } from "react";
import { Table, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getInquiries,
  deleteInquiry,
  updateInquiry,
  resetState,
} from "../features/inquiry/inquirySlice"
import { Link } from "react-router-dom";
import { FiEye, FiDelete, } from "react-icons/fi";
import { BiShow } from "react-icons/bi";
import CustomModal from "../components/CustomModal"

const columns = [
  {
    title: "SNo",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Email",
    dataIndex: "email",
  },
  {
    title: "Mobile",
    dataIndex: "mobile",
  },
  {
    title: "Status",
    dataIndex: "status",
  },

  {
    title: "Action",
    dataIndex: "action",
  },
];

const Inquiry = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [inqId, setInqId] = useState("");
  const showModal = (e) => {
    setOpen(true);
    setInqId(e);
  };
  const hideModal = () => {
    setOpen(false);
  };
  useEffect(() => {
    dispatch(resetState())
    dispatch(getInquiries());
  });

  const inquiryState = useSelector((state) => state.inquiry.inquiries);
  const inquiryData = [];
  for (let i = 0; i < inquiryState.length; i++) {
    inquiryData.push({
      key: i + 1,
      name: inquiryState[i].name,
      email: inquiryState[i].email,
      mobile: inquiryState[i].mobile,
      date: new Date(inquiryState[i].createdAt).toLocaleDateString(),
      status: (
        <>
          <select
            name=""
            id=""
            className="form-control form-select"
            defaultValue={
              inquiryState[i].status ? inquiryState[i].status : "Submitted"
            }
            onChange={(e) =>
              setInquiryStatus(e.target.value, inquiryState[i]._id)
            }
          >
            <option value="Submitted">Submitted</option>
            <option value="Contacted">Contacted</option>
            <option value="In Progress">In Progress</option>
            <option value="Resolved">Resolved</option>
          </select>
        </>
      ),
      action: (
        <>
          <Link
            className="ms-3 fs-3 text-danger"
            to={`/admin/inquiries/${inquiryState[i]._id}`}
          >
            <BiShow />
          </Link>
          <button
            className="ms-3 fs-3 text-danger bg-transparent border-0"
            onClick={() => showModal(inquiryState[i]._id)}
          >
            <FiDelete />
          </button>
        </>
      ),
    });
  }

  const setInquiryStatus = (e, i) => {
    const data = { id: i, inquiryData: e}
    dispatch(updateInquiry(data))
  }
  const deleteInquiry = (e) => {
    dispatch(deleteInquiry(e))
    setOpen(false)
    setTimeout(() => {
      dispatch(getInquiries())
    }, 300)
  }

  return (
    <div>
      <h3 className="mb-4">Inquiries</h3>
      <div className="tables">
        <Table columns={columns} dataSource={inquiryData} />
      </div>
      <CustomModal
        hideModal={hideModal}
        open={open}
        performAction={() => {
          deleteInquiry(inqId);
        }}
        title="Are you sure you want to delete this inquiry?"
      />
    </div>
  );
};

export default Inquiry;
