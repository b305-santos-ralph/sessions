import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./pages/Login";
import Reset from "./pages/Reset";
import Forgot from "./pages/Forgot";
import MainLayout from "./components/MainLayout";
import Dashboard from "./pages/Dashboard";
import Inquiry from "./pages/Inquiry";
import Blog from "./pages/Blog";
import Order from "./pages/Order";
import Customer from "./pages/Customer";
import Color from "./pages/Color";
import Brand from "./pages/Brand";
import Category from "./pages/Category";
import Product from "./pages/Product";
import AddBlog from "./pages/AddBlog";
import AddBlogCategory from "./pages/AddBlogCategory";
import AddColor from "./pages/AddColor";
import AddProductCategory from "./pages/AddCategory";
import AddBrand from "./pages/AddBrand";
import AddProduct from "./pages/AddProduct";
import ViewInquiries from "./pages/ViewInquiries";
import ViewOrder from "./pages/ViewOrder";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/reset-password" element={<Reset />} />
        <Route path="/forgot-password" element={<Forgot />} />
        <Route path="/admin" element={<MainLayout />}>
          {/* <Route index element={<Dashboard />} /> */}
          <Route path="customers" element={<Customer />} />

          {/* Product */}
          <Route path="product-list" element={<Product />} />
          <Route path="add-product" element={<AddProduct />} />
          <Route path="update-product/:id" element={<AddProduct />} />

          {/* Product Category */}
          <Route path="add-category" element={<AddProductCategory />} />
          <Route path="update-category/:id" element={<AddProductCategory />} />
          <Route path="category-list" element={<Category />} />

          {/* Brand */}
          <Route path="brand-list" element={<Brand />} />
          <Route path="add-brand" element={<AddBrand />} />
          <Route path="update-brand/:id" element={<AddBrand />} />

          {/* Color */}
          <Route path="color-list" element={<Color />} />
          <Route path="add-color" element={<AddColor />} />
          <Route path="update-color/:id" element={<AddColor />} />

          {/* Order */}
          <Route path="orders" element={<Order />} />
          <Route path="orders/:id" element={<ViewOrder />} />

          {/* Blog */}
          <Route path="blog-list" element={<Blog />} />
          <Route path="add-blog" element={<AddBlog />} />
          <Route path="update-blog/:id" element={<AddBlog />} />
          <Route path="add-blog-category" element={<AddBlogCategory />} />
          <Route path="add-blog-category/:id" element={<AddBlogCategory />} />

          {/* Inquiry */}
          <Route path="inquiries" element={<Inquiry />} />
          <Route path="inquiries/:id" element={<ViewInquiries />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
