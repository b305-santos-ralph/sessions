const express = require("express");
const app = express();
const dotenv = require("dotenv").config();
const PORT = 5000;
const cors = require("cors");
const dbConnect = require("./config/dbConnect");
const { notFound, errorHandler } = require("./middlewares/errorHandler");
const morgan = require("morgan");

const authRouter = require("./routes/authRoute");
const productRouter = require("./routes/productRoute");
const blogRouter = require("./routes/blogRoute");
const categoryRouter = require("./routes/categoryRoute");
const blogCategoryRouter = require("./routes/blogCategoryRoute");
const brandRouter = require("./routes/brandRoute");
const cartRouter = require("./routes/cartRoute");
const colorRouter = require("./routes/colorRoute");
const inquiryRouter = require("./routes/inquiryRoute");
const uploadRouter = require("./routes/uploadRoute");

// Invocation of db connection
dbConnect();

app.use(cors());

// Morgan
app.use(morgan("dev"));

// Parser
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
app.use("/user", authRouter);
app.use("/products", productRouter);
app.use("/blog", blogRouter);
app.use("/product-category", categoryRouter);
app.use("/blog-category", blogCategoryRouter);
app.use("/brands", brandRouter);
app.use("/cart", cartRouter);
app.use("/color", colorRouter);
app.use("/inquiry", inquiryRouter);
app.use("/images", uploadRouter);

// Error Handler
app.use(notFound);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Server is running at PORT ${PORT}`);
});
