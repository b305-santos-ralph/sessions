const express = require('express');
const router = express.Router()
const categoryController = require('../controllers/categoryController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create Category
router.post('/add', authMiddleware, isAdmin, categoryController.createCategory)

// Update Category
router.put('/:id', authMiddleware, isAdmin, categoryController.updateCategory)

// Delete Category
router.delete('/:id', authMiddleware, isAdmin, categoryController.deleteCategory)

// Get Single Category
router.get('/:id', categoryController.getCategory)

// Get All Category
router.get('/', categoryController.getAllCategory)

module.exports = router