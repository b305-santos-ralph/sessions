const express = require('express');
const router = express.Router()
const colorController = require('../controllers/colorController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create Color
router.post('/add', authMiddleware, isAdmin, colorController.createColor)

// Update Color
router.put('/:id', authMiddleware, isAdmin, colorController.updateColor)

// Delete Color
router.delete('/:id', authMiddleware, isAdmin, colorController.deleteColor)

// Get Single Color
router.get('/:id', colorController.getColor)

// Get All Color
router.get('/', colorController.getAllColor)

module.exports = router