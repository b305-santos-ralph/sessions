const express = require('express')
const router = express.Router();
const userController = require('../controllers/userController');
const { authMiddleware, isAdmin } = require('../middlewares/authMiddleware');



// User Signup
router.post('/signup', userController.createUser)

// User Login
router.post('/login', userController.loginUser)

// Admin Login
router.post('/admin-login', userController.loginAdmin)

// User Logout
router.get('/logout', userController.logoutUser )

// Delete A User
router.delete('/:id', userController.deleteUser)

// Handle Refresh Token
router.get('/refresh', userController.handleRefreshToken )

// Get Wishlist
router.get('/wishlist', authMiddleware, userController.getWishlist)





// Get All Users
router.get('/all-users', userController.getAllUser)

// Get Single User
router.get('/:id', authMiddleware, isAdmin, userController.getUser)

// Update A User
router.put('/edit', authMiddleware, userController.updateUser)

// Save User Address
router.put('/save-address', authMiddleware, userController.saveAddress)

// Block User
router.put('/block-user/:id', authMiddleware, isAdmin, userController.blockUser )

// Unblock User
router.put('/unblock-user/:id', authMiddleware, isAdmin, userController.unblockUser)




// Change Password
router.put('/change-password', authMiddleware, userController.updatePassword) 

// Forgot / Reset Password
router.post('/password-token', userController.forgotPasswordToken )
router.put('/reset-password/:token', userController.resetPassword)

module.exports = router;