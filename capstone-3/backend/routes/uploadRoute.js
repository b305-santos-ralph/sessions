const express = require('express');
const router = express.Router();
const uploadController = require('../controllers/uploadController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');
const uploadImg = require('../middlewares/uploadImg');


// Upload Image to Cloudinary
router.post('/upload/', authMiddleware, isAdmin, 
    uploadImg.uploadPhoto.array('images', 10), 
    uploadImg.productImageResize, 
    uploadController.uploadImages)


// Delete Images from Cloudinary
router.delete('/delete-img/:id', authMiddleware, isAdmin, uploadController.deleteImages)

module.exports = router;