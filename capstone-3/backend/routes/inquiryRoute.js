const express = require('express');
const router = express.Router()
const inquiryController = require('../controllers/inquiryController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create Inquiry
router.post('/', inquiryController.createInquiry)

// Update Inquiry
router.put('/:id', authMiddleware, isAdmin, inquiryController.updateInquiry)

// Get Single Inquiry
router.get('/:id', inquiryController.getInquiry)

// Delete An Inquiry
router.delete('/:id', authMiddleware, isAdmin, inquiryController.getInquiry)

// Get All Inquiry
router.get('/', inquiryController.getAllInquiry)

module.exports = router