const express = require('express');
const router = express.Router()
const brandController = require('../controllers/brandController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create Brand
router.post('/add', authMiddleware, isAdmin, brandController.createBrand)

// Update Brand
router.put('/:id', authMiddleware, isAdmin, brandController.updateBrand)

// Delete Brand
router.delete('/:id', authMiddleware, isAdmin, brandController.deleteBrand)

// Get Single Brand
router.get('/:id', brandController.getBrand)

// Get All Brand
router.get('/', brandController.getAllBrand)

module.exports = router