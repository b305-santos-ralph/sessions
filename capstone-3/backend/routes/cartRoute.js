const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");

// Add to Cart
router.post("/add-cart", authMiddleware, cartController.userCart);

// Get User Cart
router.get("/view-cart", authMiddleware, cartController.getUserCart);

// Empty or Remove Cart
router.delete("/remove-cart", authMiddleware, cartController.emptyCart);

// Remove Item from Cart
router.delete("/remove-item-cart/:cartItemId", authMiddleware, cartController.removeProductFromCart);

// Update Item from Cart
router.delete("/update-item-cart/:cartItemId/:newQuantity", authMiddleware, cartController.updateCartProductQuantity);

// Get Single Order
router.post("/order/:id", authMiddleware, isAdmin, cartController.getOrder);

// Create Order
router.post("/order", authMiddleware, cartController.createOrder);

// Get User Orders
router.get("/order/history", authMiddleware, cartController.getOrders);

// Get All Orders
router.get("/all-orders", authMiddleware, isAdmin, cartController.getAllOrders);



// Update Order Status
router.put(
  "/order/status/:id",
  authMiddleware,
  isAdmin,
  cartController.updateOrder
);

module.exports = router;
