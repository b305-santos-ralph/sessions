const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create A Product
router.post('/add', authMiddleware, isAdmin, productController.createProduct )

// Get A Product
router.get('/:id', productController.getProduct)

// Add / Remove to Wishlist
router.put('/wishlist', authMiddleware, productController.addToWishlist)

// Rating Product
router.put('/ratings', authMiddleware, productController.rating)

// Get All Product
router.get('/', productController.getAllProduct)

// Update A Product
router.put('/:id', authMiddleware, isAdmin, productController.updateProduct)

// Delete A Product
router.delete('/:id', authMiddleware, isAdmin, productController.deleteProduct)


module.exports = router;