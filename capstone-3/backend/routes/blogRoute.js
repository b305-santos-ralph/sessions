const express = require('express');
const { authMiddleware, isAdmin } = require('../middlewares/authMiddleware');
const router = express.Router()
const blogController = require('../controllers/blogController')
const uploadImg = require('../middlewares/uploadImg');


// Create A Blog
router.post('/add', authMiddleware, isAdmin, blogController.createBlog )

//
router.put('/upload/:id', authMiddleware, isAdmin, 
    uploadImg.uploadPhoto.array('images', 10), 
    uploadImg.productImageResize, 
    blogController.uploadImages)

// Like Blog
router.put('/likes', authMiddleware, blogController.likeBlog)

// Dislike Blog
router.put('/dislikes', authMiddleware, blogController.dislikeBlog)

// Update Blog
router.put('/:id', authMiddleware, isAdmin, blogController.updateBlog )

// Get Blog
router.get('/:id', blogController.getBlog)

// Get All Blogs
router.get('/', blogController.getAllBlogs)

// Delete A Blog
router.delete('/:id', authMiddleware, isAdmin, blogController.deleteBlog);



module.exports = router; 