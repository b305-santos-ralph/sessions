const express = require('express');
const router = express.Router()
const blogCategoryController = require('../controllers/blogCategoryController');
const { isAdmin, authMiddleware } = require('../middlewares/authMiddleware');


// Create Category
router.post('/add', authMiddleware, isAdmin, blogCategoryController.createCategory)

// Update Category
router.put('/:id', authMiddleware, isAdmin, blogCategoryController.updateCategory)

// Delete Category
router.delete('/:id', authMiddleware, isAdmin, blogCategoryController.deleteCategory)

// Get Single Category
router.get('/:id', blogCategoryController.getCategory)

// Get All Category
router.get('/', blogCategoryController.getAllCategory)

module.exports = router