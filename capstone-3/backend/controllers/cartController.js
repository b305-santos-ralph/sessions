const User = require("../models/User");
const Cart = require("../models/Cart");
const Order = require("../models/Order");
const Product = require("../models/Product");
const asyncHandler = require("express-async-handler");
const validateMongodbId = require("../utils/validateMongodbId");
const jwt = require("jsonwebtoken");
const sendEmail = require("./emailController");
const crypto = require("crypto");
const uniqid = require("uniqid");

// User Cart
const userCart = asyncHandler(async (req, res) => {
  const { productId, color, quantity, price } = req.body;
  const { _id } = req.user;
  validateMongodbId(_id);
  try {
    let newCart = await new Cart({
      userId: _id,
      productId,
      color,
      price,
      quantity,
    }).save();
    res.json(newCart);
  } catch (error) {
    throw new Error(error);
  }
});

// Fetch User Cart
const getUserCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongodbId(_id);
  try {
    const cart = await Cart.find({ userId: _id })
      .populate("productId")
      .populate("color");
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

// Empty or Remove Cart
const emptyCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongodbId(_id);

  try {
    const user = await User.findOne({ _id });
    const cart = await Cart.findOneAndRemove({ orderby: user._id });
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

// Delete product from Cart
const removeProductFromCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { cartItemId } = req.params;
  validateMongodbId(_id);

  try {
    const deleteProduct = await Cart.deleteOne({ userId: _id, _id: cartItemId });
    res.json(deleteProduct);
  } catch (error) {
    throw new Error(error);
  }
});

// Update cart product quantity
const updateCartProductQuantity = asyncHandler (async (req,res) => {
  const { _id } = req.user;
  const { cartItemId, newQuantity } = req.params;
  validateMongodbId(_id);

  try {
    const cartItem = await Cart.findOne({ userId: _id, _id: cartItemId });
    cartItem.quantity = newQuantity
    cartItem.save()
    res.json(cartItem);
  } catch (error) {
    throw new Error(error);
  }
})

// Create Order
const createOrder = asyncHandler(async (req, res) => {
  const { COD, couponApplied } = req.body;
  const { _id } = req.user;
  validateMongodbId(_id);

  try {
    if (!COD) throw new Error("Failed to Create Cash Order");
    const user = await User.findById(_id);
    let userCart = await Cart.findOne({ orderby: user._id });
    let finalAmount = 0;
    if (couponApplied && userCart.totalAfterDiscount) {
      finalAmount = userCart.totalAfterDiscount;
    } else {
      finalAmount = userCart.cartTotal;
    }

    let newOrder = await new Order({
      products: userCart.products,
      payment: {
        id: uniqid(),
        method: "COD",
        amount: finalAmount,
        status: "Pending",
        created: Date.now(),
        currency: "PHP",
      },
      orderby: user._id,
      orderStatus: "Pending",
    }).save();

    let update = userCart.products.map((item) => {
      return {
        updateOne: {
          filter: { _id: item.product._id },
          update: { $inc: { quantity: -item.count, sold: +item.count } },
        },
      };
    });

    const updated = await Product.bulkWrite(update, {});
    res.json({ message: "success" });
  } catch (error) {
    throw new Error(error);
  }
});

// Retrieve Order History
const getOrders = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongodbId(_id);

  try {
    const userOrders = await Order.findOne({ orderby: _id })
      .populate("products.product")
      .populate("orderby")
      .exec();

    res.json(userOrders);
  } catch (error) {
    throw new Error(error);
  }
});

// Get All Orders
const getAllOrders = asyncHandler(async (req, res) => {
  try {
    const allUserOrders = await Order.find()
      .populate("products.product")
      .populate("orderby")
      .exec();

    res.json(allUserOrders);
  } catch (error) {
    throw new Error(error);
  }
});

// Get Single Order
const getOrder = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongodbId(id);

  try {
    const userOrders = await Order.findOne({ orderby: id })
      .populate("products.product")
      .populate("orderby")
      .exec();

    res.json(userOrders);
  } catch (error) {
    throw new Error(error);
  }
});

// Update Order Status
const updateOrder = asyncHandler(async (req, res) => {
  const { status } = req.body;
  const { id } = req.params;
  validateMongodbId(id);

  try {
    const updatedOrder = await Order.findByIdAndUpdate(
      id,
      {
        orderStatus: status,
        payment: {
          status: status,
        },
      },
      {
        new: true,
      }
    );
    res.json(updatedOrder);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  userCart,
  getUserCart,
  emptyCart,
  createOrder,
  getOrders,
  updateOrder,
  getAllOrders,
  getOrder,
  removeProductFromCart,
  updateCartProductQuantity
};
