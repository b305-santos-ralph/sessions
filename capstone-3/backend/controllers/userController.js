const User = require('../models/User')
const asyncHandler = require('express-async-handler')
const { generateToken } = require('../config/jwtToken')
const validateMongodbId = require('../utils/validateMongodbId')
const { generateRefreshToken } = require('../config/refreshToken') 
const jwt = require('jsonwebtoken')
const sendEmail = require('./emailController')
const crypto = require('crypto')



// User Signup
const createUser = asyncHandler (
    async (req, res) => {
        const email = req.body.email;
        const findUser = await User.findOne({email});
        if (!findUser) {
            // Create a new user
            const newUser = await User.create(req.body);
            res.json(newUser)
    
        } else {
            // User Already Exists
            throw new Error ('User Already Exists')
        }
    }
)


// User Login
const loginUser = asyncHandler ( async (req,res) => {
    const { email, password } = req.body;
    
    // check if user exists or not
    const findUser = await User.findOne({email});
    if (findUser && await findUser.isPasswordMatched(password)) {
        const refreshToken = await generateRefreshToken(findUser?._id);
        const updateuser = await User.findByIdAndUpdate(findUser._id, 
            {
                refreshToken: refreshToken
            },{
                new: true
            } )
        res.cookie('refreshToken', refreshToken, {
            httpOnly: true,
            maxAge: 72 * 60 * 60 * 1000,
        });
        res.json({
            _id: findUser?._id,
            firstName: findUser.firstName,
            lastName: findUser.lastName,
            email: findUser.email,
            mobile: findUser.mobile,
            token: generateToken(findUser?._id)
        })
    } else {
        throw new Error ('Invalid User Credentials')
    }
})


//Admin Login
const loginAdmin = asyncHandler(async (req, res) => {
    const { email, password } = req.body;
    // check if user exists or not
    const findAdmin = await User.findOne({ email });
    if (findAdmin.role !== "admin") throw new Error("Not Authorized");
    if (findAdmin && (await findAdmin.isPasswordMatched(password))) {
      const refreshToken = await generateRefreshToken(findAdmin?._id);
      const updateuser = await User.findByIdAndUpdate(
        findAdmin.id,
        {
            refreshToken: refreshToken,
        },{
            new: true 
        }
      );
      res.cookie("refreshToken", refreshToken, {
        httpOnly: true,
        maxAge: 72 * 60 * 60 * 1000,
      });
      res.json({
        _id: findAdmin?._id,
        firstname: findAdmin?.firstname,
        lastname: findAdmin?.lastname,
        email: findAdmin?.email,
        mobile: findAdmin?.mobile,
        token: generateToken(findAdmin?._id),
      });
    } else {
        throw new Error("Invalid Credentials");
    }
  });


    // handle refresh token
    const handleRefreshToken = asyncHandler ( async (req, res) => {

        const cookie = req.cookies;
        if(!cookie?.refreshToken) throw new Error ('No Refresh Token in Cookies');
        const refreshToken = cookie.refreshToken;
   

        const user = await User.findOne({refreshToken});
        if(!user) throw new Error ('No Refresh Token present in database');
        jwt.verify(refreshToken, process.env.JWT_SECRET, (error, decoded) => {
            if (error || user.id !== decoded.id) {
                throw new Error ('There is something wrong with the refresh token');
            }
            const accessToken = generateToken(user?._id)
            res.json({accessToken})
        })
    })

    
// Logout User
const logoutUser = asyncHandler ( async (req, res) => {
    const cookie = req.cookies;
    if (!cookie?.refreshToken) throw new Error ('No Refresh Token in Cookies');
    const refreshToken = cookie.refreshToken;
    const user = await User.findOne({refreshToken});
    if (!user) {
        res.clearCookie('refreshToken', {
            httpOnly: true,
            secure: true,
        });
        return res.sendStatus(204); //forbidden
    }
    await User.findOneAndUpdate({refreshToken}, {
        refreshToken: ""
    });
    res.clearCookie('refreshToken', {
        httpOnly: true,
        secure: true,
    });
    return res.sendStatus(204); //forbidden
});


// Save User Address
const saveAddress = asyncHandler ( async (req, res, next) => {
    const { _id} = req.user;
    validateMongodbId(_id);

    try {
        const userAddress = await User.findByIdAndUpdate( _id, {
            address: req?.body?.address
        },
        {
            new: true
        });
        res.json(userAddress)
    } catch (error) {
        throw new Error(error)
    }
})


// Get All Users
const getAllUser = asyncHandler ( async (req, res) => {
    try {
        const getUsers = await User.find();
        res.json(getUsers);
    } catch (error) {
        throw new Error (error);
    }
})


// Get Single User
const getUser = asyncHandler ( async (req, res) => {

    const {id} = req.params;
    validateMongodbId(id);

    try {
        const fetchUser = await User.findById(id);
        res.json({fetchUser})
    } catch (error) {
        throw new Error (error);
    }
})


// Delete A User
const deleteUser = asyncHandler ( async (req, res) => {

    const {id} = req.params;
    validateMongodbId(id);

    try {
        const deletedUser = await User.findByIdAndDelete(id);
        res.json({deletedUser})
    } catch (error) {
        throw new Error(error);
    }
})


// Update A User
const updateUser = asyncHandler ( async (req, res) => {
    const { _id} = req.user;
    validateMongodbId(_id);
    try {
        const userUpdate = await User.findByIdAndUpdate( _id, {
            firstName: req?.body?.firstName,
            lastName: req?.body?.lastName, 
            email: req?.body?.email,
            mobile: req?.body?.mobile
        },
        {
            new: true
        });
        res.json(userUpdate)
    } catch (error) {
        throw new Error(error)
    }
})


//Block or Unblock A User
const blockUser = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);

    try {
        const block = await User.findByIdAndUpdate(
            id, {
                isBlocked: true,
            },
            {
                new: true,
            }
        );
        res.json({
            message: 'User Blocked!'
        })
    } catch (error) {
        throw new Error (error)
    }
})

const unblockUser = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);

    try {
        const unblock = await User.findByIdAndUpdate(
            id, {
                isBlocked: false,
            },
            {
                new: true,
            }
        );
        res.json({
            message: 'User Unblocked!'
        })
    } catch (error) {
        throw new Error (error)
    }
})


// Password Update
const updatePassword = asyncHandler ( async (req, res) => {
    const { _id } = req.user;
    const { password } = req.body;
    validateMongodbId(_id);

    const user = await User.findById(_id);
    if (password) {
        user.password = password;
        const updatedPassword = await user.save();
        res.json(updatedPassword)
    } else {
        res.json(user)
    }
})


// Forgot Password Token
const forgotPasswordToken = asyncHandler ( async (req, res) => {
    const { email } = req.body;
    const user = await User.findOne({email});
    if(!user) throw new Error ('User not found');
    
    try {
        const token = await user.createPasswordResetToken();
        await user.save();
        const resetURL = `Hi, Please follow this link to reset your password. This link is valid for 10 minutes <a href='http://localhost:5000/user/reset-password/${token}'>Click Here</a>`;
        const data = {
            to: email,
            text: 'Hey User',
            subject: "Reset Password Link",
            html: resetURL
        };
        sendEmail(data);
        res.json(token)
    } catch (error) {
        throw new Error (error)
    }

})


// Reset Password
const resetPassword = asyncHandler (async (req, res) => {
    const { password } = req.body;
    const { token } = req.params;
    const hashedToken = crypto
        .createHash('sha256')
        .update(token)
        .digest('hex');
    const user = await User.findOne({
        passwordResetToken: hashedToken,
        passwordResetExpires: { $gt: Date.now()}
    });
    if(!user) throw new Error('Token Expired, Please generate a new one');
    user.password = password;
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    
    await user.save();
    res.json(user);

})


// Fetch Wishlist
const getWishlist = asyncHandler ( async (req, res) => {
    const { _id } = req.user
    
    try {
        const findUser = await User.findById(_id)
            .populate('wishlist');
        res.json(findUser)
    } catch (error) {
        throw new Error (error)
    }
})


module.exports = { 
    createUser,
    loginUser,
    getAllUser,
    getUser,
    deleteUser,
    updateUser,
    blockUser,
    unblockUser,
    handleRefreshToken,
    logoutUser,
    updatePassword,
    forgotPasswordToken,
    resetPassword,
    loginAdmin,
    getWishlist,
    saveAddress
}