const asyncHandler = require('express-async-handler');
const { cloudinaryUploadImg, cloudinaryDeleteImg } = require('../utils/cloudinary');
const fs = require('fs');



// Upload Image to Cloudinary
const uploadImages = asyncHandler(async (req, res) => {
  // const { id } = req.params;
  // validateMongodbId (id);
  // console.log(req.files)

  try {
    const uploader = (path) => cloudinaryUploadImg(path, "images");
    const urls = [];
    const files = req.files;

    for (const file of files) {
      const { path } = file;
      const newPath = await uploader(path);
      urls.push(newPath);
      fs.unlinkSync(path);
    }
    const images = urls.map((file) => {
      return file;
    });
    res.json(images);
    // const findProduct = await Product.findByIdAndUpdate(id, {
    //     images: urls.map((file) => { return file }),
    // },{
    //     new: true
    // });
    // res.json(findProduct)
  } catch (error) {
    throw new Error(error);
  }
});

// Delete Image from Cloudinary
const deleteImages = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const deleted = cloudinaryDeleteImg(id, "images");
    res.json({ message: "Deleted" });
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  uploadImages,
  deleteImages,
};
