const Blog = require('../models/Blog');
const User = require('../models/User');
const asyncHandler = require('express-async-handler');
const validateMongodbId = require('../utils/validateMongodbId');
const cloudinaryUploadImg = require('../utils/cloudinary');
const fs = require('fs');



// Create Blog
const createBlog = asyncHandler ( async (req, res) => {
    try {
        const newBlog = await Blog.create(req.body);
        res.json({
            status: 'Success',
            newBlog
        })
    } catch (error){
        throw new Error (error)
    }
})


// Update Blog
const updateBlog = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const updatedBlog = await Blog.findByIdAndUpdate( id, req.body, {
            new: true
        });
        res.json(updatedBlog)
    } catch (error){
        throw new Error (error)
    }
})


// Get Blog
const getBlog = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    try {
        const fetchBlog = await Blog.findById(id)
        .populate('likes')
        .populate('dislikes');
        const updateViews = await Blog.findByIdAndUpdate(
            id,
            {
                $inc: { viewsCount: 1}
            },
            {
                new: true
            }
        );
        res.json(fetchBlog)
    } catch (error) {
        throw new Error (error)
    }
})


// Get All Blogs
const getAllBlogs = asyncHandler ( async (req, res) => {
    try {
        const fetchBlogs = await Blog.find();
        res.json(fetchBlogs)
    } catch (error) {
        throw new Error (error)
    }
})


// Delete A Blog
const deleteBlog = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    try {
        const deletedBlog = await Blog.findByIdAndDelete( id );
        res.json(deletedBlog)
    } catch (error){
        throw new Error (error)
    }
})


// Blog Likes
const likeBlog = asyncHandler ( async (req, res) => {
    const { blogId } = req.body;
    validateMongodbId( blogId );

    // Find the blog
    const blog = await Blog.findById(blogId);
    // Find the logged in user
    const loginUserId = req?.user?._id;
    // Find if the user liked the post/blog
    const isLiked = blog?.isLiked;
    // Find if the user disliked the post/blog
    const disliked = blog?.dislikes?.find(
        (userId) => userId?.toString() === loginUserId?.toString()
        )

    if (disliked) {
        const blog = await Blog.findByIdAndUpdate( 
            blogId, 
            {
            $pull: { dislikes: loginUserId},
            isDisliked: false
        },
        {
            new: true
        });
        res.json(blog)
    }
    if (isLiked) {
        const blog = await Blog.findByIdAndUpdate( blogId, {
            $pull: { likes: loginUserId},
            isLiked: false
        }, {
            new: true
        });
        res.json(blog)
    } else {
        const blog = await Blog.findByIdAndUpdate( blogId, {
            $push: { likes: loginUserId},
            isLiked: true
        }, {
            new: true
        });
        res.json(blog)
    }
})


// Blog Dislikes
const dislikeBlog = asyncHandler ( async (req, res) => {
    const { blogId } = req.body;
    validateMongodbId( blogId );

    // Find the blog
    const blog = await Blog.findById(blogId);
    // Find the logged in user
    const loginUserId = req?.user?._id;
    // Find if the user liked the post/blog
    const isDisliked = blog?.isDisliked;
    // Find if the user disliked the post/blog
    const liked = blog?.likes?.find(
        (userId) => userId?.toString() === loginUserId?.toString()
        )

    if (liked) {
        const blog = await Blog.findByIdAndUpdate( 
            blogId, 
            {
            $pull: { likes: loginUserId},
            isLiked: false
        },
        {
            new: true
        });
        res.json(blog)
    }
    if (isDisliked) {
        const blog = await Blog.findByIdAndUpdate( blogId, {
            $pull: { dislikes: loginUserId},
            isDisliked: false
        }, {
            new: true
        });
        res.json(blog)
    } else {
        const blog = await Blog.findByIdAndUpdate( blogId, {
            $push: { dislikes: loginUserId},
            isDisliked: true
        }, {
            new: true
        });
        res.json(blog)
    }
});


// Upload Image
const uploadImages = asyncHandler ( async (req, res) => {
    const { id } = req.params;
    validateMongodbId (id);

    try {
        const uploader = (path) => cloudinaryUploadImg(path, 'images');
        const urls = [];
        const files = req.files;

            for ( const file of files ) {
                const { path } = file;
                const newPath = await uploader(path);
                urls.push(newPath);
                fs.unlinkSync(path);
            }
        const findBlog = await Blog.findByIdAndUpdate(id, {
            images: urls.map((file) => { return file }),
        },{
            new: true
        });
        res.json(findBlog)
    } catch (error) {
        throw new Error (error)
    }
});




module.exports = {
    createBlog,
    updateBlog,
    getBlog,
    getAllBlogs,
    deleteBlog,
    likeBlog,
    dislikeBlog,
    uploadImages
}