const Inquiry = require('../models/Inquiry');
const asyncHandler = require('express-async-handler');
const validateMongodbId = require('../utils/validateMongodbId');


// Create Inquiry
const createInquiry = asyncHandler(async (req, res) => {
    try {
        const newInquiry = await Inquiry.create(req.body);
        res.json(newInquiry)
    } catch (error) {
        throw new Error (error)
    }
})


// Update Inquiry
const updateInquiry = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const updatedInquiry = await Inquiry.findByIdAndUpdate(
            id, 
            req.body, 
            {
            new: true
        });
        res.json(updatedInquiry)
    } catch (error) {
        throw new Error (error)
    }
})


// Get Single Inquiry
const getInquiry = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const fetchInquiry = await Inquiry.findById(id);
        res.json(fetchInquiry)
    } catch (error) {
        throw new Error (error)
    }
})


// Get All Inquiry
const getAllInquiry = asyncHandler(async (req, res) => {
    try {
        const fetchAllInquiry = await Inquiry.find();
        res.json(fetchAllInquiry)
    } catch (error) {
        throw new Error (error)
    }
})

// Delete Inquiry
const deleteInquiry = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const deletedInquiry = await Inquiry.findById(id);
        res.json(deletedInquiry)
    } catch (error) {
        throw new Error (error)
    }
})




module.exports = {
    createInquiry,
    updateInquiry,
    getInquiry,
    getAllInquiry,
    deleteInquiry
} 