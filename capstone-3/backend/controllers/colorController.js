const Color = require('../models/Color');
const asyncHandler = require('express-async-handler');
const validateMongodbId = require('../utils/validateMongodbId');

/* Product Color */
// Create Color
const createColor = asyncHandler(async (req, res) => {
    try {
        const newColor = await Color.create(req.body);
        res.json(newColor)
    } catch (error) {
        throw new Error (error)
    }
})


// Update Color
const updateColor = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const updatedColor = await Color.findByIdAndUpdate(
            id, 
            req.body, 
            {
            new: true
        });
        res.json(updatedColor)
    } catch (error) {
        throw new Error (error)
    }
})


// Delete Color
const deleteColor = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const deletedColor = await Color.findByIdAndDelete(id);
        res.json(deletedColor)
    } catch (error) {
        throw new Error (error)
    }
})


// Get Single Color
const getColor = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const fetchColor = await Color.findById(id);
        res.json(fetchColor)
    } catch (error) {
        throw new Error (error)
    }
})


// Get All Color
const getAllColor = asyncHandler(async (req, res) => {
    try {
        const fetchAllColor = await Color.find();
        res.json(fetchAllColor)
    } catch (error) {
        throw new Error (error)
    }
})




module.exports = {
    createColor,
    updateColor,
    deleteColor,
    getColor,
    getAllColor
} 