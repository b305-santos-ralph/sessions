const BlogCategory = require('../models/BlogCategory');
const asyncHandler = require('express-async-handler');
const validateMongodbId = require('../utils/validateMongodbId');

/* Product Category */
// Create Category
const createCategory = asyncHandler(async (req, res) => {
    try {
        const newCategory = await BlogCategory.create(req.body);
        res.json(newCategory)
    } catch (error) {
        throw new Error (error)
    }
})


// Update Category
const updateCategory = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const updatedCategory = await BlogCategory.findByIdAndUpdate(
            id, 
            req.body, 
            {
            new: true
        });
        res.json(updatedCategory)
    } catch (error) {
        throw new Error (error)
    }
})


// Delete Category
const deleteCategory = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const deletedCategory = await BlogCategory.findByIdAndDelete(id);
        res.json(deletedCategory)
    } catch (error) {
        throw new Error (error)
    }
})


// Get Single Category
const getCategory = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const fetchCategory = await BlogCategory.findById(id);
        res.json(fetchCategory)
    } catch (error) {
        throw new Error (error)
    }
})


// Get All Category
const getAllCategory = asyncHandler(async (req, res) => {
    try {
        const fetchAllCategory = await BlogCategory.find();
        res.json(fetchAllCategory)
    } catch (error) {
        throw new Error (error)
    }
})






module.exports = {
    createCategory,
    updateCategory,
    deleteCategory,
    getCategory,
    getAllCategory
}