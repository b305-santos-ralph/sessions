const Brand = require('../models/Brand');
const asyncHandler = require('express-async-handler');
const validateMongodbId = require('../utils/validateMongodbId');

/* Product Brand */
// Create Brand
const createBrand = asyncHandler(async (req, res) => {
    try {
        const newBrand = await Brand.create(req.body);
        res.json(newBrand)
    } catch (error) {
        throw new Error (error)
    }
})


// Update Brand
const updateBrand = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const updatedBrand = await Brand.findByIdAndUpdate(
            id, 
            req.body, 
            {
            new: true
        });
        res.json(updatedBrand)
    } catch (error) {
        throw new Error (error)
    }
})


// Delete Brand
const deleteBrand = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const deletedBrand = await Brand.findByIdAndDelete(id);
        res.json(deletedBrand)
    } catch (error) {
        throw new Error (error)
    }
})


// Get Single Brand
const getBrand = asyncHandler(async (req, res) => {
    const { id } = req.params;
    validateMongodbId(id);
    try {
        const fetchBrand = await Brand.findById(id);
        res.json(fetchBrand)
    } catch (error) {
        throw new Error (error)
    }
})


// Get All Brand
const getAllBrand = asyncHandler(async (req, res) => {
    try {
        const fetchAllBrand = await Brand.find();
        res.json(fetchAllBrand)
    } catch (error) {
        throw new Error (error)
    }
})






module.exports = {
    createBrand,
    updateBrand,
    deleteBrand,
    getBrand,
    getAllBrand
}