const { default: mongoose } = require('mongoose');


// MongoDB Connection
const dbConnect = () => {
    try {
        const connection = mongoose.connect(process.env.MONGODB_URL) 
        console.log('Connected to Database Successfully')
    }
    catch(error) {
        throw new Error (error);
    }
};

module.exports = dbConnect