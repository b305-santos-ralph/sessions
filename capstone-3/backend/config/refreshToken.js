const jwt = require('jsonwebtoken');

// jsonwebtoken
const generateRefreshToken = (id) => {
    return jwt.sign ({id}, process.env.JWT_SECRET, { expiresIn: '3d'})
}

module.exports = { generateRefreshToken };