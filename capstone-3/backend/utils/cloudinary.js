const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.SECRET_KEY,
})


// For image upload
const cloudinaryUploadImg = async (fileToUpload) => {
    return new Promise ((resolve) => {
        cloudinary.uploader.upload(fileToUpload, (result) => {
            resolve({
                url: result.secure_url,
                asset_id: result.asset_id,
                public_id: result.public_id
            },{
                resource_type: "auto"
            })
        })
    })
}


// For image deletion
const cloudinaryDeleteImg = async (fileToDelete) => {
    return new Promise ((resolve) => {
        cloudinary.uploader.destroy(fileToDelete, (result) => {
            resolve({
                url: result.secure_url,
                asset_id: result.asset_id,
                public_id: result.public_id
            },{
                resource_type: "auto"
            })
        })
    })
}

module.exports = {cloudinaryUploadImg, cloudinaryDeleteImg}

