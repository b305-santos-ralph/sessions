import React from "react";
import ReactStars from "react-rating-stars-component";
import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addToWishlist } from "../features/products/productSlice";
import { BiCart, BiHeart, BiShow } from "react-icons/bi";
import { LuGitCompare } from "react-icons/lu";

const ProductCard = (props) => {
  const { grid, data } = props;
  const dispatch = useDispatch();
  let location = useLocation();
  const addWishlist = (id) => {
    dispatch(addToWishlist(id));
  };

  return (
    <>
      {data &&
        data?.map((item, index) => {
          return (
            <div
              key={index}
              className={`${
                location.pathname === "/store" ? `gr-${grid}` : "col-3"
              }`}
            >
              {/* Dynamic Translation */}
              <div  
                className="product-card position-relative"
              >
                <div className="product-image">
                  <img
                    className="img-fluid mx-auto"
                    src={item?.images[0]?.url}
                    alt="product image"
                  />
                </div>
                <div className="product-information">
                  <div className="product-details">
                    <h6 className="brand">{item?.brand}</h6>
                    <h5 className="product-title">
                      {item?.title ? item?.title.substr(0, 40) + "..." : ""}
                    </h5>
                    <ReactStars
                      count={5}
                      size={16}
                      value={item?.totalRating.toString()}
                      edit={false}
                    />
                    <p
                      className={`description ${
                        grid === 12 || grid === 6 ? "d-block" : "d-none"
                      }`}
                      dangerouslySetInnerHTML={{
                        __html: item?.description
                          ? item?.description.substr(0, 300) + "..."
                          : "",
                      }}
                    ></p>
                    <p className="price">&#8369; {item?.price}</p>
                  </div>
                  <div className="action-bar position-absolute">
                    <div className="d-flex flex-row gap-2">
                      <Link to={'/store/' + item?._id} className="border-0 bg-transparent text-dark">
                        <BiShow className="img-button" />
                      </Link>
                      <button className="border-0 bg-transparent">
                        <LuGitCompare className="img-button" />
                      </button>
                      <button className="border-0 bg-transparent">
                        <BiCart className="img-button" />
                      </button>
                      <button
                        className="border-0 bg-transparent"
                        onClick={() => {
                          addWishlist(item?._id);
                        }}
                      >
                        <BiHeart className="img-button" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};

export default ProductCard;
