import React from "react";
import { Form, InputGroup } from "react-bootstrap";
import {
  BsFacebook,
  BsTwitter,
  BsInstagram,
  BsTiktok,
  BsLinkedin,
} from "react-icons/bs";
import { Link } from "react-router-dom";
import newsletter from "../images/newsletter.png";

const Footer = () => {
  return (
    <>
      {/* Top Footer */}
      <footer className="py-3">
        <div className="container-xxl">
          <div className="row align-items-center">
            <div className="col-5">
              <div className="footer-top-data d-flex gap-3 align-items-center">
                <img src={newsletter} alt="" />
                <h2 className="mb-0 text-white fs-5">
                  Sign Up for News Letter
                </h2>
              </div>
            </div>
            <div className="col-7">
              <InputGroup className="">
                <Form.Control
                  type="text"
                  className="py-1 fs-6"
                  placeholder="Your Email Address"
                  aria-label="Your Email Address"
                  aria-describedby="basic-addon2"
                />
                <InputGroup.Text id="basic-addon2" className="p-2 fs-6">
                  Subscribe
                </InputGroup.Text>
              </InputGroup>
            </div>
          </div>
        </div>
      </footer>

      {/* Middle Footer */}
      <footer className="py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-4">
              <h4 className="text-white mb-0">Customer Service</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="text-white mb-2">Help Centre</Link>
                <Link to="/return-refund" className="text-white mb-2">
                  Return & Refund
                </Link>
                <Link to="/contact" className="text-white mb-2">
                  Contact Us
                </Link>
                <div className="contact-area">
                  <address className="text-white">
                    000 <br />
                    street <br />
                    city <br />
                    state & postal code <br />
                  </address>
                  <a href="tel:(XX) XXXX XXXX" className="text-white d-block">
                    (XX) XXXX XXXX
                  </a>
                  <a
                    href="mailto:sample@mail.com"
                    className="text-white d-block"
                  >
                    sample@mail.com
                  </a>
                </div>
              </div>
            </div>
            <div className="col-3">
              <h4 className="text-white mb-0">Information</h4>
              <div className="footer-links d-flex flex-column">
                <Link to="/about-us" className="text-white mb-2">
                  About Us
                </Link>
                <Link to="/blog" className="text-white mb-2">
                  Blogs
                </Link>
                <Link to="/FAQs" className="text-white mb-2">
                  FAQs
                </Link>
                <Link to="/policies" className="text-white mb-2">
                  Policies
                </Link>
                <Link to="/terms-and-conditions" className="text-white mb-2">
                  Terms & Conditions
                </Link>
                <Link to="/inquiry" className="text-white mb-2">
                  Media Contact
                </Link>
              </div>
            </div>
            <div className="col-3">
              <h4 className="text-white mb-0">Follow Us</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="text-white mb-2">
                  <BsFacebook className="fs-6" /> Facebook
                </Link>
                <Link className="text-white mb-2">
                  <BsInstagram className="fs-6" /> Instagram
                </Link>
                <Link className="text-white mb-2">
                  <BsTwitter className="fs-6" /> Twitter
                </Link>
                <Link className="text-white mb-2">
                  <BsTiktok className="fs-6" /> Tiktok
                </Link>
                <Link className="text-white mb-2">
                  <BsLinkedin className="fs-6" /> LinkedIn
                </Link>
              </div>
            </div>
            <div className="col-2">
              <h4 className="text-white mb-0">Quick Links</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="text-white mb-2">test</Link>
                <Link className="text-white mb-2">test</Link>
                <Link className="text-white mb-2">test</Link>
                <Link className="text-white mb-2">test</Link>
              </div>
            </div>
          </div>
        </div>
      </footer>

      {/* Bottom Footer */}
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <p className="text-center text-white mb-0">
                &copy; {new Date().getFullYear()}; Created by Ralph Santos{" "}
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
