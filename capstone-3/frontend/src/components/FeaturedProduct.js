import React from "react";
import ReactStars from "react-rating-stars-component";
import { Link } from "react-router-dom";

const FeaturedProduct = (props) => {
  const { title, brand, totalRating, price, sold, quantity, image, id } = props;
  return (
    <>
      <div className="col-6 mb-3">
        <div className="featured-product-card">
          <div className="d-flex justify-content-between gap-3">
            <div>
              <img src={image} alt="" className="img-fluid" />
            </div>
            <div className="featured-product-content">
              <h5 className="brand">{brand}</h5>
              <h6 className="title">{title}</h6>
              <ReactStars
                count={5}
                size={24}
                value={totalRating}
                edit={false}
                activeColor="#ffd700"
              />
              <p className="price">
                <span className="red-p">&#8369; {price}</span>
              </p>
              <div className="discount-until d-flex align-items-center gap-2">
                <p className="mb-0">
                  <b>5 </b>days
                </p>
                <div className="d-flex gap-1 align-items-center">
                  <span className="badge rounded-circle p-3 bg-danger">1</span>:
                  <span className="badge rounded-circle p-3 bg-danger">1</span>:
                  <span className="badge rounded-circle p-3 bg-danger">1</span>
                </div>
              </div>
              <div className="product-count my-3">
                <p>Products: {quantity}</p>
                <div className="progress">
                  <div
                    className="progress-bar"
                    role="progressbar"
                    style={{ width: quantity / quantity + sold * 100 + "%" }}
                    aria-valuenow={quantity / quantity + sold * 100}
                    aria-valuemin={quantity}
                    aria-valuemax={sold + quantity}
                  ></div>
                </div>
              </div>
              <Link className="btn blog-button border-0" to={"/store/" + id}>
                View Product
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FeaturedProduct;
