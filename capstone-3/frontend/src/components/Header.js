import React, { useEffect, useState }  from "react";
import { NavLink, Link } from "react-router-dom";
import { InputGroup, Form, Dropdown, Badge } from "react-bootstrap";
import { BsSearch } from "react-icons/bs";
import compare from "../images/compare.svg";
import wishlist from "../images/wishlist.svg";
import user from "../images/user.svg";
import cart from "../images/cart.svg";
import menu from "../images/menu.svg";
import { useDispatch, useSelector } from "react-redux";


const Header = () => {
  const dispatch = useDispatch();
  const cartState = useSelector((state) => state?.auth?.cartProducts);
  const [total, setTotal] = useState(null);
  // useEffect(() => {
  //   let sum = 0;
  //   for (let i = 0; i < cartState.length; i++) {
  //     sum = sum + Number(cartState[i].quantity) * Number(cartState[i].price);
  //     setTotal(sum)
  //   }
  // }, [cartState]);
  return (
    <>
      {/* Header Top */}
      <header className="header-top-strip">
        <div className="container-xxl">
          <div className="row">
            <div className="col-6">
              <p className="text-white mb-0">
                Free Shipping & Returns during Weekends
              </p>
            </div>
            <div className="col-6">
              <p className="text-end text-white">
                Hotline:{" "}
                <a className="text-white" href="tel:+1-800-766-6779">
                  +XX-XXXX-XXXX
                </a>
              </p>
            </div>
          </div>
        </div>
      </header>

      {/* Header Upper */}
      <header className="header-upper py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-1">
              <h2>
                <Link className="text-white">Logo</Link>
              </h2>
            </div>
            <div className="col-7">
              <InputGroup className="">
                <Form.Control
                  type="text"
                  className="py-2 fs-6"
                  placeholder="Search Product"
                  aria-label="Search Product"
                  aria-describedby="basic-addon2"
                />
                <InputGroup.Text id="basic-addon2" className="p-3">
                  <BsSearch className="fs-5" />
                </InputGroup.Text>
              </InputGroup>
            </div>
            <div className="col-4">
              <div
                className="header-upper-links 
                    d-flex 
                    align-items-between 
                    justify-content-between"
              >
                <div>
                  <Link
                    to="/compare-products"
                    className="d-flex align-items-center gap-1 text-white"
                  >
                    <img src={compare} alt="compare" />
                    <p className="mb-0">
                      Compare <br /> Products
                    </p>
                  </Link>
                </div>
                <div>
                  <Link
                    to="/wishlist"
                    className="d-flex align-items-center gap-1 text-white"
                  >
                    <img src={wishlist} alt="wishlist" />
                    <p className="mb-0">Wishlist</p>
                  </Link>
                </div>
                <div>
                  <Link
                    to="/login"
                    className="d-flex align-items-center gap-1 text-white"
                  >
                    <img src={user} alt="user" />
                    <p className="mb-0">Login</p>
                  </Link>
                </div>
                <div>
                  <Link
                    to="/cart"
                    className="d-flex align-items-center gap-1 text-white"
                  >
                    <img src={cart} alt="cart" />
                    <div className="d-flex flex-column gap-1">
                      <Badge className="bg-white text-dark d-flex justify-content-center">
                        {cartState?.length ? cartState.length : 0}
                      </Badge>
                      <p className="mb-0">&#8369; {total ? total : 0}</p>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      {/* Header Bottom */}
      <header className="header-bottom py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              {/* Dropdown Button */}
              <div className="menu-bottom d-flex align-items-center gap-3">
                <div>
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="text"
                      id="dropdown-basic"
                      className="d-flex align-items-center gap-2 me-4"
                    >
                      <img src={menu} alt="menu" />
                      <span className="me-1 d-inline-block">Categories</span>
                    </Dropdown.Toggle>

                    <Dropdown.Menu id="dropdown-menu">
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                <div className="menu-links">
                  <div className="d-flex align-items-center gap-3">
                    <NavLink to="/">home</NavLink>
                    <NavLink to="/store">store</NavLink>
                    <NavLink to="/blog">blog</NavLink>
                    <NavLink to="/contact">contact</NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
