import React from "react";
import { Link } from "react-router-dom";

const BlogCard = (props) => {
  const { id, title, description, date, image } = props;
  return (
    <div className="blog-card">
      <div className="card-image">
        <img
          src={image ? image : "No Image Available"}    
          alt="blog"
          className="img-fluid"
        />
      </div>
      <div className="blog-content">
        <p className="date">{date}</p>
        <h5 className="title">{title}</h5>
        <p
          className="description"
          dangerouslySetInnerHTML={{
            __html: description ? description.substr(0, 150) + "..." : "",
          }}
        ></p>
        <Link to={"/blog/" + id} className="btn blog-button">
          Read More
        </Link>
      </div>
    </div>
  );
};

export default BlogCard;
