import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Store from "./pages/Store";
import Blog from "./pages/Blog";
import CompareProduct from "./pages/CompareProduct";
import Wishlist from "./pages/Wishlist";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import Password from "./pages/Password";
import Reset from "./pages/Reset";
import BlogView from "./pages/BlogView";
import Policies from "./pages/Policies";
import Terms from "./pages/TandC";
import ReturnRefund from "./pages/ReturnRefund";
import ProductView from "./pages/ProductView";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";


function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="contact" element={<Contact />} />
            <Route path="store" element={<Store />} />
            <Route path="store/:id" element={<ProductView />} />
            <Route path="blog" element={<Blog />} />
            <Route path="blog/:id" element={<BlogView />} />
            <Route path="cart" element={<Cart />} />
            <Route path="checkout" element={<Checkout />} />
            <Route path="compare-products" element={<CompareProduct />} />
            <Route path="wishlist" element={<Wishlist />} />
            <Route path="login" element={<Login />} />
            <Route path="signup" element={<Signup />} />
            <Route path="forgot-password" element={<Password />} />
            <Route path="reset-password" element={<Reset />} />
            <Route path="policies" element={<Policies />} />
            <Route path="return-refund" element={<ReturnRefund />} />
            <Route path="terms-and-conditions" element={<Terms />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
