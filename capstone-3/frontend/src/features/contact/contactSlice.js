import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import { contactService } from "./contactService";

export const createInquiry = createAsyncThunk(
  "contact/post", // Action type that will be dispatched
  async (contactData, thunkAPI) => {
    try {
      return contactService.postInquiry(contactData);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);


const initialState = {
  contact: "",
  isError: false, // No errors by default.
  isSuccess: false, // Registration is not successful by default.
  isLoading: false, // Initial state is not loading.
  message: "", // A message to store any error messages.
};

// Create an authentication slice with reducers and extra reducers.
export const contactSlice = createSlice({
  name: "products",
  initialState: initialState,
  reducers: {},

  // Extra reducers are used to handle actions dispatched by createAsyncThunk.
  extraReducers: (builder) => {
    builder

      .addCase(createInquiry.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createInquiry.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.contact = action.payload;
        if(state.isSuccess === true) {
          toast.success("Submitted Successfully!")
        }
      })
      .addCase(createInquiry.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
        if(state.isError === true) {
          toast.error("Something Went Wrong!")
        }
      });
  },
});

export default contactSlice.reducer;
