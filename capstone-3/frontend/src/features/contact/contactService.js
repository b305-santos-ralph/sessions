import axios from "axios";
import { base_url, config } from "../../utils/axiosConfig";

const postInquiry = async (contactData) => {
  const response = await axios.post(`${base_url}/inquiry/`, contactData);
  if (response.data) {
    return response.data;
  }
};


/**
 * A service for user authentication and registration.
 */
export const contactService = {
  postInquiry,

};