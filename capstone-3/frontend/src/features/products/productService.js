import axios from "axios";
import { base_url, config } from "../../utils/axiosConfig";

const getProducts = async () => {
  const response = await axios.get(`${base_url}/products`);
  if (response.data) {
    return response.data;
  }
};

const getProduct = async (id) => {
  const response = await axios.get(`${base_url}/products/${id}`);
  if (response.data) {
    return response.data;
  }
};

const addToWishlist = async (productId) => {
  const response = await axios.put(
    `${base_url}/products/wishlist`,
    productId,
    config
  );
  if (response.data) {
    return response.data;
  }
};


/**
 * A service for user authentication and registration.
 */
export const productService = {
  getProducts,
  addToWishlist,
  getProduct
};
