import axios from "axios";
import { base_url, config } from "../../utils/axiosConfig";

const register = async (userData) => {
  try {
    const response = await axios.post(`${base_url}/user/signup`, userData);
    if (response.data) {
      if (response.data) {
        localStorage.setItem("customer", JSON.stringify(response.data));
      }
      return response.data;
    }
  } catch (error) {
    console.error("Registration failed:", error);
    return null;
  }
};

const login = async (userData) => {
  try {
    const response = await axios.post(`${base_url}/user/login`, userData);
    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.error("Login Failed:", error);
    return null;
  }
};

const getUserWishlist = async () => {
  const response = await axios.get(`${base_url}/user/wishlist`, config);
  if (response.data) {
    return response.data;
  }
};

const addToCart = async (cartData) => {
  const response = await axios.post(
    `${base_url}/cart/add-cart`,
    cartData,
    config
  );
  if (response.data) {
    return response.data;
  }
};

const getCart = async () => {
  const response = await axios.get(`${base_url}/cart/view-cart`, config);
  if (response.data) {
    return response.data;
  }
};

const removeFromCart = async (cartItemId) => {
  const response = await axios.delete(
    `${base_url}/cart/remove-item-cart/${cartItemId}`,
    config
  );
  if (response.data) {
    return response.data;
  }
};

const updateFromCart = async (cartDetails) => {
  const response = await axios.put(
    `${base_url}/cart/update-item-cart/${cartDetails.cartItemId}/${cartDetails.quantity}`,
    config
  );
  if (response.data) {
    return response.data;
  }
};

export const authService = {
  register,
  login,
  getUserWishlist,
  addToCart,
  getCart,
  removeFromCart,
  updateFromCart,
};
