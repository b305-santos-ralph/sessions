import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import { blogService } from "./blogService";


export const getAllBlogs = createAsyncThunk(
  "blog/get-blogs", // Action type that will be dispatched
  async (thunkAPI) => {
    try {
      return blogService.getBlogs();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getABlog = createAsyncThunk(
    "blog/get-blog", // Action type that will be dispatched
    async (id, thunkAPI) => {
      try {
        return blogService.getBlog(id);
      } catch (error) {
        return thunkAPI.rejectWithValue(error);
      }
    }
  );

const initialState = {
  blogs: "",
  isError: false, // No errors by default.
  isSuccess: false, // Registration is not successful by default.
  isLoading: false, // Initial state is not loading.
  message: "", // A message to store any error messages.
};

// Create an authentication slice with reducers and extra reducers.
export const blogSlice = createSlice({
  name: "blogs", 
  initialState: initialState, 
  reducers: {}, 

  // Extra reducers are used to handle actions dispatched by createAsyncThunk.
  extraReducers: (builder) => {
    builder

      .addCase(getAllBlogs.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllBlogs.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.blogs = action.payload;
      })
      .addCase(getAllBlogs.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(getABlog.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getABlog.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.singleBlog = action.payload;
      })
      .addCase(getABlog.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
  },
});

export default blogSlice.reducer;
