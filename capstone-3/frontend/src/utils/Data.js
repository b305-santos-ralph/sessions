export const services = [
  {
    image: "/images/service.png",
    title: "Free Shipping",
    tagline: "Every Weekends",
  },
  {
    image: "/images/service-02.png",
    title: "Daily Offers",
    tagline: "Save up to 25% off",
  },
  {
    image: "/images/service-03.png",
    title: "Support 24/7",
    tagline: "Shop with an expert",
  },
  {
    image: "/images/service-04.png",
    title: "Affordable Prices",
    tagline: "Get the best deals",
  },
  {
    image: "/images/service-05.png",
    title: "Secure Payment",
    tagline: "100% Protected",
  },
];

