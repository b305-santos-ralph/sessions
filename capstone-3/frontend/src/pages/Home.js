import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import Marquee from "react-fast-marquee";
import BlogCard from "../components/BlogCard";
import ProductCard from "../components/ProductCard";
import Container from "../components/Container";
import { services } from "../utils/Data";
import { useDispatch, useSelector } from "react-redux";
import { getAllBlogs } from "../features/blogs/blogSlice";
import moment from "moment";
import { addToWishlist, getAllProducts } from "../features/products/productSlice";
import FeaturedProduct from "../components/FeaturedProduct";

const Home = () => {
  const blogState = useSelector((state) => state?.blog?.blogs);
  const productState = useSelector((state) => state?.product?.products);
  const dispatch = useDispatch();
  const navigate = useNavigate()

  useEffect(() => {
    getBlogs();
    getProducts();
  }, []);

  const getBlogs = () => {
    dispatch(getAllBlogs());
  };

  const getProducts = () => {
    dispatch(getAllProducts());
  };

  const addToWish = (id) => {
    dispatch(addToWishlist(id))
  }

  return (
    <>
      {/* Hot Products Section */}
      <Container class1="home-wrapper-1 py-4">
        <div className="row">
          {/* Main Banner ( LEFT ) */}
          <div className="col-6">
            <div className="main-banner position-relative">
              <img
                src="/images/sample.png"
                className="img-fluid"
                alt="830 x 550"
              />
              <div className="main-banner-content position-absolute">
                <h4>title title title</h4>
                <h5>description</h5>
                <p>promo</p>
                <Link>BUY NOW</Link>
              </div>
            </div>
          </div>

          {/* Small Banner ( RIGHT ) */}
          <div className="col-6">
            <div className="d-flex flex-wrap justify-content-between align-items-center gap-2">
              {/* First Banner */}
              <div className="small-banner position-relative">
                <img
                  src="/images/sample.png"
                  className="img-fluid"
                  alt="405 x 265"
                />
                <div className="small-banner-content position-absolute">
                  <h4>title title title</h4>
                  <h5>description</h5>
                  <p>
                    promo <br /> promo
                  </p>
                </div>
              </div>

              {/* Second Banner */}
              <div className="small-banner position-relative">
                <img
                  src="/images/sample.png"
                  className="img-fluid"
                  alt="405 x 265"
                />
                <div className="small-banner-content position-absolute">
                  <h4>title title title</h4>
                  <h5>description</h5>
                  <p>
                    promo <br /> promo
                  </p>
                </div>
              </div>

              {/* Third Banner */}
              <div className="small-banner position-relative">
                <img
                  src="/images/sample.png"
                  className="img-fluid"
                  alt="405 x 265"
                />
                <div className="small-banner-content position-absolute">
                  <h4>title title title</h4>
                  <h5>description</h5>
                  <p>
                    promo <br /> promo
                  </p>
                </div>
              </div>

              {/* Fourth Banner */}
              <div className="small-banner position-relative">
                <img
                  src="/images/sample.png"
                  className="img-fluid"
                  alt="405 x 265"
                />
                <div className="small-banner-content position-absolute">
                  <h4>title title title</h4>
                  <h5>description</h5>
                  <p>
                    promo <br /> promo
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>

      {/* Services Section*/}
      <Container class1="home-wrapper-2 py-4">
        <div className="row my-4">
          <div className="col-12">
            <div className="services d-flex align-items-center justify-content-between">
              {services?.map((data, index) => {
                return (
                  <div className="d-flex align-items-center gap-3" key={index}>
                    <img src={data.image} alt="services" />
                    <div>
                      <h6 className="mb-0">{data.title}</h6>
                      <p className="mb-0">{data.tagline}</p>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </Container>

      {/* Categories Section */}
      <Container class1="home-wrapper-2 pb-4">
        <div className="row">
          <div className="col-12">
            {/* Category Area */}
            <div className="categories d-flex flex-wrap justify-content-between align-items-center">
              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">MIYABI</h6>
                  <p>Zwilling Group Brand</p>
                </div>
                <img
                  src="/images/brand1.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">SHUN</h6>
                  <p>Japanese cutlery to the US and EU</p>
                </div>
                <img
                  src="/images/brand2.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">ITAMAE PH</h6>
                  <p>Cutlery & Kitchen Knives</p>
                </div>
                <img
                  src="/images/brand3.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">MASAMOTO</h6>
                  <p>Traditional Japanese knives</p>
                </div>
                <img
                  src="/images/brand4.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">TOJIRO</h6>
                  <p>"Ichigo Ichie" - Once in a lifetime</p>
                </div>
                <img
                  src="/images/brand5.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">TAKESHI SAJI</h6>
                  <p>Takeshi Saji's knives</p>
                </div>
                <img
                  src="/images/brand6.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">SAKAI TAKAYUKI</h6>
                  <p>600 years of history</p>
                </div>
                <img
                  src="/images/brand7.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>

              <div className="d-flex gap-2 align-items-center">
                <div>
                  <h6 className="mb-0">WUSTHOF</h6>
                  <p>Made in Solingen</p>
                </div>
                <img
                  src="/images/brand8.png"
                  alt="110 x 110"
                  className="img-fluid"
                  style={{ width: "110px", height: "110px" }}
                />
              </div>
            </div>
          </div>
        </div>
      </Container>

      {/* Featured Products Section */}
      <Container class1="featured-wrapper py-4 home-wrapper-2">
        <div className="row">
          <div className="col-12">
            <h3 className="section-heading pt-3">Featured Products</h3>
          </div>
        </div>
        <div className="row">
          {productState &&
            productState?.map((item, index) => {
              if (item.tags === "featured") {
                return (
                  <FeaturedProduct
                    key={index}
                    id={item?._id}
                    brand={item?.brand}
                    title={item?.title}
                    totalRating={item?.totalRating.toString()}
                    price={item?.price}
                    sold={item?.sold}
                    quantity={item?.quantity}
                    image={item?.images[0].url}
                  />
                );
              }
            })}
        </div>
      </Container>

      {/* Brands Section */}
      <Container class1="marquee-wrapper py-3">
        <div className="row">
          <div className="col-12">
            <div className="marquee-inner-wrapper card-wrapper p-3">
              <Marquee className="d-flex">
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
                <div className="mx-4 w-25">
                  <img src="" alt="brand 150 x 150" />
                </div>
              </Marquee>
            </div>
          </div>
        </div>
      </Container>

      {/* Blog Section */}
      <Container class1="blog-wrapper py-4 home-wrapper-2">
        <div className="row">
          <div className="col-12">
            <h3 className="section-heading pt-3">Our Latest Blogs</h3>
          </div>
        </div>
        <div className="row">
          {blogState &&
            blogState?.map((item, index) => {
              if (index < 4) {
                return (
                  <div key={index} className="col-3">
                    <BlogCard
                      className="blog-card-home"
                      id={item?._id}
                      title={item?.title}
                      description={item?.description}
                      image={item?.images[0]?.url}
                      date={moment(item?.createdAt).format("MMMM Do YYYY")}
                    />
                  </div>
                );
              }
            })}
        </div>
      </Container>
    </>
  );
};

export default Home;
