import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import Container from "../components/Container";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getUserProductWishlist } from "../features/user/userSlice";
import { addToWishlist } from "../features/products/productSlice";

const Wishlist = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    getWishlistFromDB();
  }, []);

  const getWishlistFromDB = () => {
    dispatch(getUserProductWishlist());
  };

  const wishlistState = useSelector((state) => state.auth.wishlist);
  const removeFromWishlist = (id) => {
    dispatch(addToWishlist(id));
    setTimeout(() => {
      dispatch(getUserProductWishlist());
    }, 300);
  };

  return (
    <>
      <Meta title={"Wishlist"} />
      <BreadCrumb title="Wishlist" />
      <Container class1="wishlist-wrapper home-wrapper-2 py-4">
        <div className="row">
          {wishlistState && wishlistState.length === 0 && (
            <div className="text-center fs-3">No Data</div>
          )}
          {wishlistState?.map((item, index) => {
            <div className="col-3" key={index}>
              <div className="wishlist-card position-relative">
                <img
                  onClick={() => {
                    removeFromWishlist(item?._id);
                  }}
                  src="/images/cross.svg"
                  alt="cross"
                  className="cross position-absolute img-fluid"
                />
                <div className="wishlist-card-image bg-white">
                  <img
                    className="img-fluid w-100 d-block mx-auto"
                    src={
                      item?.images[0].url
                        ? item?.images[0].url
                        : "No Image Available"
                    }
                    alt="wishlisted"
                    width={160}
                  />
                </div>
                <div className="py-3">
                  <h5 className="title">{item?.title}</h5>
                  <h6 className="price">&#8369; {item?.price}</h6>
                </div>
              </div>
            </div>;
          })}
          {/* Wishlist */}
        </div>
      </Container>
    </>
  );
};

export default Wishlist;
