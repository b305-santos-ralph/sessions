import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import Color from "../components/Color";
import Container from "../components/Container";

const CompareProduct = () => {
  return (
    <>
      <Meta title={"Compare Products"} />
      <BreadCrumb title="Compare Products" />
      <Container class1="compare-product-wrapper py-5 home-wrapper-2">
        <div className="row">
          <div className="col-3">
            <div className="compare-product-card position-relative">
              <img
                src="/images/cross.svg"
                alt="cross"
                className="cross position-absolute img-fluid"
              />
              <div className="product-card-image">
                <img
                  src="/images/watch.jpg"
                  alt="product image"
                  className="img-fluid"
                />
              </div>
              <div className="compare-product-details">
                <h5 className="title">Ipsum</h5>
                <h6 className="price mb-4 mt-2">&#8369; 000.00</h6>
                <div className="">
                  <div className="product-details">
                    <h5>Brand</h5>
                    <p className="">Brand Here</p>
                  </div>
                  <div className="product-details">
                    <h5>Type</h5>
                    <p className="">Type Here</p>
                  </div>
                  <div className="product-details">
                    <h5>SKU</h5>
                    <p className="">SKU Here</p>
                  </div>
                  <div className="product-details">
                    <h5>Availability</h5>
                    <p className="">Availability here</p>
                  </div>
                  <div className="product-details">
                    <h5>Colors</h5>
                    <Color />
                  </div>
                  <div className="product-details">
                    <h5>Sizes</h5>
                    <div className="d-flex gap-1 ">
                      <p>6"</p>
                      <p>8"</p>
                      <p>10"</p>
                      <p>12"</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="compare-product-card position-relative">
              <img
                src="/images/cross.svg"
                alt="cross"
                className="cross position-absolute img-fluid"
              />
              <div className="product-card-image">
                <img
                  src="/images/watch.jpg"
                  alt="product image"
                  className="img-fluid"
                />
              </div>
              <div className="compare-product-details">
                <h5 className="title">Ipsum</h5>
                <h6 className="price mb-4 mt-2">&#8369; 000.00</h6>
                <div className="">
                  <div className="product-details">
                    <h5>Brand</h5>
                    <p className="">Brand Here</p>
                  </div>
                  <div className="product-details">
                    <h5>Type</h5>
                    <p className="">Type Here</p>
                  </div>
                  <div className="product-details">
                    <h5>SKU</h5>
                    <p className="">SKU Here</p>
                  </div>
                  <div className="product-details">
                    <h5>Availability</h5>
                    <p className="">Availability here</p>
                  </div>
                  <div className="product-details">
                    <h5>Colors</h5>
                    <Color />
                  </div>
                  <div className="product-details">
                    <h5>Sizes</h5>
                    <div className="d-flex gap-1 ">
                      <p>6"</p>
                      <p>8"</p>
                      <p>10"</p>
                      <p>12"</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default CompareProduct;
