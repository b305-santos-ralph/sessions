import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import Container from "../components/Container";

const Policies = () => {
  return (
    <>
      <Meta title={"Policies"} />
      <BreadCrumb title="Policies" />
      <Container className="policy-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-12">
            <div className="policy"></div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Policies;
