import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";

const ReturnRefund = () => {
  return (
    <>
      <Meta title={"Returns and Refunds"} />
      <BreadCrumb title="Returns and Refunds" />
      <div className="policy-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="policy"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ReturnRefund;
