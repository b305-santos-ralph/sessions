import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Container from "../components/Container";
import CustomInput from "../utils/CustomInput";

const Password = () => {
  return (
    <>
      <Meta title={"Forgot Password"} />
      <BreadCrumb title="Forgot Password" />
      <Container class1="password-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-12">
            <div className="login-card">
              <h3 className="title text-center mb-1">Reset Password</h3>
              <p className="text-center mx-2">
                We will send you an email to reset your password.
              </p>
              <form className="d-flex flex-column gap-3">
                <CustomInput type="email" name="email" placeholder="Email Address"/>
                <div>
                  <div className="d-flex justify-content-center flex-column align-items-center gap-2 mt-3">
                    <Button className="button submit">Submit</Button>
                    <Link to="/login" className="btn cancel">
                      Cancel
                    </Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Password;
