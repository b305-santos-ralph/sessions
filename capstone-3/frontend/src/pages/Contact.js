import React from "react";
import Meta from "../components/Meta";
import BreadCrumb from "../components/BreadCrumb";
import { Button, Form } from "react-bootstrap";
import {
  AiOutlineHome,
  AiOutlinePhone,
  AiOutlineMail,
  AiOutlineEye,
  AiOutlineInfoCircle,
} from "react-icons/ai";
import Container from "../components/Container";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { createInquiry } from "../features/contact/contactSlice";


const contactSchema = Yup.object().shape({
  firstName: Yup.string().required("First Name is a required field"),
  lastName: Yup.string().required("Last Name is a required field"),
  email: Yup.string()
    .email("Invalid email")
    .required("Email is a required field"),
  mobile: Yup.string().required("Contact Number is a required field"),
  comment: Yup.string().required("Please provide comment"),
});

const Contact = () => {
  const dispatch = useDispatch()
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      mobile: "",
      comment: "",
    },
    validationSchema: contactSchema,
    onSubmit: (values) => {
      dispatch(createInquiry(values))
    },
  });
  return (
    <>
      <Meta title={"Contact"} />
      <BreadCrumb title="Contact" />
      <Container class1="contact-wrapper py-5 home-wrapper-2">
        <div className="row">
          <div className="col-12">
            {/* Google Map Location */}
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.3725142386247!2d121.03386404193905!3d14.634783376292837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7d08b33ef41%3A0xee0502a01eaa04c2!2ssouth%20insula%2C%20timog%2C%20quezon%20city!5e0!3m2!1sen!2sph!4v1697433238517!5m2!1sen!2sph"
              width="600"
              height="450"
              className="border-0 w-100 google-map"
              allowFullScreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade"
            ></iframe>
          </div>

          <div className="col-12 mt-4">
            <div className="contact-inner-wrapper d-flex justify-content-between">
              <div>
                <h3 className="contact-title">Contact Us</h3>
                <Form
                  className="d-flex flex-column gap-2"
                  onSubmit={formik.handleSubmit}
                >
                  <Form.Group className="mb-2" controlId="form.ControlInput1">
                    <Form.Control
                      type="text"
                      placeholder="First Name"
                      className="mb-2"
                      name="firstName"
                      onChange={formik.handleChange("firstName")}
                      onBlur={formik.handleBlur("firstName")}
                      value={formik.values.firstName}
                    />
                    <div className="error">
                      {formik.touched.firstName && formik.errors.firstName}
                    </div>
                    <Form.Control
                      type="text"
                      placeholder="Last Name"
                      className="mb-2"
                      name="lastName"
                      onChange={formik.handleChange("lastName")}
                      onBlur={formik.handleBlur("lastName")}
                      value={formik.values.lastName}
                    />
                    <div className="error">
                      {formik.touched.lastName && formik.errors.lastName}
                    </div>
                    <Form.Control
                      type="email"
                      placeholder="Email Address"
                      className="mb-2"
                      name="email"
                      onChange={formik.handleChange("email")}
                      onBlur={formik.handleBlur("email")}
                      value={formik.values.email}
                    />
                    <div className="error">
                      {formik.touched.email && formik.errors.email}
                    </div>
                    <Form.Control
                      type="tel"
                      placeholder="Contact Number"
                      className="mb-2"
                      name="mobile"
                      onChange={formik.handleChange("mobile")}
                      onBlur={formik.handleBlur("mobile")}
                      value={formik.values.mobile}
                    />
                    <div className="error">
                      {formik.touched.mobile && formik.errors.mobile}
                    </div>
                  </Form.Group>
                  <Form.Group
                    className="mb-2"
                    controlId="form.ControlTextarea1"
                  >
                    <Form.Control
                      as="textarea"
                      placeholder="Comments"
                      rows={6}
                      name="comment"
                      onChange={formik.handleChange("comment")}
                      onBlur={formik.handleBlur("comment")}
                      value={formik.values.comment}
                    />
                    <div className="error">
                      {formik.touched.comment && formik.errors.comment}
                    </div>
                  </Form.Group>
                  <button
                    type="submit"
                    value="Submit"
                    className="btn-secondary"
                  >
                    Submit
                  </button>
                </Form>
              </div>
              <div>
                <h3 className="contact-title">Get in Touch</h3>
                <div>
                  <ul className="ps-0">
                    <li className="mb-3 d-flex gap-2 align-items-center">
                      <AiOutlineHome className="fs-5" />
                      <address className="mb-0">
                        000, Street, City, State Zip
                      </address>
                    </li>
                    <li className="mb-3 d-flex gap-2 align-items-center">
                      <AiOutlinePhone className="fs-5" />
                      <a href="tel:+(XX) XXXX-XXXX">+(XX) XXXX-XXXX</a>
                    </li>
                    <li className="mb-3 d-flex gap-2 align-items-center">
                      <AiOutlineMail className="fs-5" />
                      <a href="mailto:sample@mail.com">sample@mail.com</a>
                    </li>
                    <li className="mb-3 d-flex gap-2 align-items-center">
                      <AiOutlineInfoCircle className="fs-5" />
                      <p className="mb-0">
                        {" "}
                        STORE HOURS <br />
                        <span>Monday - Friday ( 11:00AM - 9:00PM )</span>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Contact;
