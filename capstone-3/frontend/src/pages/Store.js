import React, { useEffect, useState } from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Form } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../features/products/productSlice";

const Store = () => {
  const [grid, setGrid] = useState(4);
  const productState = useSelector((state) => state.product.products);
  const dispatch = useDispatch();

  useEffect(() => {
    getProducts();
  }, []);

  const getProducts = () => {
    dispatch(getAllProducts());
  };

  return (
    <>
      <Meta title={"Store"} />
      <BreadCrumb title="Store" />
      <div className="store-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            {/* Side Pane */}
            <div className="col-3">
              {/* Category Card Section */}
              <div className="filter-card mb-3">
                <h3 className="filter-title">Shop By Category</h3>
                <div>
                  <ul className="ps-2">
                    <li>test 1</li>
                    <li>test 2</li>
                    <li>test 3</li>
                    <li>test 4</li>
                  </ul>
                </div>
              </div>

              {/* Filter Card Section */}
              <div className="filter-card mb-3">
                <h3 className="filter-title">Filter By</h3>

                <h5 className="sub-title">Availability</h5>
                <div>
                  <div className="form-check mb-1 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      In Stock (1)
                    </label>
                  </div>
                  <div class="form-check mb-3 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      Out of Stock (0)
                    </label>
                  </div>
                </div>

                <h5 className="sub-title">Price</h5>
                <div className="d-flex align-items-center gap-3">
                  <div className="form-floating mb-3">
                    <input
                      type="number"
                      className="form-control py-0"
                      id="floatingInput"
                      placeholder="From"
                    />
                    <label htmlFor="floatingInput">From</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      type="number"
                      className="form-control py-0"
                      id="floatingInput"
                      placeholder="To"
                    />
                    <label htmlFor="floatingInput">To</label>
                  </div>
                </div>

                <h5 className="sub-title">Colors</h5>
                <div>
                  <div>
                    <ul className="colors ps-1">
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                    </ul>
                  </div>
                </div>

                <h5 className="sub-title">Sizes</h5>
                <div className="sizes">
                  <div className="form-check mb-1 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      6" (0)
                    </label>
                  </div>
                  <div className="form-check mb-1 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      8" (0)
                    </label>
                  </div>
                  <div className="form-check mb-1 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      10" (0)
                    </label>
                  </div>
                  <div className="form-check mb-1 ps-4">
                    <input
                      id=""
                      name=""
                      type="checkbox"
                      className="form-check-input fs-6"
                      value=""
                    />
                    <label id="_label" className="form-check-label" htmlFor="">
                      12" (0)
                    </label>
                  </div>
                </div>
              </div>

              <div className="filter-card mb-3">
                <h3 className="filter-title">Brands</h3>
                <div>
                  <div className="product-tags d-flex flex-wrap align-items-center gap-2">
                    <span className="badge py-2 px-3">test</span>
                    <span className="badge py-2 px-3">test</span>
                    <span className="badge py-2 px-3">test</span>
                    <span className="badge py-2 px-3">test</span>
                  </div>
                </div>
              </div>
              <div className="filter-card mb-3">
                <h3 className="filter-title">Random Product</h3>
              </div>
            </div>

            {/* Products Catalog Pane */}
            <div className="col-9">
              <div className="filter-sort-grid mb-3">
                <div className="d-flex justify-content-between align-items-center">
                  {/* Sort By Section */}
                  <div className="d-flex align-items-center gap-1">
                    <p className="d-block mb-0" style={{ width: "100px" }}>
                      Sort By:
                    </p>
                    <Form.Select aria-label="sort_by" id="">
                      <option value="manual">Featured</option>
                      <option value="best-selling" selected>
                        Best Selling
                      </option>
                      <option value="title-ascending">
                        Alphabetically, A-Z
                      </option>
                      <option value="title-descending">
                        Alphabetically, Z-A
                      </option>
                      <option value="price-ascending">
                        Price, low to high
                      </option>
                      <option value="price-descending">
                        Price, high to low
                      </option>
                    </Form.Select>
                  </div>

                  {/* Products Display Section */}
                  <div className="d-flex align-items-center gap-1">
                    <p className="totalproducts mb-0">XX Products</p>
                    <div className="d-flex align-items-center grid gap-1">
                      <img
                        className="d-block img-fluid"
                        src="/images/gr4.svg"
                        alt="grid"
                        onClick={() => {
                          setGrid(3);
                        }}
                      />
                      <img
                        className="d-block img-fluid"
                        src="/images/gr3.svg"
                        alt="grid"
                        onClick={() => {
                          setGrid(4);
                        }}
                      />
                      <img
                        className="d-block img-fluid"
                        src="/images/gr2.svg"
                        alt="grid"
                        onClick={() => {
                          setGrid(6);
                        }}
                      />
                      <img
                        className="d-block img-fluid"
                        src="/images/gr.svg"
                        alt="grid"
                        onClick={() => {
                          setGrid(12);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="products-list pb-2">
                <div className="d-flex flex-wrap gap-3">
                  <ProductCard
                  className=""
                    data={productState ? productState : []}
                    grid={grid}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Store;
