import React, { useState } from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import ProductCard from "../components/ProductCard";
import ReactStars from "react-rating-stars-component";
import { Button, Form } from "react-bootstrap";
import ReactImageZoom from "react-image-zoom";
import Color from "../components/Color";
import { FaRegHeart } from "react-icons/fa";
import { IoGitCompareOutline } from "react-icons/io5";
import Container from "../components/Container";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getAProduct } from "../features/products/productSlice";
import { toast } from "react-toastify";
import { addProductToCart, getUserCart } from "../features/user/userSlice";

const ProductView = () => {
  const [color, setColor] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const [alreadyAdded, setAlreadyAdded] = useState(false);
  const location = useLocation();
  const navigate = useNavigate()
  const getProductId = location.pathname.split("/")[2];
  console.log(getProductId);
  const dispatch = useDispatch();
  const productState = useSelector((state) => state.product.singleProduct);
  console.log(productState);
  console.log(color);
  const cartState = useSelector((state) => state.auth.cartProducts);
  useEffect(() => {
    dispatch(getAProduct(getProductId));
    dispatch(getUserCart());
  }, []);

  // useEffect(() => {
  //   for (let i = 0; i < cartState.length; i++) {
  //     if (getProductId === cartState[i]?.productId?._id) {
  //       setAlreadyAdded(true);
  //     }
  //   }
  // });

  const createCart = () => {
    if (color === null) {
      toast.error("Please Select A Color");
      return false;
    } else {
      dispatch(
        addProductToCart({
          productId: productState?._id,
          quantity,
          color,
          price: productState?.price,
        })
      )
      navigate('/cart')
    }
  };

  const props = {
    width: 250,
    height: 250,
    zoomWidth: 250,
    zoomPosition: "left",
    offset: { vertical: 0, horizontal: 10 },
    zoomLensStyle: "border-radius: 100%; background-color:gray; opacity:0.4",
    img: productState?.images[0]?.url
      ? productState?.images[0]?.url
      : "No Image Available",
  };

  const [orderedProduct, setOrderedProduct] = useState(true);
  const copyToClipboard = (text) => {
    console.log("text", text);
    var textField = document.createElement("textarea");
    textField.innerText = text;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand("copy");
    textField.remove();
  };

  return (
    <>
      <Meta title={productState?.title} />
      <BreadCrumb title={productState?.title} />

      {/* main product display */}
      <Container class1="main-product-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-6">
            <div className="main-product-image">
              {/* zoom effect */}
              <div>
                <ReactImageZoom {...props} />
              </div>
            </div>

            {/* product images */}
            <div className="other-product-image d-flex flex-wrap gap-2 mt-4">
              {productState?.images &&
                productState?.images.map((item, index) => {
                  return (
                    <div key={index}>
                      <img src={item?.url} alt="" className="img-fluid" />
                    </div>
                  );
                })}
            </div>
          </div>

          <div className="col-6">
            <div className="main-product-details">
              <div className="border-bottom">
                <h3 className="title">{productState?.title}</h3>
              </div>
              <div className="border-bottom py-2">
                <h1 className="price">&#8369; {productState?.price}</h1>
                <ReactStars
                  count={5}
                  size={24}
                  value={productState?.totalRating}
                  edit={false}
                />{" "}
                <p className="mb-2 review-count">(x reviews)</p>
                <a href="#write-review" className="review-btn">
                  {" "}
                  Write a review
                </a>
              </div>
              <div className="border-bottom py-2">
                <div className="d-flex align-items-center gap-2 my-2">
                  <h3 className="product-heading">Brand:</h3>{" "}
                  <p className="product-data">{productState?.brand}</p>
                </div>
                <div className="d-flex align-items-center gap-2 my-2">
                  <h3 className="product-heading">Category:</h3>{" "}
                  <p className="product-data">{productState?.category}</p>
                </div>
                <div className="d-flex align-items-center gap-2 my-2">
                  <h3 className="product-heading">Tags:</h3>{" "}
                  <p className="product-data">
                    {productState?.tags.toUpperCase()}
                  </p>
                </div>
                <div className="d-flex flex-column gap-1 my-3">
                  <h3 className="product-heading">Size:</h3>
                  <div className="d-flex flex-wrap gap-2">
                    <span className="badge text-dark bg-light border ">6"</span>
                    <span className="badge text-dark bg-light border ">8"</span>
                    <span className="badge text-dark bg-light border ">
                      12"
                    </span>
                  </div>
                </div>

                {alreadyAdded === false && (
                  <>
                    <div className="d-flex flex-column gap-2 my-3">
                      <h3 className="product-heading">Color:</h3>
                      <Color
                        setColor={setColor}
                        colorData={productState?.color}
                      />
                    </div>
                  </>
                )}
                <div className="d-flex flex-row align-items-center gap-3 my-2">
                  {alreadyAdded === false && (
                    <>
                      <h3 className="product-heading">Quantity:</h3>{" "}
                      <div className="">
                        <input
                          type="number"
                          min={1}
                          max={10}
                          style={{ width: "64px" }}
                          className="form-control"
                          onChange={(e) => setQuantity(e.target.value)}
                          value={quantity}
                        />
                      </div>
                    </>
                  )}
                  {/* Button Area */}
                  <div className={alreadyAdded ? "ms-0" : "d-flex align-items-center gap-3 ms-4"}>
                    <button
                      className="add-to-cart button border-0"
                      // data-bs-toggle="modal"
                      // data-bs-target="#staticBackdrop"
                      type="button"
                      onClick={() => {
                        alreadyAdded ? navigate("/cart") : createCart() ;
                      }}
                    >
                      {alreadyAdded ? "Go" : "Add"} to Cart
                    </button>
                    <Link className="buy-now">Buy Now</Link>
                  </div>
                </div>
                <div className="d-flex align-items-center gap-3">
                  <div>
                    <a href="">
                      <IoGitCompareOutline className="fs-5 me-1" /> Compare
                    </a>
                  </div>
                  <div>
                    <a href="">
                      <FaRegHeart className="fs-5 me-1" /> Wishlist
                    </a>
                  </div>
                </div>

                <div className="d-flex flex-column gap-1 mt-4 mb-3">
                  <h3 className="product-heading">Shipping & Returns</h3>{" "}
                  <p className="product-data">
                    Free shipping and returns available on all orders!
                    <br /> We ship all over Philippines within{" "}
                    <b>5-10 Business Days!</b>
                  </p>
                </div>
                <div className="d-flex flex-column gap-1 mb-3">
                  <h3 className="product-heading">Product Link</h3>{" "}
                  <a
                    // avoid redirect or reroute
                    href="javascript:void(0);"
                    className="text-decoration-underline"
                    onClick={() => {
                      copyToClipboard(window.location.href);
                    }}
                  >
                    Copy Here
                  </a>
                </div>
              </div>
            </div>

            <div className="description-wrapper mt-4">
              <div className="border-bottom">
                <h4 className="title">Description</h4>
              </div>
              <div className=" description-inner-wrapper py-4">
                <p
                  dangerouslySetInnerHTML={{
                    __html: productState?.description,
                  }}
                ></p>
              </div>
            </div>
          </div>
        </div>
      </Container>

      {/* product reviews */}
      <Container class1="reviews-wrapper home-wrapper-2 py-2">
        <div className="row">
          <div className="col-12 mt-4">
            <h3 id="write-review">Reviews</h3>
            <div className="review-inner-wrapper">
              <div className="review-head d-flex justify-content-between align-items-end">
                <div>
                  <h4 className="mb-1">Customer Reviews</h4>

                  <div className="d-flex align-items-center gap-1">
                    <ReactStars
                      count={5}
                      size={16}
                      value={0}
                      edit={true}
                      isHalf={true}
                    />
                    <p className="mb-0">Based on xx Reviews</p>
                  </div>
                </div>
                <div>
                  {orderedProduct && (
                    <a className="text-dark text-decoration-underline" href="">
                      {" "}
                      Write a review
                    </a>
                  )}
                </div>
              </div>
              <div className="review-form mt-3">
                <h4>Write a review</h4>
                <Form className="d-flex flex-column gap-2 mb-3">
                  <Form.Group className="mb-3" controlId="form.ControlInput1">
                    <Form.Control
                      type="text"
                      placeholder="Name"
                      className="mb-2"
                    />
                    <Form.Control
                      type="email"
                      placeholder="Email Address"
                      className="mb-2"
                    />
                    <p className="mb-0">Rating</p>
                    <ReactStars
                      count={5}
                      size={16}
                      value={0}
                      edit={true}
                      isHalf={true}
                    />
                  </Form.Group>
                  <Form.Group
                    className="mb-3"
                    controlId="form.ControlTextarea1"
                  >
                    <Form.Control
                      as="textarea"
                      placeholder="Comments"
                      rows={6}
                    />
                  </Form.Group>
                  <Button
                    type="submit"
                    value="Submit"
                    className="form-button d-flex justify-content-end border-0"
                  >
                    Submit Review
                  </Button>
                </Form>
              </div>
              <div className="reviews mt-3">
                <div className="review">
                  <div className="d-flex align-items-center gap-1">
                    <h6 className="mb-0 pe-2">User</h6>
                    <ReactStars
                      count={5}
                      size={16}
                      value={0}
                      edit={true}
                      isHalf={true}
                    />
                  </div>
                  <p className="mt-3">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Morbi a mauris non lectus aliquet fermentum.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>

      <Container class1="popular-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-12">
            <h3 className="section-heading">Our Popular Products</h3>
          </div>
        </div>
        <div className="row">
          <ProductCard />
        </div>
      </Container>
    </>
  );
};

export default ProductView;
