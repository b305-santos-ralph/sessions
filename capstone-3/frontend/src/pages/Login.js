import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Button, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Container from "../components/Container";
import CustomInput from "../utils/CustomInput";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { loginUser } from "../features/user/userSlice";

const loginSchema = yup.object({
  email: yup
    .string()
    .email("Invalid email")
    .required("Email is a required field"),
  password: yup.string().required("Password is a required field"),
});

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      dispatch(loginUser(values));
      setTimeout(() => {
        navigate("/");
      }, 600);
    },
  });

  return (
    <>
      <Meta title={"Login"} />
      <BreadCrumb title="Login" />
      <Container class1="login-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-12">
            <div className="login-card">
              <h3 className="title text-center mb-3">Login</h3>
              <form
                className="d-flex flex-column gap-2"
                onSubmit={formik.handleSubmit}
              >
                <div>
                  <CustomInput
                    type="email"
                    name="email"
                    placeholder="Email Address"
                    onChange={formik.handleChange("email")}
                    onBlur={formik.handleBlur("email")}
                    value={formik.values.email}
                  />
                  <div className="error">
                    {formik.touched.email && formik.errors.email}
                  </div>
                </div>
                <div>
                  <CustomInput
                    type="password"
                    name="password"
                    placeholder="Password"
                    onChange={formik.handleChange("password")}
                    onBlur={formik.handleBlur("password")}
                    value={formik.values.password}
                  />
                  <div className="error">
                    {formik.touched.password && formik.errors.password}
                  </div>
                </div>
                <div>
                  <Link to="/forgot-password">Forgot Password?</Link>

                  <div className="d-flex justify-content-center align-items-center gap-2 mt-3">
                    <button
                      className="btn btn-secondary border-0"
                      type="submit"
                    >
                      Login
                    </button>
                    <Link to="/signup" className="btn signup">
                      Signup
                    </Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Login;
