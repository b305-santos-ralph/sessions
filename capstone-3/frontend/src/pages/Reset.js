import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import CustomInput from "../utils/CustomInput";

const Reset = () => {
  return (
    <>
      <Meta title={"Reset Password"} />
      <BreadCrumb title="Reset Password" />
      <div className="login-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="login-card">
                <h3 className="title text-center mb-3">Reset Password</h3>
                <form className="d-flex flex-column gap-3">
                <CustomInput type="password" name="password" placeholder="New Password" />
                <CustomInput type="password" name="confirmPassword" placeholder="Confirm password" />
                  <div>
                    <div className="d-flex justify-content-center align-items-center gap-2 mt-3">
                      <Button className="button" type="submit">
                        Submit
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Reset;
