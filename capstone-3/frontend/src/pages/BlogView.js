import React, { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Link, useLocation } from "react-router-dom";
import { FaArrowLeftLong } from "react-icons/fa6";
import Container from "../components/Container";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { getABlog } from "../features/blogs/blogSlice";

const BlogView = () => {
  const blogState = useSelector((state) => state?.blog?.singleBlog);
  const location = useLocation();
  const getBlogId = location.pathname.split("/")[2];
  const dispatch = useDispatch();

  useEffect(() => {
    getBlog();
  }, []);

  const getBlog = () => {
    dispatch(getABlog(getBlogId));
  };

  return (
    <>
      <Meta title={blogState?.title} />
      <BreadCrumb title={blogState?.title} />
      <Container class1="blog-wrapper home-wrapper-2 py-5">
        <div className="row">
          <div className="col-12">
            <div className="blog-card-view">
              <Link to="/blog" className="d-flex align-items-center gap-2">
                <FaArrowLeftLong className="fs-5" /> Back to Blog
              </Link>
              <h3 className="title">{blogState?.title}</h3>
              <img
                src={blogState?.images[0].url ? blogState?.images[0].url : "No Image Available"}
                alt="blog"
                className="img-fluid w-100 my-4"
              />
              <p
                className="description"
                dangerouslySetInnerHTML={{
                  __html: blogState?.description
                    ? blogState?.description
                    : "",
                }}
              ></p>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default BlogView;
