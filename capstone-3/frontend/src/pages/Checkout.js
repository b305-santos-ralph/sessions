import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import { Form, FloatingLabel } from "react-bootstrap";
import { Link } from "react-router-dom";
import { IoCaretBack } from "react-icons/io5";
import watch from "../images/watch.jpg";
import Container from "../components/Container";

const Checkout = () => {
  return (
    <>
      <Meta title={"Cart"} />
      <BreadCrumb title="Cart" />

      <Container class1="checkout-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-7">
            <div className="checkout-left-data">
              <h3 className="website-name">Cutlery Corner</h3>

              {/* Divider */}
              <nav
                style={{ "--bs-breadcrumb-divider": ">" }}
                aria-label="breadcrumb"
              >
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <Link to="/card" className="text-dark total">
                      Cart
                    </Link>
                  </li>
                  &nbsp; /
                  <li
                    className="breadcrumb-item active total"
                    aria-current="page"
                  >
                    Information
                  </li>
                  &nbsp; /
                  <li
                    className="breadcrumb-item active total"
                    aria-current="page"
                  >
                    Shipping
                  </li>
                  &nbsp; /
                  <li
                    className="breadcrumb-item active total"
                    aria-current="page"
                  >
                    Payment
                  </li>
                </ol>
              </nav>

              {/* Contact Information Part */}
              <h4 className="title total">Contact Information</h4>
              <p className="user-details total">Full Name and Email Populate</p>
              <h4 className="mb-3">Shipping Address</h4>
              <div className="d-flex flex-wrap justify-content-between gap-2">
                {/* Saved Address Part */}
                <FloatingLabel
                  controlId="floatingSelect"
                  label="Saved Address"
                  className="w-100"
                >
                  <Form.Select aria-label="Floating label select example">
                    <option></option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </Form.Select>
                </FloatingLabel>

                {/* Country / Region part */}
                <FloatingLabel
                  controlId="floatingSelect"
                  label="Country / Region"
                  className="w-100"
                >
                  <Form.Select
                    aria-label="Floating label select example"
                    placeholder=" "
                  >
                    <option></option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </Form.Select>
                </FloatingLabel>

                {/* First Name part */}
                <Form.Group
                  className="mb-3 flex-grow-1"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="First Name" />
                </Form.Group>

                {/* Last Name part */}
                <Form.Group
                  className="mb-3 flex-grow-1"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="Last Name" />
                </Form.Group>

                {/* Address Part */}
                <Form.Group
                  className="mb-3 w-100"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="address" placeholder="Address" />
                </Form.Group>

                {/* Apartment, Street, etc Part */}
                <Form.Group
                  className="mb-3 w-100"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control
                    type="address"
                    placeholder="Apartment, suite, etc. (optional)"
                  />
                </Form.Group>

                {/* City Part */}
                <Form.Group
                  className="mb-3 flex-grow-1"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="City" />
                </Form.Group>

                {/* State Part */}
                <FloatingLabel
                  controlId="floatingSelect"
                  label="State"
                  className="flex-grow-1"
                >
                  <Form.Select aria-label="Floating label select example">
                    <option></option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </Form.Select>
                </FloatingLabel>

                {/* Zip part */}
                <Form.Group
                  className="mb-3 flex-grow-1"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="ZIP Code" />
                </Form.Group>
              </div>
              <div className="w-100">
                <div className="d-flex justify-content-between align-items-center">
                  <Link to="/cart" className="text-dark">
                    <IoCaretBack className="" /> Return to Cart
                  </Link>
                  <Link to="/" className="button">
                    Continue Checkout
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-5">
            <div className="border-bottom py-4">
              <div className="d-flex align-items-center gap-1 mb-2">
                <div className="w-75 d-flex gap-2">
                  <div className="w-25 position-relative">
                    <span
                      className="badge bg-secondary text-white rounded-circle p-2 position-absolute"
                      style={{ top: "-10px", right: "2px" }}
                    >
                      1
                    </span>
                    <img src={watch} alt="" className="img-fluid" />
                  </div>
                  <div>
                    <h5 className="total-price">TEST</h5>
                    <p className="total-price">test</p>
                  </div>
                </div>
                <div className="flex-grow-1">
                  <h5 className="total">Price</h5>
                </div>
              </div>
            </div>
            <div className="border-bottom py-4">
              <div className="d-flex justify-content-between align-items-center">
                <p>Subtotal</p>
                <p>subtotal</p>
              </div>
              <div className="d-flex justify-content-between align-items-center">
                <p className="mb-0 total">Shipping</p>
                <p className="mb-0 total-price">fee</p>
              </div>
            </div>

            <div className="d-flex justify-content-between align-items-center border-bottom py-4">
              <h4 className="total">Total</h4>
              <h5 className="total-price">Price</h5>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Checkout;
