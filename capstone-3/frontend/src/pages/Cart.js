import React, { useEffect, useState } from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import watch from "../images/watch.jpg";
import { MdOutlineDeleteForever } from "react-icons/md";
import { Link } from "react-router-dom";
import Container from "../components/Container";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCartProduct,
  getUserCart,
  updateCartProduct,
} from "../features/user/userSlice";

const Cart = () => {
  const dispatch = useDispatch();
  const [productUpdate, setProductUpdate] = useState(null);
  const [totalAmount, setTotalAmount] = useState(null);
  const userCartState = useSelector((state) => state.auth?.cartProducts);
  useEffect(() => {
    dispatch(getUserCart());
  }, []);

  useEffect(() => {
    if (productUpdate !== null) {
      dispatch(
        updateCartProduct({
          cartItemId: productUpdate?.cartItemId,
          quantity: productUpdate?.quantity,
        })
      );
      setTimeout(() => {
        dispatch(getUserCart());
      }, 300);
    }
  }, [productUpdate]);

  const deleteACartProduct = (id) => {
    dispatch(deleteCartProduct(id));
    setTimeout(() => {
      dispatch(getUserCart());
    }, 300);
  };

  useEffect(() => {
    let sum = 0;
    for (let i = 0; i < userCartState?.length; i++) {
      sum = sum + Number(userCartState[i].quantity) * Number(userCartState[i].price);
      setTotalAmount(sum);
    }
  }, [userCartState]);

  return (
    <>
      <Meta title={"Cart"} />
      <BreadCrumb title="Cart" />
      <Container class1="cart-wrapper home-wrapper-2 py-4">
        <div className="row">
          <div className="col-12">
            {/* Header Part */}
            <div className="cart-header d-flex justify-content-between align-content-center py-2">
              <h4 className="cart-col-1">Product</h4>
              <h4 className="cart-col-2">Price</h4>
              <h4 className="cart-col-3">Quantity</h4>
              <h4 className="cart-col-4">Total</h4>
            </div>
            {userCartState &&
              userCartState?.map((item, index) => {
                return (
                  // Cart Data Part
                  <div
                    key={index}
                    className="cart-data d-flex justify-content-between align-items-center py-2 mb-2"
                  >
                    <div className="cart-col-1 d-flex align-items-center gap-2">
                      <div className="w-25">
                        <img
                          className="img-fluid"
                          src={watch}
                          alt="product image"
                        />
                      </div>
                      <div className="w-75">
                        <p>{item?.productId.title}</p>
                        <p className="d-flex gap-3">
                          Color:
                          <ul className="colors ps-0">
                            <li
                              style={{ backgroundColor: item?.color.title }}
                            ></li>
                          </ul>
                        </p>
                      </div>
                    </div>
                    <div className="cart-col-2">
                      <h5 className="price">&#8369; {item?.price}</h5>
                    </div>
                    <div className="cart-col-3 d-flex align-items-center gap-2">
                      <div>
                        <input
                          className="form-control"
                          type="number"
                          min={1}
                          max={10}
                          value={
                            productUpdate?.quantity
                              ? productUpdate?.quantity
                              : item?.quantity
                          }
                          onChange={(e) => {
                            setProductUpdate({
                              cartItemId: item?._id,
                              quantity: e.target.value,
                            });
                          }}
                          name=""
                          id=""
                        />
                      </div>
                      <div>
                        <MdOutlineDeleteForever
                          className="fs-4 text-danger"
                          onClick={() => {
                            deleteACartProduct(item?._id);
                          }}
                        />
                      </div>
                    </div>
                    <div className="cart-col-4">
                      <h5 className="price">
                        &#8369; {item?.price * item?.quantity}
                      </h5>
                    </div>
                  </div>
                );
              })}

            <div className="col-12 py-2 mt-4">
              <div className="d-flex justify-content-between align-items-baseline checkout-page">
                <Link to="/store" className="button continue">
                  Continue Shopping
                </Link>
                {(totalAmount !== null || totalAmount !== 0) && (
                  <div className="d-flex flex-column justify-content-end text-center">
                    <h4>Subtotal: &#8369; {totalAmount}</h4>
                    <Link to="/checkout" className="button checkout">
                      Checkout
                    </Link>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Cart;
