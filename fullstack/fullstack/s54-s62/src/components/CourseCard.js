import { Card, Button } from 'react-bootstrap';
// import coursesData from '../data/coursesData';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed
	// console.log(courseProp);

	// Checks if every component receives the information in a form of an object
	// console.log(typeof props);

	const { _id, name, description, price } = courseProp; 

	// Use the state hook in this component to be able to store its state. example: 
	/* Syntax:
			const [getter, setter] = useState(initialGetterValue) */

	const [ count, setCount] = useState(0);

	// Function that keeps track of the enrollees for a course
	function enroll(){
		if (count < 30) {
			const enrollCount = count + 1;
			setCount(enrollCount);

			if (enrollCount === 30) {
				alert('No more seats.')
			}
		}
	}
	
	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price</Card.Subtitle>
	        <Card.Text>PHP {price}</Card.Text>
	        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
	    </Card.Body>
	</Card>	
	)
}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}