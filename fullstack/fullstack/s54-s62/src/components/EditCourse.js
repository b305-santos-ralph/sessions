import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';


export default function EditCourse({course}){

    // state for course id which will be used for fetching
    const [courseId, setCourseId] = useState("");

    // state for edit course modal
    const [showEdit, setShowEdit] = useState(false); // Initialize showEdit state

    // useState for our form (modal)
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    

    // functions for opening and closing the edit modal
    const openEdit = (courseId) => {
        setShowEdit(true); //Open the modal edit button 
    
        // fetching the actual data from the form
        fetch(`http://localhost:4000/courses/${courseId}`)
        
        // chain method
        .then(res => res.json())
        .then(({ _id, name, description, price }) => {
            setCourseId(_id);
            setName(name);
            setDescription(description);
            setPrice(price);
        })
    };
    
    const closeEdit = () => {
        setShowEdit(false);
        setName("");
        setDescription("");
        setPrice("");
    };


    // function to save the edits/changes made
    const updateCourse = (event, courseId) => {
        event.preventDefault();

        fetch(`http://localhost:4000/courses/${courseId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name,
                description,
                price
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Update Success!",
                    icon: "success",
                    text: "Course Successfully Updated!"
                })
                closeEdit();
            } else {
                Swal.fire({
                    title: "Update Failed!",
                    icon: "error",
                    text: "Please try again!"
                })
                closeEdit();
            }
        })
    }


    return(
        <>
            <Button variant="primary" size="sm" onClick={() => {openEdit(course)}}>Edit</Button>

            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={event => {
                        updateCourse(event, courseId);
                        // closeEdit();
                        }}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="courseName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={name} onChange={event => setName(event.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="courseDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control as="textarea" required value={description} onChange={event => setDescription(event.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="coursePrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" required value={price} onChange={event => setPrice(event.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}