import { Button, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


export default function Banner(props){

	const { title, content, destination, label } = props 

	const navigate = useNavigate()
	const handleClick = () => {
		navigate(destination)
	}

	return (
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" onClick={handleClick}>{label}</Button>
			</Col>
		</Row>
	)
}

