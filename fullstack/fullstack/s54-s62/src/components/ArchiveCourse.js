import { useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ArchiveCourse({ course }) {
  const [courseId, setCourseId] = useState("");

  const archiveCourse = (e, courseId) => {
    e.preventDefault();

    fetch(`http://localhost:4000/courses/${courseId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Update the course object's isActive property
        const updatedCourse = { ...course, isActive: false };
        setCourseId(updatedCourse);

        if (!data.isActive) {
          Swal.fire({
            title: "Archive Success!",
            icon: "success",
            text: "Course Successfully Archived!",
          });
        } else {
          Swal.fire({
            title: "Update Error!",
            icon: "error",
            text: "Please try again!",
          });
        }
      });
  };

  return (
    <>
      <Button variant="danger" onClick={() => archiveCourse(course)}>
        Archive
      </Button>
    </>
  );
}