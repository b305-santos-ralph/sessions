// MOCK DATABASE
const coursesData = [
    {
        id: 'wdc001',
        name: 'PHP - Laravel',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dapibus, enim ut pretium pharetra, nibh eros sodales ex, ut euismod nisi ligula a tellus. Sed euismod mollis mollis. Phasellus tincidunt finibus consequat. Curabitur ac sem urna. Praesent congue maximus dapibus. Suspendisse ut pulvinar lacus. Nulla ut neque lacus. Donec a dapibus lorem. In pretium vehicula faucibus.',
        price: 45000,
        onOffer: true,
    },
    {
        id: 'wdc002',
        name: 'Python - Django',
        description: 'Integer condimentum auctor justo, id faucibus tellus ultricies eget. Integer enim arcu, ultricies in volutpat eget, pharetra sit amet tortor. Nullam eleifend arcu in felis congue euismod. Donec pellentesque pulvinar ligula, quis scelerisque tellus tincidunt eu. Sed ipsum arcu, tincidunt eget est ut, venenatis placerat lectus.',
        price: 50000,
        onOffer: true,
    },
    {
        id: 'wdc003',
        name: 'Java - Springboot',
        description: 'Pellentesque a ligula nisl. Mauris varius porta justo, in aliquet sapien. Phasellus non elit id lacus pharetra malesuada. Quisque laoreet luctus lobortis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus quis mollis leo.',
        price: 55000,
        onOffer: true,
    }
]

export default coursesData;