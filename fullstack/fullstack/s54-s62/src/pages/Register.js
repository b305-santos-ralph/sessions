import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


export default function Register(){
    
    // State hooks - to store values of the input fields
    // Syntax: [stateVariable, setterFunction] = useState("assigned state/initial state")
    // Note: setterFunction is to update the state value
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [isActive, setIsActive] = useState(false);
    const { user } = useContext(UserContext);

    // Checks if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);

    function registerUser(event){

        // prevents the default behavior during submission, which is the page redirects via form submission
        event.preventDefault();

        fetch('http://localhost:4000/users/register', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                mobileNo,
                password
            })
        })
        .then(res => res.json())
        .then(data => {
            /* 'data' is the response from the web service */
            if(data){
                setFirstName("");
                setLastName("");
                setEmail("");
                setMobileNo("");
                setPassword("");
                setConfirmPassword("");

                alert('Thank you for registering!')
            }else{
                alert('Please try again later.')
            }


            // checks the value of response
            console.log(data);
        })
    }

    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") 
            && (password === confirmPassword) 
            && (mobileNo.length === 11)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    },[ firstName, lastName, email, mobileNo, password, confirmPassword])


    return(

        /* Two-way binding */
        // getting the value
        // setting the value / updating the value
        (user.id !== null) ?
        <Navigate to="/courses" />
        :
		<Form onSubmit={(event) => registerUser(event)}>
		<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={event => {setFirstName(event.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={event => {setLastName(event.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={event => {setEmail(event.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={event => {setMobileNo(event.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={event => {setPassword(event.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={event => {setConfirmPassword(event.target.value)}}/>
			</Form.Group>

            {/* Conditional rendering */}
            {
                isActive ?
                <Button variant="primary" type="submit">Submit</Button>
                :
                <Button variant="primary" type="submit" disabled>Submit</Button>
            }
			
				
		</Form>
    )
}