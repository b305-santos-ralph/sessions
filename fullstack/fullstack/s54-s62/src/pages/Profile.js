import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

function Profile() {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    // Fetch user details from the backend
    fetch('http://localhost:4000/users/details', {
      method: 'GET',
      headers: {
        "Content-Type": "application/json",
        // Retrieve token from local storage
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {

        // Set user data received from the backend
        setUserData(data); 
      })
      .catch((error) => {
        console.error('Error fetching user profile:', error);
      });
  }, []);

  return (
    <Container fluid>
        {userData ? (
            
        <>
        <Row className="p-5 bg-dark text-white">
        <h1 className="bg-danger p-2">PROFILE</h1>
            <Col className="px-5">
            <h2 className="bg-warning p-2 text-dark">{userData.firstName} {userData.lastName}</h2>
            </Col>
        </Row>
        <Row className="px-5 bg-secondary pt-3">
            <Col className="bg-primary p-2 mb-3">
            <h4 className="bg-success m-2 p-2">Details</h4>
            <p className="px-5 pt-3 bg-info m-0">Contact: {userData.mobileNo}</p>
            <p className="px-5 bg-info m-0 pb-3">Email: {userData.email}</p>
            </Col>
        </Row>
        </>
        ) : (
            <p></p>
    )}
    </Container>
  );
}

export default Profile;