import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';


export default function Logout() {

    const { unsetUser, setUser } = useContext(UserContext);

    // updates the localStorage to empty / clears the localStorage
    unsetUser();

    useEffect(() => {
        // setter function
        // sets saved user state in context to null
        setUser({
            id: null,
            isAdmin: null
        });
    })


    // Redirect back to login
    return (
        <Navigate to="/login" />
    )
}