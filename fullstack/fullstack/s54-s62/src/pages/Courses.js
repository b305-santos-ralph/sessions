import CourseCard from '../components/CourseCard';
import AdminView from './AdminView';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

/* For mock database */
// import coursesData from '../data/coursesData';

/* Actual database */
import { useState, useEffect, useContext } from 'react';

export default function Courses(){

    // Checks if the mock database was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    const { user } = useContext(UserContext);
    const [courses, setCourses] = useState([]);

    // const fetchData = () => {
    //     fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
    //     .then(res => res.json())
    //     .then(data => {
            
    //         console.log(data);

    //         // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
    //         setCourses(data);

    //     });
    // }

    // useEffect(() => {

    //     fetchData()

    // }, []);

    useEffect(() => {
        // get all active courses
        // fetch(`http://localhost:4000/courses/`)
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
        .then(res => res.json())
        .then(data => {

            if(user.isAdmin){
                setCourses(data);
            }else{
                setCourses(data.map(course => {
                return (
                    <CourseCard key={course._id} courseProp={course} />
                    )
                }))
            }

        }, []);
    })



    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} courseProp={course} />
    //     )
    // })

    return (
        <>
        {
            (user.isAdmin) ?
            <>
                <AdminView coursesData={courses}/>
            </>
            :
            <>
            {courses}
            </>
        }

        </>
    )
}