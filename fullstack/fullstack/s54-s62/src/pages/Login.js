import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Login() {

    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    // State hook to determine whether the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    function loginUser(event){
        event.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email,
                password
            })
        })
        .then(res => res.json())
        // data = response = object with access property -> access property contains token
        .then(data => {

            console.log(data);

            if(typeof data.access !== "undefined"){

                localStorage.setItem('token', data.access);

                // 
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })

                // this is for user  state to have access propert with token
                // setUser({
                //     access: localStorage.getItem('token')
                // })
                // alert('Login success!')

            }else{
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "warning",
                    text: "Check your login details and try again!"
                })
            }
        })
        // dependency array to clear input fields after submission
        setEmail("");
        setPassword("");
    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // for data retrieved checking
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if( email !== "" || password !== ""){
            setIsActive(true);
        }else{
            setIsActive(false)
        }
    },[email, password]);

    return (
    /* If local storage contains token it will navigate to courses component using a defined endpoint */

    // 
    // (user.id !== null ) ? (
    //     (user.isAdmin) ? (
    //         <Navigate to="/adminView" />
    //     ) : (
    //         <Navigate to="/courses" />
    //     )
    // )

    
    (user.id !== null ) ?
    <Navigate to="/courses" />
    :
    /* else, it will use the form for login */
      <Form id="login" onSubmit={(event) => loginUser(event)}>
        <h1 className="my-5 text-center">Login</h1>
        <Form.Group className="mb-3" controlId="formGroupEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event => setEmail(event.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formGroupPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" required value={password} onChange={event => setPassword(event.target.value)}/>
        </Form.Group>

        {
            isActive ?
            <Button variant="primary" type="submit">Submit</Button>
            :
            <Button variant="primary" type="submit" disabled>Submit</Button>
        }
      </Form>
        
    )
}