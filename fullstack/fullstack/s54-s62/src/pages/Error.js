import Banner from '../components/Banner';


export default function Error(){

	return (
		<Banner
			title="Error 404 - Page Not Found"
			content="Page requested does not exist"
			destination="/"
			label="Back to Home"
			/>
	)
}