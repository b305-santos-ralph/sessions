let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}



function enqueue(item){
    // add an item
    collection.push(item);
    return collection;
}
// enqueue names



function dequeue() {
    // remove an item
    if (!isEmpty()) {
        collection.shift();
    }
    return collection;
}
// dequeue the first name



function front(){
    // get the front item
    return collection.length > 0 ? collection[0] : undefined;
}
// get the front item or name



function size(){
    // get the size of the array
    return collection.length;
}
// get queue size



function isEmpty(){
    // check array if empty
    return collection.length === 0;
}
// check queue if empty



// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};